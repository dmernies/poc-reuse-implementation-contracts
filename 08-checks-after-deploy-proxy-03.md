# Checks after Deployment - Proxy 03

## Token and ComplianceService Ownerships and target

### Token

First we check basic information about the token.

```bash
truffle(development)> const tokenProxy03 = await DSToken.at('0xd287A90417b877aa7Fa3523e74C043EC5a4299B4')
undefined
truffle(development)> await tokenProxy03.name.call()
'Proxy03'
truffle(development)> await tokenProxy03.symbol.call()
'Proxy03'
truffle(development)> await tokenProxy03.decimals.call()
BN { negative: 0, words: [ 0, <1 empty item> ], length: 1, red: null }
truffle(development)> await tokenProxy03.owner.call()
'0x0a065Eaf3b6e037F8213fa13e76A025d2846Bd48'
```

We can see the correct target address.

```bash
truffle(development)> const proxyProxy03 = await Proxy.at('0xd287A90417b877aa7Fa3523e74C043EC5a4299B4')
undefined
truffle(development)> await proxyProxy03.target.call()
'0x5f14051c79fEC81f89b3790D8F2427ed5b25F817'
```

The we check Proxy03 storage (Proxy Contract)

```bash
truffle(development)> const proxyProxy03 = await Proxy.at('0xd287A90417b877aa7Fa3523e74C043EC5a4299B4')
undefined
truffle(development)> await proxyProxy03.target.call()
'0x5f14051c79fEC81f89b3790D8F2427ed5b25F817'
truffle(development)> web3.eth.getStorageAt('0xd287A90417b877aa7Fa3523e74C043EC5a4299B4', 0)
'0x0a065eaf3b6e037f8213fa13e76a025d2846bd48'
truffle(development)> web3.eth.getStorageAt('0xd287A90417b877aa7Fa3523e74C043EC5a4299B4', 1)
'0x015f14051c79fec81f89b3790d8f2427ed5b25f817'
truffle(development)> web3.eth.getStorageAt('0xd287A90417b877aa7Fa3523e74C043EC5a4299B4', 2)
'0x05'
truffle(development)> web3.eth.getStorageAt('0xd287A90417b877aa7Fa3523e74C043EC5a4299B4', 3)
'0x0a065eaf3b6e037f8213fa13e76a025d2846bd48'
truffle(development)> web3.eth.getStorageAt('0xd287A90417b877aa7Fa3523e74C043EC5a4299B4', 4)
'0x0'
truffle(development)> web3.eth.getStorageAt('0xd287A90417b877aa7Fa3523e74C043EC5a4299B4', 5)
'0x0'
truffle(development)> web3.eth.getStorageAt('0xd287A90417b877aa7Fa3523e74C043EC5a4299B4', 6)
'0x0'
truffle(development)> web3.eth.getStorageAt('0xd287A90417b877aa7Fa3523e74C043EC5a4299B4', 7)
'0x0'
truffle(development)> web3.eth.getStorageAt('0xd287A90417b877aa7Fa3523e74C043EC5a4299B4', 8)
'0x0'
truffle(development)> web3.eth.getStorageAt('0xd287A90417b877aa7Fa3523e74C043EC5a4299B4', 9)
'0x0'
truffle(development)> web3.eth.getStorageAt('0xd287A90417b877aa7Fa3523e74C043EC5a4299B4', 10)
'0x0'
```

And we check implementation contract. No changes found after the two proxy deployments.

```bash
truffle(development)> web3.eth.getStorageAt('0x5f14051c79fEC81f89b3790D8F2427ed5b25F817', 0)
'0x0'
truffle(development)> web3.eth.getStorageAt('0x5f14051c79fEC81f89b3790D8F2427ed5b25F817', 1)
'0x0'
truffle(development)> web3.eth.getStorageAt('0x5f14051c79fEC81f89b3790D8F2427ed5b25F817', 2)
'0x01'
truffle(development)> web3.eth.getStorageAt('0x5f14051c79fEC81f89b3790D8F2427ed5b25F817', 3)
'0x0'
truffle(development)> web3.eth.getStorageAt('0x5f14051c79fEC81f89b3790D8F2427ed5b25F817', 4)
'0x0'
truffle(development)> web3.eth.getStorageAt('0x5f14051c79fEC81f89b3790D8F2427ed5b25F817', 5)
'0x0'
truffle(development)> web3.eth.getStorageAt('0x5f14051c79fEC81f89b3790D8F2427ed5b25F817', 6)
'0x0'
truffle(development)> web3.eth.getStorageAt('0x5f14051c79fEC81f89b3790D8F2427ed5b25F817', 7)
'0x0'
truffle(development)> web3.eth.getStorageAt('0x5f14051c79fEC81f89b3790D8F2427ed5b25F817', 8)
'0x0'
truffle(development)> web3.eth.getStorageAt('0x5f14051c79fEC81f89b3790D8F2427ed5b25F817', 9)
'0x0'
truffle(development)> web3.eth.getStorageAt('0x5f14051c79fEC81f89b3790D8F2427ed5b25F817', 10)
'0x0'
truffle(development)> web3.eth.getStorageAt('0x5f14051c79fEC81f89b3790D8F2427ed5b25F817', 11)
'0x0'
truffle(development)> web3.eth.getStorageAt('0x5f14051c79fEC81f89b3790D8F2427ed5b25F817', 12)
'0x0'
```

And finally general information about the token 

```bash
diego-securitize@heavy-machine:~/dsclient/master-master2$ dsclient token:info
{"tokenParams":{"name":"Proxy03","symbol":"Proxy03","decimals":0,"totalIssued":"0","totalWallets":0,"isPaused":false}}
diego-securitize@heavy-machine:~/dsclient/master-master2$ dsclient token:totalSupply
0
diego-securitize@heavy-machine:~/dsclient/master-master2$ dsclient holder:info inversor01
 ›   Error: Investor not found
```


### ComplianceService

First we check onweship and proxy target.

```bash
truffle(development)> const complianceProxy = await ComplianceServiceRegulated.at('0x74282c8B078d99d93e179E8216A0748Ec2e3E0FE')
undefined
truffle(development)> await complianceProxy.owner.call()
'0x0a065Eaf3b6e037F8213fa13e76A025d2846Bd48'
truffle(development)> const complianceProxyProxy = await Proxy.at('0x74282c8B078d99d93e179E8216A0748Ec2e3E0FE')
truffle(development)> await complianceProxyProxy.target.call()
'0x597c515ece0f68Ba1528Ee9aa4c38E3aD9C82D45'
```

Then we check the storage of the Proxy contracts

```bash
truffle(development)> web3.eth.getStorageAt('0x74282c8B078d99d93e179E8216A0748Ec2e3E0FE', 0)
'0x0a065eaf3b6e037f8213fa13e76a025d2846bd48'
truffle(development)> web3.eth.getStorageAt('0x74282c8B078d99d93e179E8216A0748Ec2e3E0FE', 1)
'0x01597c515ece0f68ba1528ee9aa4c38e3ad9c82d45'
truffle(development)> web3.eth.getStorageAt('0x74282c8B078d99d93e179E8216A0748Ec2e3E0FE', 2)
'0x06'
truffle(development)> web3.eth.getStorageAt('0x74282c8B078d99d93e179E8216A0748Ec2e3E0FE', 3)
'0x0a065eaf3b6e037f8213fa13e76a025d2846bd48'
truffle(development)> web3.eth.getStorageAt('0x74282c8B078d99d93e179E8216A0748Ec2e3E0FE', 4)
'0x0'
truffle(development)> web3.eth.getStorageAt('0x74282c8B078d99d93e179E8216A0748Ec2e3E0FE', 5)
'0x0'
truffle(development)> web3.eth.getStorageAt('0x74282c8B078d99d93e179E8216A0748Ec2e3E0FE', 6)
'0x0'
truffle(development)> web3.eth.getStorageAt('0x74282c8B078d99d93e179E8216A0748Ec2e3E0FE', 7)
'0x0'
truffle(development)> web3.eth.getStorageAt('0x74282c8B078d99d93e179E8216A0748Ec2e3E0FE', 8)
'0x0'
truffle(development)> web3.eth.getStorageAt('0x74282c8B078d99d93e179E8216A0748Ec2e3E0FE', 9)
'0x0'
truffle(development)> web3.eth.getStorageAt('0x74282c8B078d99d93e179E8216A0748Ec2e3E0FE', 10)
'0x0'
truffle(development)> web3.eth.getStorageAt('0x74282c8B078d99d93e179E8216A0748Ec2e3E0FE', 11)
'0x0'
truffle(development)> web3.eth.getStorageAt('0x74282c8B078d99d93e179E8216A0748Ec2e3E0FE', 12)
'0x0'
```

Finally we check de implementation contract. No changes found.

```bash
truffle(development)> web3.eth.getStorageAt('0x597c515ece0f68Ba1528Ee9aa4c38E3aD9C82D45', 0)
'0x0'
truffle(development)> web3.eth.getStorageAt('0x597c515ece0f68Ba1528Ee9aa4c38E3aD9C82D45', 1)
'0x0'
truffle(development)> web3.eth.getStorageAt('0x597c515ece0f68Ba1528Ee9aa4c38E3aD9C82D45', 2)
'0x01'
truffle(development)> web3.eth.getStorageAt('0x597c515ece0f68Ba1528Ee9aa4c38E3aD9C82D45', 3)
'0x0'
truffle(development)> web3.eth.getStorageAt('0x597c515ece0f68Ba1528Ee9aa4c38E3aD9C82D45', 4)
'0x0'
truffle(development)> web3.eth.getStorageAt('0x597c515ece0f68Ba1528Ee9aa4c38E3aD9C82D45', 5)
'0x0'
truffle(development)> web3.eth.getStorageAt('0x597c515ece0f68Ba1528Ee9aa4c38E3aD9C82D45', 6)
'0x0'
truffle(development)> web3.eth.getStorageAt('0x597c515ece0f68Ba1528Ee9aa4c38E3aD9C82D45', 7)
'0x0'
truffle(development)> web3.eth.getStorageAt('0x597c515ece0f68Ba1528Ee9aa4c38E3aD9C82D45', 8)
'0x0'
truffle(development)> web3.eth.getStorageAt('0x597c515ece0f68Ba1528Ee9aa4c38E3aD9C82D45', 9)
'0x0'
truffle(development)> web3.eth.getStorageAt('0x597c515ece0f68Ba1528Ee9aa4c38E3aD9C82D45', 10)
'0x0'
truffle(development)> web3.eth.getStorageAt('0x597c515ece0f68Ba1528Ee9aa4c38E3aD9C82D45', 11)
'0x0'
truffle(development)> web3.eth.getStorageAt('0x597c515ece0f68Ba1528Ee9aa4c38E3aD9C82D45', 12)
'0x0'
```
