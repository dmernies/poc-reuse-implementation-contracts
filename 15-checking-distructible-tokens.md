# Trying to destroy a token

We created a new branch with distructible interface to see what happens
when a proxy is distroyed.

Proxy Distroyable token: 0x3281233432112eF22aeafBEe934c8b67BB656A25
Implementation Distroyable: 0xe8d19AC1ddfD36766e81Edc3a3f22D5a418fBd45

```bash
diego-securitize@heavy-machine:~/dstoken$ npx truffle console --network developmentDestroyer
truffle(developmentDestroyer)> const proxy = await Proxy.at('0x3281233432112eF22aeafBEe934c8b67BB656A25')
undefined
truffle(developmentDestroyer)> proxy.target.call()
'0xe8d19AC1ddfD36766e81Edc3a3f22D5a418fBd45'
truffle(developmentDestroyer)> const token = await DSToken.at('0x3281233432112eF22aeafBEe934c8b67BB656A25')
undefined
truffle(developmentDestroyer)> token.name.call()
'DistructibleToken'
truffle(developmentDestroyer)> const toeknImpl = await DSToken.at('0xe8d19AC1ddfD36766e81Edc3a3f22D5a418fBd45')
undefined
truffle(developmentDestroyer)> toeknImpl.name.call()
''
```

Destroying it
```bash
truffle(developmentDestroyer)> await token.destroy()
{
  tx: '0xf86f3a415c037003ac9205b458c10cea7776f56452a607f17a94b1a075a28811',
  receipt: {
    transactionHash: '0xf86f3a415c037003ac9205b458c10cea7776f56452a607f17a94b1a075a28811',
    transactionIndex: 0,
    blockHash: '0x471dcb8341202f640faa67530396bdd5c753c7597d4ce7a7d52cda59dde52792',
    blockNumber: 6033,
    from: '0x51f0c9842f12d7e32077d5a69a95d8c656f5cc02',
    to: '0x3281233432112ef22aeafbee934c8b67bb656a25',
    gasUsed: 14893,
    cumulativeGasUsed: 14893,
    contractAddress: null,
    logs: [],
    status: true,
    logsBloom: '0x00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000',
    rawLogs: []
  },
  logs: []
}
```

Then we destory the token

```bash
truffle(developmentDestroyer)> const token2 = await DSToken.at('0x3281233432112eF22aeafBEe934c8b67BB656A25')
Uncaught:
Error: Cannot create instance of DSToken; no code at address 0x3281233432112eF22aeafBEe934c8b67BB656A25
    at processTicksAndRejections (internal/process/task_queues.js:97:5)
    at Function.at (/home/diego-securitize/dstoken/node_modules/truffle/build/webpack:/packages/contract/lib/contract/constructorMethods.js:74:1)
    at Object.checkCode (/home/diego-securitize/dstoken/node_modules/truffle/build/webpack:/packages/contract/lib/utils/index.js:265:1)

```

But implementation is still alive

```bash
truffle(developmentDestroyer)> const token2 = await DSToken.at('0xe8d19AC1ddfD36766e81Edc3a3f22D5a418fBd45')
undefined
truffle(developmentDestroyer)> token2
TruffleContract {
  constructor: [Function: TruffleContract] {
    _constructorMethods: {
      configureNetwork: [Function: configureNetwork],
```