# Deploy a new Token

For this test we will use the latest dev version that includes new OmnibusTBEWallet.

The goal of this test is check that all proxies and implementation contracts are alive after and upgrade. Also wi will check that storage of upgraded contracts are OK.

We will deploy a new implementation contract and upgrade PocToken-01 to new implementation and check that  other proxies are still alive.

**Note:** PocToken-01 was deployed with other owner. 

- PocToken-01: 0x33cc6c1c002311d26b561e1c8df3466f62bae1f9
- NewToken: 0x7A4479DeD8b545C2Bc74f9716b72b69B63913534



```bash
$git checkout dev
$git pull
$truffle migrate --name NewToken --symbol NewToken --decimals 0 --network developmentMaster3 --owners "0x7A4479DeD8b545C2Bc74f9716b72b69B63913534 0x51f0c9842F12d7E32077d5A69a95D8c656F5cc02" --required_confirmations 2 --omnibus_wallet 0x275B0d8f14a77DceDB4A2b7cD4532a39C9AAb2A4
```

```bash
Starting migrations...
======================
> Network name:    'developmentMaster3'
> Network id:      5777
> Block gas limit: 6721975 (0x6691b7)


1_initial_migration.js
======================

   Deploying 'Migrations'
   ----------------------
   > transaction hash:    0x95dc65e6eed79ad37932cd04d9a5fd733351e798a162bdc2158a55fbe9c761d7
   > Blocks: 0            Seconds: 0
   > contract address:    0xF51B3593220b769c2D1631C63Abe11A80327aCC3
   > block number:        5646
   > block timestamp:     1611890031
   > account:             0x7A4479DeD8b545C2Bc74f9716b72b69B63913534
   > balance:             9999999999.99451428
   > gas used:            274286 (0x42f6e)
   > gas price:           20 gwei
   > value sent:          0 ETH
   > total cost:          0.00548572 ETH


   > Saving migration to chain.
   > Saving artifacts
   -------------------------------------
   > Total cost:          0.00548572 ETH


2_deploy_libraries.js
=====================
{
  _: [ 'migrate' ],
  name: 'NewToken',
  symbol: 'NewToken',
  decimals: 0,
  network: 'developmentMaster3',
  owners: '0x7A4479DeD8b545C2Bc74f9716b72b69B63913534 0x51f0c9842F12d7E32077d5A69a95D8c656F5cc02',
  required_confirmations: 2,
  omnibus_wallet: '0x275B0d8f14a77DceDB4A2b7cD4532a39C9AAb2A4'
}

   Deploying 'ComplianceServiceLibrary'
   ------------------------------------
   > transaction hash:    0xf358726911070e4e03ac865f4ee110ab5f682752fe4c9def9c59775016159b3f
   > Blocks: 0            Seconds: 0
   > contract address:    0xe0abeB1D5A5209996fCD4BB3b0c5ffABC35a1cEa
   > block number:        5648
   > block timestamp:     1611890033
   > account:             0x7A4479DeD8b545C2Bc74f9716b72b69B63913534
   > balance:             9999999999.89059034
   > gas used:            5153896 (0x4ea468)
   > gas price:           20 gwei
   > value sent:          0 ETH
   > total cost:          0.10307792 ETH


   Deploying 'TokenLibrary'
   ------------------------
   > transaction hash:    0xae3cb5fb4276b952d511d22920c1323eeaec82ee4f16525e5c129bb1fb101147
   > Blocks: 0            Seconds: 0
   > contract address:    0xA8b1AA2f9EeFE74A06386a3606239D8eC8Ed8664
   > block number:        5649
   > block timestamp:     1611890033
   > account:             0x7A4479DeD8b545C2Bc74f9716b72b69B63913534
   > balance:             9999999999.8542713
   > gas used:            1815952 (0x1bb590)
   > gas price:           20 gwei
   > value sent:          0 ETH
   > total cost:          0.03631904 ETH


   Linking
   -------
   * Contract: ComplianceServiceRegulated <--> Library: ComplianceServiceLibrary (at address: 0xe0abeB1D5A5209996fCD4BB3b0c5ffABC35a1cEa)

   Linking
   -------
   * Contract: DSToken <--> Library: TokenLibrary (at address: 0xA8b1AA2f9EeFE74A06386a3606239D8eC8Ed8664)

   > Saving migration to chain.
   > Saving artifacts
   -------------------------------------
   > Total cost:          0.13939696 ETH


3_deploy_trust_manager.js
=========================

   Deploying 'TrustService'
   ------------------------
   > transaction hash:    0xc369dc85ac640486ffe7a831f432ebfbbb34eed4398436271154e45a5d5baaae
   > Blocks: 0            Seconds: 0
   > contract address:    0xA7ceDb8f483E9794D4EfA3707AE8d374335bb3bC
   > block number:        5651
   > block timestamp:     1611890034
   > account:             0x7A4479DeD8b545C2Bc74f9716b72b69B63913534
   > balance:             9999999999.81299926
   > gas used:            2036301 (0x1f124d)
   > gas price:           20 gwei
   > value sent:          0 ETH
   > total cost:          0.04072602 ETH


   Deploying 'Proxy'
   -----------------
   > transaction hash:    0x1e4dcd410bfebad4185f11460faf83c76f6a028c8f97554dc1632512c2424dfd
   > Blocks: 0            Seconds: 0
   > contract address:    0x06C729e4f59360FF273D3C2498F311a5838725Ec
   > block number:        5652
   > block timestamp:     1611890034
   > account:             0x7A4479DeD8b545C2Bc74f9716b72b69B63913534
   > balance:             9999999999.80825352
   > gas used:            237287 (0x39ee7)
   > gas price:           20 gwei
   > value sent:          0 ETH
   > total cost:          0.00474574 ETH


   > Saving migration to chain.
   > Saving artifacts
   -------------------------------------
   > Total cost:          0.04547176 ETH


4_deploy_registry.js
====================

   Deploying 'RegistryService'
   ---------------------------
   > transaction hash:    0xaa193283354bb711859c849bedba35914b48ce8bac7374472083236548b8977e
   > Blocks: 0            Seconds: 0
   > contract address:    0xB5B426f0759F538a1c6b9E4ab130a316cCf3Af55
   > block number:        5656
   > block timestamp:     1611890035
   > account:             0x7A4479DeD8b545C2Bc74f9716b72b69B63913534
   > balance:             9999999999.70749614
   > gas used:            4831371 (0x49b88b)
   > gas price:           20 gwei
   > value sent:          0 ETH
   > total cost:          0.09662742 ETH


   Replacing 'Proxy'
   -----------------
   > transaction hash:    0x1fabb0f1b08b03ccd06910a4c21a244da25e5bca05012158a501a499cbf16c1e
   > Blocks: 0            Seconds: 0
   > contract address:    0xb08F3664f5661b63E46dAb6E6c6AFD3ad07581c5
   > block number:        5657
   > block timestamp:     1611890036
   > account:             0x7A4479DeD8b545C2Bc74f9716b72b69B63913534
   > balance:             9999999999.7027504
   > gas used:            237287 (0x39ee7)
   > gas price:           20 gwei
   > value sent:          0 ETH
   > total cost:          0.00474574 ETH


   > Saving migration to chain.
   > Saving artifacts
   -------------------------------------
   > Total cost:          0.10137316 ETH


5_deploy_compliance_manager.js
==============================

   Deploying 'ComplianceServiceRegulated'
   --------------------------------------
   > transaction hash:    0x71e4a5fe1beb8263b6165986733bd99fbb78ba9bcc091c9dade72081272ef7a5
   > Blocks: 0            Seconds: 0
   > contract address:    0xEc52A1cD5d945d7CF7a56A71FDA1a0f81B06479D
   > block number:        5661
   > block timestamp:     1611890037
   > account:             0x7A4479DeD8b545C2Bc74f9716b72b69B63913534
   > balance:             9999999999.60337048
   > gas used:            4738283 (0x484ceb)
   > gas price:           20 gwei
   > value sent:          0 ETH
   > total cost:          0.09476566 ETH


   Replacing 'Proxy'
   -----------------
   > transaction hash:    0x2e6fa1996545e2a74dce532bc03788d929ce50f5d68fec5acf547d97575cac8c
   > Blocks: 0            Seconds: 0
   > contract address:    0xaA8883f94cB2DD3145D6C38c0D3AC508398BD52D
   > block number:        5662
   > block timestamp:     1611890037
   > account:             0x7A4479DeD8b545C2Bc74f9716b72b69B63913534
   > balance:             9999999999.59862474
   > gas used:            237287 (0x39ee7)
   > gas price:           20 gwei
   > value sent:          0 ETH
   > total cost:          0.00474574 ETH


   > Saving migration to chain.
   > Saving artifacts
   -------------------------------------
   > Total cost:           0.0995114 ETH


6_deploy_complianceConfiguration.js
===================================

   Deploying 'ComplianceConfigurationService'
   ------------------------------------------
   > transaction hash:    0x22894c25963295dad80ef6c57cee85b8842883e0c5b3a145ba6ae3598f16b187
   > Blocks: 0            Seconds: 0
   > contract address:    0xD32cB1168742Ddfd593b442711101ec27aBeC2E0
   > block number:        5666
   > block timestamp:     1611890038
   > account:             0x7A4479DeD8b545C2Bc74f9716b72b69B63913534
   > balance:             9999999999.5204968
   > gas used:            3628169 (0x375c89)
   > gas price:           20 gwei
   > value sent:          0 ETH
   > total cost:          0.07256338 ETH


   Replacing 'Proxy'
   -----------------
   > transaction hash:    0x0ab03713d01e2b853e615758de7a4355d8eb8d66e3e81e9096e39ca85ed6dcc1
   > Blocks: 0            Seconds: 0
   > contract address:    0x90409Aa7A70d7C5e73b36710131DFCB043A6e242
   > block number:        5667
   > block timestamp:     1611890038
   > account:             0x7A4479DeD8b545C2Bc74f9716b72b69B63913534
   > balance:             9999999999.51575106
   > gas used:            237287 (0x39ee7)
   > gas price:           20 gwei
   > value sent:          0 ETH
   > total cost:          0.00474574 ETH


   > Saving migration to chain.
   > Saving artifacts
   -------------------------------------
   > Total cost:          0.07730912 ETH


7_deploy_wallet_manager.js
==========================

   Deploying 'WalletManager'
   -------------------------
   > transaction hash:    0xb2994efed718ca63258588a9750f063993bff4c442542cac5a545fae47d81186
   > Blocks: 0            Seconds: 0
   > contract address:    0x782302007EAd7f84802558675585a5D51D6B30F9
   > block number:        5671
   > block timestamp:     1611890039
   > account:             0x7A4479DeD8b545C2Bc74f9716b72b69B63913534
   > balance:             9999999999.47948056
   > gas used:            1582901 (0x182735)
   > gas price:           20 gwei
   > value sent:          0 ETH
   > total cost:          0.03165802 ETH


   Replacing 'Proxy'
   -----------------
   > transaction hash:    0x407e38c8fe46a6b9ab91c716bf395e142f6778fe0ebee86c500f2861e4d06f46
   > Blocks: 0            Seconds: 0
   > contract address:    0x89d9c902381DCa10A4FbE1Fe2FAB65Eca2dD74A8
   > block number:        5672
   > block timestamp:     1611890039
   > account:             0x7A4479DeD8b545C2Bc74f9716b72b69B63913534
   > balance:             9999999999.47473482
   > gas used:            237287 (0x39ee7)
   > gas price:           20 gwei
   > value sent:          0 ETH
   > total cost:          0.00474574 ETH


   > Saving migration to chain.
   > Saving artifacts
   -------------------------------------
   > Total cost:          0.03640376 ETH


8_deploy_lock_manager.js
========================

   Deploying 'InvestorLockManager'
   -------------------------------
   > transaction hash:    0x4adde7f78548c57ff3a266b99c219010b5f653081b27daeac14b21701858e8db
   > Blocks: 0            Seconds: 0
   > contract address:    0x9914CaD363FBD399b6ee1C95173593fE15b317F6
   > block number:        5676
   > block timestamp:     1611890040
   > account:             0x7A4479DeD8b545C2Bc74f9716b72b69B63913534
   > balance:             9999999999.39481186
   > gas used:            3765513 (0x397509)
   > gas price:           20 gwei
   > value sent:          0 ETH
   > total cost:          0.07531026 ETH


   Replacing 'Proxy'
   -----------------
   > transaction hash:    0x12665956b3d5ae07a7bfc34596c40c25f2a7b2938236b9a57597bff33b2a9acf
   > Blocks: 0            Seconds: 0
   > contract address:    0x2Ac1632aA277005988366CB684ccd095111F48E0
   > block number:        5677
   > block timestamp:     1611890041
   > account:             0x7A4479DeD8b545C2Bc74f9716b72b69B63913534
   > balance:             9999999999.39006612
   > gas used:            237287 (0x39ee7)
   > gas price:           20 gwei
   > value sent:          0 ETH
   > total cost:          0.00474574 ETH


   > Saving migration to chain.
   > Saving artifacts
   -------------------------------------
   > Total cost:            0.080056 ETH


9_deploy_partitions_manager.js
==============================

   > Saving migration to chain.
   -------------------------------------
   > Total cost:                   0 ETH


10_deploy_token.js
==================

   Deploying 'DSToken'
   -------------------
   > transaction hash:    0x6d985028885e4e14469ccaf5905e440382a1eeeba7a36de9bc04815fe6b6a293
   > Blocks: 0            Seconds: 0
   > contract address:    0x309a5fe7A5C369342e51C37212a4633E4B02F4B3
   > block number:        5682
   > block timestamp:     1611890042
   > account:             0x7A4479DeD8b545C2Bc74f9716b72b69B63913534
   > balance:             9999999999.30148802
   > gas used:            4170936 (0x3fa4b8)
   > gas price:           20 gwei
   > value sent:          0 ETH
   > total cost:          0.08341872 ETH


   Replacing 'Proxy'
   -----------------
   > transaction hash:    0x25425fea7e88acbedb41ec69266faf91fa45107bc9fce728303ff942c74e9a2d
   > Blocks: 0            Seconds: 0
   > contract address:    0x49ee48C26F5D362f3dd186dF7C2f5005e9c316f0
   > block number:        5683
   > block timestamp:     1611890042
   > account:             0x7A4479DeD8b545C2Bc74f9716b72b69B63913534
   > balance:             9999999999.29674228
   > gas used:            237287 (0x39ee7)
   > gas price:           20 gwei
   > value sent:          0 ETH
   > total cost:          0.00474574 ETH


   > Saving migration to chain.
   > Saving artifacts
   -------------------------------------
   > Total cost:          0.08816446 ETH


11_deploy_token_issuer.js
=========================

   Deploying 'TokenIssuer'
   -----------------------
   > transaction hash:    0x36331baabc8f00ee5c210946e22c135aca9ba89053360eb78833b7f50cf39591
   > Blocks: 0            Seconds: 0
   > contract address:    0x195656cCb84CDA1fa53CB2c3960DB626b3dD27e9
   > block number:        5687
   > block timestamp:     1611890043
   > account:             0x7A4479DeD8b545C2Bc74f9716b72b69B63913534
   > balance:             9999999999.25374294
   > gas used:            1851731 (0x1c4153)
   > gas price:           20 gwei
   > value sent:          0 ETH
   > total cost:          0.03703462 ETH


   Replacing 'Proxy'
   -----------------
   > transaction hash:    0xf0dc3586a38bfd68508a3f8b6269362cb8bfd6fb7ba8dcd0566465913e05b019
   > Blocks: 0            Seconds: 0
   > contract address:    0xCd33449Ac85c28108a40BF7923b0167eF39834dc
   > block number:        5688
   > block timestamp:     1611890043
   > account:             0x7A4479DeD8b545C2Bc74f9716b72b69B63913534
   > balance:             9999999999.2489972
   > gas used:            237287 (0x39ee7)
   > gas price:           20 gwei
   > value sent:          0 ETH
   > total cost:          0.00474574 ETH


   > Saving migration to chain.
   > Saving artifacts
   -------------------------------------
   > Total cost:          0.04178036 ETH


12_deploy_omnibus_wallet_controller.js
======================================

   Deploying 'OmnibusTBEController'
   --------------------------------
   > transaction hash:    0xac858c3d24651ec978b7a3b194ee7dc9e1ac1d14764512eb19ec6256c4687794
   > Blocks: 0            Seconds: 0
   > contract address:    0xB414F119774a28073eECD77d2a94665758baD448
   > block number:        5692
   > block timestamp:     1611890044
   > account:             0x7A4479DeD8b545C2Bc74f9716b72b69B63913534
   > balance:             9999999999.18583824
   > gas used:            2927301 (0x2caac5)
   > gas price:           20 gwei
   > value sent:          0 ETH
   > total cost:          0.05854602 ETH


   Replacing 'Proxy'
   -----------------
   > transaction hash:    0x50d5b19ede0fb998049c91ed5a41cee8dd84c655352698b136be4933baf9cd88
   > Blocks: 0            Seconds: 0
   > contract address:    0x74709E40235ce2EA2263ad10071C6333A6a1A3ff
   > block number:        5693
   > block timestamp:     1611890044
   > account:             0x7A4479DeD8b545C2Bc74f9716b72b69B63913534
   > balance:             9999999999.1810925
   > gas used:            237287 (0x39ee7)
   > gas price:           20 gwei
   > value sent:          0 ETH
   > total cost:          0.00474574 ETH


   > Saving migration to chain.
   > Saving artifacts
   -------------------------------------
   > Total cost:          0.06329176 ETH


13_deploy_multisigwallet.js
===========================

   Deploying 'MultiSigWallet'
   --------------------------
   > transaction hash:    0x120422eb989825b4b839f4861890f8c871a0aeaaccf16b56c5692e4dfff424ee
   > Blocks: 0            Seconds: 0
   > contract address:    0x08A4D433e7DB187b2F36788c4c7F2c7d9Ff6c2A7
   > block number:        5697
   > block timestamp:     1611890045
   > account:             0x7A4479DeD8b545C2Bc74f9716b72b69B63913534
   > balance:             9999999999.16190942
   > gas used:            728749 (0xb1ead)
   > gas price:           20 gwei
   > value sent:          0 ETH
   > total cost:          0.01457498 ETH


   > Saving migration to chain.
   > Saving artifacts
   -------------------------------------
   > Total cost:          0.01457498 ETH


14_deploy_wallet_registrar.js
=============================

   Deploying 'WalletRegistrar'
   ---------------------------
   > transaction hash:    0xc1cf59378dc1ad0df05b7904b56e0f241dbe5a749e3ba2f4b547a35e70ba07f1
   > Blocks: 0            Seconds: 0
   > contract address:    0x3E055C9ab9F2AA8c1073C0c8f181bD71718173E8
   > block number:        5699
   > block timestamp:     1611890046
   > account:             0x7A4479DeD8b545C2Bc74f9716b72b69B63913534
   > balance:             9999999999.13297298
   > gas used:            1419521 (0x15a901)
   > gas price:           20 gwei
   > value sent:          0 ETH
   > total cost:          0.02839042 ETH


   Replacing 'Proxy'
   -----------------
   > transaction hash:    0xf6474321e7e4f8c80581d6320721d5a2ac1bde807bdd94eb2e1ae14947d25cfe
   > Blocks: 0            Seconds: 0
   > contract address:    0x37959Ff1fB35e2902B346e3a5Ca14b243d6D23bd
   > block number:        5700
   > block timestamp:     1611890046
   > account:             0x7A4479DeD8b545C2Bc74f9716b72b69B63913534
   > balance:             9999999999.12822724
   > gas used:            237287 (0x39ee7)
   > gas price:           20 gwei
   > value sent:          0 ETH
   > total cost:          0.00474574 ETH


   > Saving migration to chain.
   > Saving artifacts
   -------------------------------------
   > Total cost:          0.03313616 ETH


15_set_roles.js
===============
Give issuer permissions to token issuer
Give issuer permissions to wallet registrar
Give issuer permissions to Omnibus TBE Controller

   > Saving migration to chain.
   -------------------------------------
   > Total cost:                   0 ETH


16_set_services.js
==================
Connecting compliance configuration to trust service
Connecting compliance manager to trust service
Connecting compliance manager to compliance configuration service
Connecting compliance manager to wallet manager
Connecting compliance manager to lock manager
Connecting compliance service to token
Connecting registry to trust service
Connecting registry to wallet manager
Connecting registry to token
Connecting registry to compliance service
Connecting token to registry
Connecting token to token issuer
Connecting token to wallet registrar
Connecting token issuer to registry
Connecting wallet registrar to registry
Connecting wallet manager to registry
Connecting lock manager to registry
Connecting compliance manager to registry
Connecting token to omnibus TBE controller
Connecting compliance manager to TBE controller
Connecting TBE controller to token
Connecting Omnibus TBE Controller to compliance configuration service
Connecting Omnibus TBE Controller to trust service
Connecting Omnibus TBE Controller to compliance service
Connecting token to trust service
Connecting token to compliance service
Connecting token to compliance configuration service
Connecting token to wallet manager
Connecting token to lock manager
Connecting wallet manager to trust service
Connecting lock manager to trust service
Connecting lock manager to compliance service
Connecting lock manager to token
Connecting token issuer to trust service
Connecting token issuer to lock manager
Connecting token issuer to token
Connecting wallet registrar to trust service
Set omnibus wallet as Platform wallet


Token "NewToken" (NewToken) [decimals: 0] deployment complete
-------------------------
Token is at address: 0x49ee48C26F5D362f3dd186dF7C2f5005e9c316f0 | Version: 3,5,6,5,5
Trust service is at address: 0x06C729e4f59360FF273D3C2498F311a5838725Ec | Version: 3,3
Investor registry is at address: 0xb08F3664f5661b63E46dAb6E6c6AFD3ad07581c5 | Version: 4,5,6,4
Omnibus TBE controller is at address: 0x74709E40235ce2EA2263ad10071C6333A6a1A3ff | Version: 3,5,6
Compliance service is at address: 0xaA8883f94cB2DD3145D6C38c0D3AC508398BD52D, and is of type NORMAL | Version: 5,5,6,5,3,10
Compliance configuration service is at address: 0x90409Aa7A70d7C5e73b36710131DFCB043A6e242 | Version: 5,5,6,6
Wallet manager is at address: 0x89d9c902381DCa10A4FbE1Fe2FAB65Eca2dD74A8 | Version: 3,5,6,3
Lock manager is at address: 0x2Ac1632aA277005988366CB684ccd095111F48E0, and is of type INVESTOR. | Version: 3,5,6,3
Token issuer is at address: 0xCd33449Ac85c28108a40BF7923b0167eF39834dc | Version: 3,5,6,3
Wallet registrar is at address: 0x37959Ff1fB35e2902B346e3a5Ca14b243d6D23bd | Version: 3,5,6,3
Multisig wallet is at address: 0x08A4D433e7DB187b2F36788c4c7F2c7d9Ff6c2A7 | Version: 1



   > Saving migration to chain.
   -------------------------------------
   > Total cost:                   0 ETH


Summary
=======
> Total deployments:   24
> Final cost:          0.8259556 ETH

```

Finnally we check proxy targets and its storage of DSToken and ComplianceService.

```bash
diego-securitize@heavy-machine:~/src/dstoken$ npx truffle console --network development
truffle(development)> const newToken = await DSToken.at('0x49ee48C26F5D362f3dd186dF7C2f5005e9c316f0')
undefined
truffle(development)> await newToken.name.call()
'NewToken'
truffle(development)> await newToken.owner.call()
'0x7A4479DeD8b545C2Bc74f9716b72b69B63913534'
truffle(development)> const proxyNewToken = await Proxy.at('0x49ee48C26F5D362f3dd186dF7C2f5005e9c316f0')
undefined
truffle(development)> await proxyNewToken.target.call()
'0x309a5fe7A5C369342e51C37212a4633E4B02F4B3'
truffle(development)> web3.eth.getStorageAt('0x309a5fe7A5C369342e51C37212a4633E4B02F4B3', 0)
'0x0'
truffle(development)> web3.eth.getStorageAt('0x309a5fe7A5C369342e51C37212a4633E4B02F4B3', 1)
'0x0'
truffle(development)> web3.eth.getStorageAt('0x309a5fe7A5C369342e51C37212a4633E4B02F4B3', 2)
'0x01'
truffle(development)> web3.eth.getStorageAt('0x309a5fe7A5C369342e51C37212a4633E4B02F4B3', 3)
'0x0'
truffle(development)> web3.eth.getStorageAt('0x309a5fe7A5C369342e51C37212a4633E4B02F4B3', 4)
'0x0'
truffle(development)> web3.eth.getStorageAt('0x309a5fe7A5C369342e51C37212a4633E4B02F4B3', 5)
'0x0'
truffle(development)> web3.eth.getStorageAt('0x309a5fe7A5C369342e51C37212a4633E4B02F4B3', 6)
'0x0'
truffle(development)> web3.eth.getStorageAt('0x309a5fe7A5C369342e51C37212a4633E4B02F4B3', 7)
'0x0'
truffle(development)> web3.eth.getStorageAt('0x309a5fe7A5C369342e51C37212a4633E4B02F4B3', 8)
'0x0'
truffle(development)> web3.eth.getStorageAt('0x309a5fe7A5C369342e51C37212a4633E4B02F4B3', 9)
'0x0'
truffle(development)> web3.eth.getStorageAt('0x309a5fe7A5C369342e51C37212a4633E4B02F4B3', 10)
'0x0'
truffle(development)> web3.eth.getStorageAt('0x309a5fe7A5C369342e51C37212a4633E4B02F4B3', 11)
'0x0'
truffle(development)> web3.eth.getStorageAt('0x309a5fe7A5C369342e51C37212a4633E4B02F4B3', 12)
'0x0'
```

