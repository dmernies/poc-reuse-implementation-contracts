# Deploying only implementation contract

In this case we created a branch that alows deploy only implementation contracts.

```bash
diego-securitize@heavy-machine:~/src/dstoken$ truffle migrate --name OnlyImplToken --symbol NewToken --decimals 0 --network development --owners "0x7A4479DeD8b545C2Bc74f9716b72b69B63913534 0x51f0c9842F12d7E32077d5A69a95D8c656F5cc02" --required_confirmations 2 --omnibus_wallet 0x275B0d8f14a77DceDB4A2b7cD4532a39C9AAb2A4
> Warning: possible unsupported (undocumented in help) command line option: --name,--symbol,--decimals,--owners,--required_confirmations,--omnibus_wallet

Compiling your contracts...
===========================
✔ Fetching solc version list from solc-bin. Attempt #1
> Compiling ./contracts/compliance/ComplianceConfigurationService.sol
> Compiling ./contracts/compliance/ComplianceService.sol
> Compiling ./contracts/compliance/ComplianceServiceNotRegulated.sol
> Compiling ./contracts/compliance/ComplianceServiceRegulated.sol
> Compiling ./contracts/compliance/ComplianceServiceRegulatedPartitioned.sol
> Compiling ./contracts/compliance/ComplianceServiceWhitelisted.sol
> Compiling ./contracts/compliance/IDSComplianceConfigurationService.sol
> Compiling ./contracts/compliance/IDSComplianceService.sol
> Compiling ./contracts/compliance/IDSComplianceServicePartitioned.sol
> Compiling ./contracts/compliance/IDSLockManager.sol
> Compiling ./contracts/compliance/IDSLockManagerPartitioned.sol
> Compiling ./contracts/compliance/IDSPartitionsManager.sol
> Compiling ./contracts/compliance/IDSWalletManager.sol
> Compiling ./contracts/compliance/InvestorLockManager.sol
> Compiling ./contracts/compliance/InvestorLockManagerBase.sol
> Compiling ./contracts/compliance/InvestorLockManagerPartitioned.sol
> Compiling ./contracts/compliance/LockManager.sol
> Compiling ./contracts/compliance/PartitionsManager.sol
> Compiling ./contracts/compliance/WalletManager.sol
> Compiling ./contracts/data-stores/BaseLockManagerDataStore.sol
> Compiling ./contracts/data-stores/ComplianceConfigurationDataStore.sol
> Compiling ./contracts/data-stores/ComplianceServiceDataStore.sol
> Compiling ./contracts/data-stores/InvestorLockManagerDataStore.sol
> Compiling ./contracts/data-stores/LockManagerDataStore.sol
> Compiling ./contracts/data-stores/OmnibusControllerDataStore.sol
> Compiling ./contracts/data-stores/OmnibusTBEControllerDataStore.sol
> Compiling ./contracts/data-stores/PartitionsManagerDataStore.sol
> Compiling ./contracts/data-stores/RegistryServiceDataStore.sol
> Compiling ./contracts/data-stores/ServiceConsumerDataStore.sol
> Compiling ./contracts/data-stores/TokenDataStore.sol
> Compiling ./contracts/data-stores/TrustServiceDataStore.sol
> Compiling ./contracts/data-stores/WalletManagerDataStore.sol
> Compiling ./contracts/issuance/IDSTokenIssuer.sol
> Compiling ./contracts/issuance/TokenIssuer.sol
> Compiling ./contracts/mocks/StandardTokenMock.sol
> Compiling ./contracts/mocks/TestCalls.sol
> Compiling ./contracts/mocks/TestToken.sol
> Compiling ./contracts/omnibus/IDSOmnibusTBEController.sol
> Compiling ./contracts/omnibus/IDSOmnibusWalletController.sol
> Compiling ./contracts/omnibus/OmnibusTBEController.sol
> Compiling ./contracts/omnibus/OmnibusWalletController.sol
> Compiling ./contracts/registry/IDSRegistryService.sol
> Compiling ./contracts/registry/IDSWalletRegistrar.sol
> Compiling ./contracts/registry/RegistryService.sol
> Compiling ./contracts/registry/WalletRegistrar.sol
> Compiling ./contracts/service/IDSServiceConsumer.sol
> Compiling ./contracts/service/ServiceConsumer.sol
> Compiling ./contracts/token/DSToken.sol
> Compiling ./contracts/token/DSTokenPartitioned.sol
> Compiling ./contracts/token/IDSToken.sol
> Compiling ./contracts/token/IDSTokenPartitioned.sol
> Compiling ./contracts/token/StandardToken.sol
> Compiling ./contracts/token/TokenLibrary.sol
> Compiling ./contracts/token/TokenPartitionsLibrary.sol
> Compiling ./contracts/trust/IDSTrustService.sol
> Compiling ./contracts/trust/TrustService.sol
> Compiling ./contracts/utils/CommonUtils.sol
> Compiling ./contracts/utils/Initializable.sol
> Compiling ./contracts/utils/Migrations.sol
> Compiling ./contracts/utils/MultiSigWallet.sol
> Compiling ./contracts/utils/Ownable.sol
> Compiling ./contracts/utils/Proxy.sol
> Compiling ./contracts/utils/ProxyTarget.sol
> Compiling ./contracts/utils/VersionedContract.sol
> Compiling ./contracts/zeppelin/GSN/Context.sol
> Compiling ./contracts/zeppelin/GSN/GSNRecipient.sol
> Compiling ./contracts/zeppelin/GSN/GSNRecipientERC20Fee.sol
> Compiling ./contracts/zeppelin/GSN/GSNRecipientSignature.sol
> Compiling ./contracts/zeppelin/GSN/IRelayHub.sol
> Compiling ./contracts/zeppelin/GSN/IRelayRecipient.sol
> Compiling ./contracts/zeppelin/access/Roles.sol
> Compiling ./contracts/zeppelin/access/roles/CapperRole.sol
> Compiling ./contracts/zeppelin/access/roles/MinterRole.sol
> Compiling ./contracts/zeppelin/access/roles/PauserRole.sol
> Compiling ./contracts/zeppelin/access/roles/SignerRole.sol
> Compiling ./contracts/zeppelin/access/roles/WhitelistAdminRole.sol
> Compiling ./contracts/zeppelin/access/roles/WhitelistedRole.sol
> Compiling ./contracts/zeppelin/cryptography/ECDSA.sol
> Compiling ./contracts/zeppelin/cryptography/MerkleProof.sol
> Compiling ./contracts/zeppelin/drafts/Counters.sol
> Compiling ./contracts/zeppelin/drafts/ERC1046/ERC20Metadata.sol
> Compiling ./contracts/zeppelin/drafts/ERC20Migrator.sol
> Compiling ./contracts/zeppelin/drafts/ERC20Snapshot.sol
> Compiling ./contracts/zeppelin/drafts/SignedSafeMath.sol
> Compiling ./contracts/zeppelin/drafts/Strings.sol
> Compiling ./contracts/zeppelin/drafts/TokenVesting.sol
> Compiling ./contracts/zeppelin/introspection/ERC165.sol
> Compiling ./contracts/zeppelin/introspection/ERC165Checker.sol
> Compiling ./contracts/zeppelin/introspection/ERC1820Implementer.sol
> Compiling ./contracts/zeppelin/introspection/IERC165.sol
> Compiling ./contracts/zeppelin/introspection/IERC1820Implementer.sol
> Compiling ./contracts/zeppelin/introspection/IERC1820Registry.sol
> Compiling ./contracts/zeppelin/lifecycle/Pausable.sol
> Compiling ./contracts/zeppelin/math/Math.sol
> Compiling ./contracts/zeppelin/math/SafeMath.sol
> Compiling ./contracts/zeppelin/ownership/Ownable.sol
> Compiling ./contracts/zeppelin/ownership/Secondary.sol
> Compiling ./contracts/zeppelin/token/ERC20/ERC20.sol
> Compiling ./contracts/zeppelin/token/ERC20/ERC20Burnable.sol
> Compiling ./contracts/zeppelin/token/ERC20/ERC20Capped.sol
> Compiling ./contracts/zeppelin/token/ERC20/ERC20Detailed.sol
> Compiling ./contracts/zeppelin/token/ERC20/ERC20Mintable.sol
> Compiling ./contracts/zeppelin/token/ERC20/ERC20Pausable.sol
> Compiling ./contracts/zeppelin/token/ERC20/IERC20.sol
> Compiling ./contracts/zeppelin/token/ERC20/SafeERC20.sol
> Compiling ./contracts/zeppelin/token/ERC20/TokenTimelock.sol
> Compiling ./contracts/zeppelin/token/ERC721/ERC721.sol
> Compiling ./contracts/zeppelin/token/ERC721/ERC721Burnable.sol
> Compiling ./contracts/zeppelin/token/ERC721/ERC721Enumerable.sol
> Compiling ./contracts/zeppelin/token/ERC721/ERC721Full.sol
> Compiling ./contracts/zeppelin/token/ERC721/ERC721Holder.sol
> Compiling ./contracts/zeppelin/token/ERC721/ERC721Metadata.sol
> Compiling ./contracts/zeppelin/token/ERC721/ERC721MetadataMintable.sol
> Compiling ./contracts/zeppelin/token/ERC721/ERC721Mintable.sol
> Compiling ./contracts/zeppelin/token/ERC721/ERC721Pausable.sol
> Compiling ./contracts/zeppelin/token/ERC721/IERC721.sol
> Compiling ./contracts/zeppelin/token/ERC721/IERC721Enumerable.sol
> Compiling ./contracts/zeppelin/token/ERC721/IERC721Full.sol
> Compiling ./contracts/zeppelin/token/ERC721/IERC721Metadata.sol
> Compiling ./contracts/zeppelin/token/ERC721/IERC721Receiver.sol
> Compiling ./contracts/zeppelin/token/ERC777/ERC777.sol
> Compiling ./contracts/zeppelin/token/ERC777/IERC777.sol
> Compiling ./contracts/zeppelin/token/ERC777/IERC777Recipient.sol
> Compiling ./contracts/zeppelin/token/ERC777/IERC777Sender.sol
> Compiling ./contracts/zeppelin/utils/Address.sol
> Compiling ./contracts/zeppelin/utils/Arrays.sol
> Compiling ./contracts/zeppelin/utils/ReentrancyGuard.sol
✔ Fetching solc version list from solc-bin. Attempt #1
> Artifacts written to /home/diego-securitize/src/dstoken/build/contracts
> Compiled successfully using:
   - solc: 0.5.17+commit.d19bba13.Emscripten.clang


> Duplicate contract names found for Ownable.
> This can cause errors and unknown behavior. Please rename one of your contracts.


Starting migrations...
======================
> Network name:    'development'
> Network id:      5777
> Block gas limit: 6721975 (0x6691b7)


1_initial_migration.js
======================

   Deploying 'Migrations'
   ----------------------
   > transaction hash:    0xf524fecf280aec58e1ae8f880891d3b2fa793a7b9d1faabe2d3dd3bc26c9ea3c
   > Blocks: 0            Seconds: 0
   > contract address:    0x6c54D15606359D7728CfF67Ed797BB82e3D694A0
   > block number:        5932
   > block timestamp:     1611929430
   > account:             0x33cC6c1c002311D26B561e1C8df3466F62bAe1f9
   > balance:             9999999757.070732504
   > gas used:            274286 (0x42f6e)
   > gas price:           20 gwei
   > value sent:          0 ETH
   > total cost:          0.00548572 ETH


   > Saving migration to chain.
   > Saving artifacts
   -------------------------------------
   > Total cost:          0.00548572 ETH


2_deploy_libraries.js
=====================
{
  _: [ 'migrate' ],
  name: 'OnlyImplToken',
  symbol: 'NewToken',
  decimals: 0,
  network: 'development',
  owners: '0x7A4479DeD8b545C2Bc74f9716b72b69B63913534 0x51f0c9842F12d7E32077d5A69a95D8c656F5cc02',
  required_confirmations: 2,
  omnibus_wallet: '0x275B0d8f14a77DceDB4A2b7cD4532a39C9AAb2A4'
}

   Deploying 'ComplianceServiceLibrary'
   ------------------------------------
   > transaction hash:    0x4cc8e60ed683d2c754ca424a713ac308d648e3154e9add52700267e2801d144a
   > Blocks: 0            Seconds: 0
   > contract address:    0xB81867BeaD4264992bDAB4a0297433450428dcaf
   > block number:        5934
   > block timestamp:     1611929431
   > account:             0x33cC6c1c002311D26B561e1C8df3466F62bAe1f9
   > balance:             9999999756.966808564
   > gas used:            5153896 (0x4ea468)
   > gas price:           20 gwei
   > value sent:          0 ETH
   > total cost:          0.10307792 ETH


   Deploying 'TokenLibrary'
   ------------------------
   > transaction hash:    0x54a8dc902459bd858ebadc2d8ac73797ed0d73138824548a79c5e0742d7d5328
   > Blocks: 0            Seconds: 0
   > contract address:    0xa85a38491eeD48b692950F3469EDc99789775154
   > block number:        5935
   > block timestamp:     1611929432
   > account:             0x33cC6c1c002311D26B561e1C8df3466F62bAe1f9
   > balance:             9999999756.930489524
   > gas used:            1815952 (0x1bb590)
   > gas price:           20 gwei
   > value sent:          0 ETH
   > total cost:          0.03631904 ETH


   Linking
   -------
   * Contract: ComplianceServiceRegulated <--> Library: ComplianceServiceLibrary (at address: 0xB81867BeaD4264992bDAB4a0297433450428dcaf)

   Linking
   -------
   * Contract: DSToken <--> Library: TokenLibrary (at address: 0xa85a38491eeD48b692950F3469EDc99789775154)

   > Saving migration to chain.
   > Saving artifacts
   -------------------------------------
   > Total cost:          0.13939696 ETH


3_deploy_trust_manager.js
=========================

   Deploying 'TrustService'
   ------------------------
   > transaction hash:    0xe3572fb0179ccbe9a8889593090cc3fbc1f22f28121f38baef7791edc54cb5e7
   > Blocks: 0            Seconds: 0
   > contract address:    0xdF0F9816CcDE7Bd353Ee9C0D63F95A8545d40F3a
   > block number:        5937
   > block timestamp:     1611929433
   > account:             0x33cC6c1c002311D26B561e1C8df3466F62bAe1f9
   > balance:             9999999756.889217484
   > gas used:            2036301 (0x1f124d)
   > gas price:           20 gwei
   > value sent:          0 ETH
   > total cost:          0.04072602 ETH


   > Saving migration to chain.
   > Saving artifacts
   -------------------------------------
   > Total cost:          0.04072602 ETH


4_deploy_registry.js
====================

   Deploying 'RegistryService'
   ---------------------------
   > transaction hash:    0x1c5f18b27ba38e5ec91c2438c8c98849350f3f2b8cde9c1d2bffd17d2f4fd1c0
   > Blocks: 0            Seconds: 0
   > contract address:    0xC840bc3Fb0F52D11219a04A94f20525bFfb1093C
   > block number:        5939
   > block timestamp:     1611929434
   > account:             0x33cC6c1c002311D26B561e1C8df3466F62bAe1f9
   > balance:             9999999756.792044044
   > gas used:            4831371 (0x49b88b)
   > gas price:           20 gwei
   > value sent:          0 ETH
   > total cost:          0.09662742 ETH


   > Saving migration to chain.
   > Saving artifacts
   -------------------------------------
   > Total cost:          0.09662742 ETH


5_deploy_compliance_manager.js
==============================

   Deploying 'ComplianceServiceRegulated'
   --------------------------------------
   > transaction hash:    0xfb7588b6d7c156c180d98ba2b6b093a14fce150cf2d603a3d69d51c63fc4b38e
   > Blocks: 0            Seconds: 0
   > contract address:    0xbf8eE4FcA6B27c7cB44303EC7B8eFF6Fa39fcf69
   > block number:        5941
   > block timestamp:     1611929434
   > account:             0x33cC6c1c002311D26B561e1C8df3466F62bAe1f9
   > balance:             9999999756.696732364
   > gas used:            4738283 (0x484ceb)
   > gas price:           20 gwei
   > value sent:          0 ETH
   > total cost:          0.09476566 ETH


   > Saving migration to chain.
   > Saving artifacts
   -------------------------------------
   > Total cost:          0.09476566 ETH


6_deploy_complianceConfiguration.js
===================================

   Deploying 'ComplianceConfigurationService'
   ------------------------------------------
   > transaction hash:    0xa9f19b381e7768227cab8f5edc5eca4d3878e9c43f815619a02a72acde7f8ce3
   > Blocks: 0            Seconds: 0
   > contract address:    0xf61002017Bdf9825b30d6466421971F542Ff971F
   > block number:        5943
   > block timestamp:     1611929435
   > account:             0x33cC6c1c002311D26B561e1C8df3466F62bAe1f9
   > balance:             9999999756.623622964
   > gas used:            3628169 (0x375c89)
   > gas price:           20 gwei
   > value sent:          0 ETH
   > total cost:          0.07256338 ETH


   > Saving migration to chain.
   > Saving artifacts
   -------------------------------------
   > Total cost:          0.07256338 ETH


7_deploy_wallet_manager.js
==========================

   Deploying 'WalletManager'
   -------------------------
   > transaction hash:    0xeb26e3756fdf70c044f0a2efe8157049f79513dc8c961f28c9a5f686384021fb
   > Blocks: 0            Seconds: 0
   > contract address:    0x83eCCbC5295c948D571C25721dA431FBDDF14747
   > block number:        5945
   > block timestamp:     1611929436
   > account:             0x33cC6c1c002311D26B561e1C8df3466F62bAe1f9
   > balance:             9999999756.591418924
   > gas used:            1582901 (0x182735)
   > gas price:           20 gwei
   > value sent:          0 ETH
   > total cost:          0.03165802 ETH


   > Saving migration to chain.
   > Saving artifacts
   -------------------------------------
   > Total cost:          0.03165802 ETH


8_deploy_lock_manager.js
========================

   Deploying 'InvestorLockManager'
   -------------------------------
   > transaction hash:    0xeb1dc826576b7837b17e20e6f954d3bd3c21f2e2e4420676a5e70dea894aa2cf
   > Blocks: 0            Seconds: 0
   > contract address:    0x6Fc2e74DaeEE98b44a745797548a3570b2D327C4
   > block number:        5947
   > block timestamp:     1611929437
   > account:             0x33cC6c1c002311D26B561e1C8df3466F62bAe1f9
   > balance:             9999999756.515562644
   > gas used:            3765513 (0x397509)
   > gas price:           20 gwei
   > value sent:          0 ETH
   > total cost:          0.07531026 ETH


   > Saving migration to chain.
   > Saving artifacts
   -------------------------------------
   > Total cost:          0.07531026 ETH


9_deploy_partitions_manager.js
==============================

   > Saving migration to chain.
   -------------------------------------
   > Total cost:                   0 ETH


10_deploy_token.js
==================

   Deploying 'DSToken'
   -------------------
   > transaction hash:    0x459f1b5f93dfaed8c5d886f988d5415639ed11384bb1a660c94ed07863067b33
   > Blocks: 0            Seconds: 0
   > contract address:    0x5ea8eC93548dF3579650d2Ea375c6b5Aa8Bc9cFA
   > block number:        5950
   > block timestamp:     1611929438
   > account:             0x33cC6c1c002311D26B561e1C8df3466F62bAe1f9
   > balance:             9999999756.431051884
   > gas used:            4170936 (0x3fa4b8)
   > gas price:           20 gwei
   > value sent:          0 ETH
   > total cost:          0.08341872 ETH


   > Saving migration to chain.
   > Saving artifacts
   -------------------------------------
   > Total cost:          0.08341872 ETH


11_deploy_token_issuer.js
=========================

   Deploying 'TokenIssuer'
   -----------------------
   > transaction hash:    0x973faea5f3d5d7c8b6ec93064fc14fccfb265924b8ed0a8c39df9c1fb59a120b
   > Blocks: 0            Seconds: 0
   > contract address:    0x269a5fecFbBD8b3f1D85E1A961167443eb1E445c
   > block number:        5952
   > block timestamp:     1611929439
   > account:             0x33cC6c1c002311D26B561e1C8df3466F62bAe1f9
   > balance:             9999999756.393471244
   > gas used:            1851731 (0x1c4153)
   > gas price:           20 gwei
   > value sent:          0 ETH
   > total cost:          0.03703462 ETH


   > Saving migration to chain.
   > Saving artifacts
   -------------------------------------
   > Total cost:          0.03703462 ETH


12_deploy_omnibus_wallet_controller.js
======================================

   Deploying 'OmnibusTBEController'
   --------------------------------
   > transaction hash:    0xfd448c79a7c70882776a75bbccd6ad089f1ec12b410c3ad66d3d4598e279b59b
   > Blocks: 0            Seconds: 0
   > contract address:    0x5b2aF50f07dC703f23E0553fbDc805EdEcc080A2
   > block number:        5954
   > block timestamp:     1611929440
   > account:             0x33cC6c1c002311D26B561e1C8df3466F62bAe1f9
   > balance:             9999999756.334379204
   > gas used:            2927301 (0x2caac5)
   > gas price:           20 gwei
   > value sent:          0 ETH
   > total cost:          0.05854602 ETH


   > Saving migration to chain.
   > Saving artifacts
   -------------------------------------
   > Total cost:          0.05854602 ETH


13_deploy_multisigwallet.js
===========================

   Deploying 'MultiSigWallet'
   --------------------------
   > transaction hash:    0x4c82883dd0a752d662735c91c1e02838ec1a3d998ccfb898d8cb36da3548f00d
   > Blocks: 0            Seconds: 0
   > contract address:    0xf17021AE09AC6CeC49A336703cB22B281a062880
   > block number:        5956
   > block timestamp:     1611929441
   > account:             0x33cC6c1c002311D26B561e1C8df3466F62bAe1f9
   > balance:             9999999756.319258204
   > gas used:            728749 (0xb1ead)
   > gas price:           20 gwei
   > value sent:          0 ETH
   > total cost:          0.01457498 ETH


   > Saving migration to chain.
   > Saving artifacts
   -------------------------------------
   > Total cost:          0.01457498 ETH


14_deploy_wallet_registrar.js
=============================

   Deploying 'WalletRegistrar'
   ---------------------------
   > transaction hash:    0x5306670884a01b3603881b01101a32d58c30267967ee9d58a716a8b3fc17cec2
   > Blocks: 0            Seconds: 0
   > contract address:    0x77BE3d115E82aA1804ac38F87Fe51B1dd61C49f3
   > block number:        5958
   > block timestamp:     1611929442
   > account:             0x33cC6c1c002311D26B561e1C8df3466F62bAe1f9
   > balance:             9999999756.290321764
   > gas used:            1419521 (0x15a901)
   > gas price:           20 gwei
   > value sent:          0 ETH
   > total cost:          0.02839042 ETH


   > Saving migration to chain.
   > Saving artifacts
   -------------------------------------
   > Total cost:          0.02839042 ETH


Summary
=======
> Total deployments:   14
> Final cost:          0.7784982 ETH
```