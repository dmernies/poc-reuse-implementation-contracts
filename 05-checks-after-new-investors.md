# Checks unit test running

```bash
diego-securitize@heavy-machine:~/dsclient/master$ dsclient token:info
{"tokenParams":{"name":"PoC-Token-A01","symbol":"PoCTokA01","decimals":0,"totalIssued":"3186","totalWallets":13,"isPaused":false}}
diego-securitize@heavy-machine:~/dsclient/master$ dsclient token:totalSupply
1456
diego-securitize@heavy-machine:~/dsclient/master$ dsclient compliance:counters
{
  euRetailInvestorsCounts: [
    { countryCode: 'AT', count: 0 },
    { countryCode: 'BE', count: 0 },
    { countryCode: 'BG', count: 0 },
    { countryCode: 'HR', count: 0 },
    { countryCode: 'CY', count: 0 },
    { countryCode: 'CZ', count: 0 },
    { countryCode: 'DK', count: 0 },
    { countryCode: 'EE', count: 0 },
    { countryCode: 'FI', count: 0 },
    { countryCode: 'FR', count: 0 },
    { countryCode: 'DE', count: 0 },
    { countryCode: 'GR', count: 0 },
    { countryCode: 'HU', count: 0 },
    { countryCode: 'IE', count: 0 },
    { countryCode: 'IT', count: 0 },
    { countryCode: 'LV', count: 0 },
    { countryCode: 'LT', count: 0 },
    { countryCode: 'LU', count: 0 },
    { countryCode: 'MT', count: 0 },
    { countryCode: 'NL', count: 0 },
    { countryCode: 'PL', count: 0 },
    { countryCode: 'PT', count: 0 },
    { countryCode: 'RO', count: 0 },
    { countryCode: 'SK', count: 0 },
    { countryCode: 'SI', count: 0 },
    { countryCode: 'ES', count: 0 },
    { countryCode: 'SE', count: 0 },
    { countryCode: 'GB', count: 0 },
    { countryCode: 'IS', count: 0 },
    { countryCode: 'LI', count: 0 },
    { countryCode: 'NO', count: 0 },
    { countryCode: 'CH', count: 0 }
  ],
  usInvestorsCount: 2,
  totalInvestorsCount: 12,
  accreditedInvestorsCount: 8,
  usAccreditedInvestorsCount: 0,
  jpInvestorsCount: 0
}
diego-securitize@heavy-machine:~/dsclient/master$ dsclient compliance:getCountry US
us
diego-securitize@heavy-machine:~/dsclient/master$ dsclient compliance:info
{
  totalInvestorsLimit: 5000,
  minUSTokens: '0',
  minEUTokens: '0',
  usInvestorsLimit: 5000,
  euRetailInvestorsLimit: 150,
  jpInvestorsLimit: 0,
  usAccreditedInvestorsLimit: 5000,
  nonAccreditedInvestorsLimit: 5000,
  maxUSInvestorsPercentage: 100,
  blockFlowbackEndTime: 4000000000,
  nonUSLockPeriod: 0,
  minimumTotalInvestors: 0,
  minimumHoldingsPerInvestor: '0',
  maximumHoldingsPerInvestor: '5000',
  usLockPeriod: 31536000,
  forceFullTransfer: false,
  forceAccredited: false,
  forceAccreditedUS: true,
  worldWideForceFullTransfer: false
}
diego-securitize@heavy-machine:~/dsclient/master$ dsclient lock:list investor01
[
  { value: '10', reason: 'evasor', reasonCode: 0 },
  { value: '20', reason: 'evasor2', reasonCode: 0 }
]
diego-securitize@heavy-machine:~/dsclient/master$ dsclient lock:list investor02
[ { value: '50', reason: 'test-sucutrule', reasonCode: 0 } ]
diego-securitize@heavy-machine:~/dsclient/master$ dsclient lock:list investor03
[]
diego-securitize@heavy-machine:~/dsclient/master$
```

```bash
diego-securitize@heavy-machine:~/dsclient/master$ dsclient token:events 0
{
  fromBlock: 0,
  events: [
    {
      id: 'log_f2141e96',
      transactionId: '0x8f43a6e7aa0c5a25f95eb7ecfd546cfb22cd307c2949e89038260cecca148119',
      blockNumber: 4723,
      blockTime: 1611702791,
      eventType: 'DSTrustServiceRoleAdded',
      parameters: {
        targetAddress: '0xeCf7CbA7637Faf97F4CCddAde4C91D2FE69c13a8',
        role: '2',
        sender: '0x33cC6c1c002311D26B561e1C8df3466F62bAe1f9'
      }
    },
    {
      id: 'log_d2734000',
      transactionId: '0xc646a1d2467a8366eea747a71bfcb27ce037352797d28f49f0c7f86fb7aab827',
      blockNumber: 4724,
      blockTime: 1611702791,
      eventType: 'DSTrustServiceRoleAdded',
      parameters: {
        targetAddress: '0x634c4cAc02bf2168299A4834438CB05340EF15bc',
        role: '2',
        sender: '0x33cC6c1c002311D26B561e1C8df3466F62bAe1f9'
      }
    },
    {
      id: 'log_dd5bd4d6',
      transactionId: '0xfb065aa59cc4b30e9e8bb54e9f14db9986df5cd37ab79dd3ccbf9346cf5ae0cf',
      blockNumber: 4725,
      blockTime: 1611702791,
      eventType: 'DSTrustServiceRoleAdded',
      parameters: {
        targetAddress: '0x10CD7fD6d2c59EE5c2d42031a343F09CeBA12D06',
        role: '2',
        sender: '0x33cC6c1c002311D26B561e1C8df3466F62bAe1f9'
      }
    },
    {
      id: 'log_5a7c6ca8',
      transactionId: '0x50d987f769a0bc1e3e3815d3661dd6e17d37d8157542008096e6371c67bd7bbd',
      blockNumber: 4726,
      blockTime: 1611702792,
      eventType: 'DSTrustServiceRoleAdded',
      parameters: {
        targetAddress: '0x9414E4BeFF0Bd871f9fce17bB9F67f941c580594',
        role: '2',
        sender: '0x33cC6c1c002311D26B561e1C8df3466F62bAe1f9'
      }
    },
    {
      id: 'log_357cad34',
      transactionId: '0xd979ea4a0450ba2d622f467f7a401e292fba820d29d57c112d6aa619a4fb6a33',
      blockNumber: 4768,
      blockTime: 1611702796,
      eventType: 'DSComplianceUIntRuleSet',
      parameters: {
        ruleName: 'totalInvestorsLimit',
        prevValue: '0',
        newValue: '0'
      }
    },
    {
      id: 'log_9e66de06',
      transactionId: '0xd979ea4a0450ba2d622f467f7a401e292fba820d29d57c112d6aa619a4fb6a33',
      blockNumber: 4768,
      blockTime: 1611702796,
      eventType: 'DSComplianceUIntRuleSet',
      parameters: { ruleName: 'minUSTokens', prevValue: '0', newValue: '0' }
    },
    {
      id: 'log_b9604461',
      transactionId: '0xd979ea4a0450ba2d622f467f7a401e292fba820d29d57c112d6aa619a4fb6a33',
      blockNumber: 4768,
      blockTime: 1611702796,
      eventType: 'DSComplianceUIntRuleSet',
      parameters: { ruleName: 'minEUTokens', prevValue: '0', newValue: '0' }
    },
    {
      id: 'log_fa0cfe80',
      transactionId: '0xd979ea4a0450ba2d622f467f7a401e292fba820d29d57c112d6aa619a4fb6a33',
      blockNumber: 4768,
      blockTime: 1611702796,
      eventType: 'DSComplianceUIntRuleSet',
      parameters: { ruleName: 'usInvestorsLimit', prevValue: '0', newValue: '0' }
    },
    {
      id: 'log_eef2b98f',
      transactionId: '0xd979ea4a0450ba2d622f467f7a401e292fba820d29d57c112d6aa619a4fb6a33',
      blockNumber: 4768,
      blockTime: 1611702796,
      eventType: 'DSComplianceUIntRuleSet',
      parameters: {
        ruleName: 'usAccreditedInvestorsLimit',
        prevValue: '0',
        newValue: '0'
      }
    },
    {
      id: 'log_8b8c8635',
      transactionId: '0xd979ea4a0450ba2d622f467f7a401e292fba820d29d57c112d6aa619a4fb6a33',
      blockNumber: 4768,
      blockTime: 1611702796,
      eventType: 'DSComplianceUIntRuleSet',
      parameters: {
        ruleName: 'nonAccreditedInvestorsLimit',
        prevValue: '0',
        newValue: '0'
      }
    },
    {
      id: 'log_6a928455',
      transactionId: '0xd979ea4a0450ba2d622f467f7a401e292fba820d29d57c112d6aa619a4fb6a33',
      blockNumber: 4768,
      blockTime: 1611702796,
      eventType: 'DSComplianceUIntRuleSet',
      parameters: {
        ruleName: 'maxUSInvestorsPercentage',
        prevValue: '0',
        newValue: '0'
      }
    },
    {
      id: 'log_b29a0794',
      transactionId: '0xd979ea4a0450ba2d622f467f7a401e292fba820d29d57c112d6aa619a4fb6a33',
      blockNumber: 4768,
      blockTime: 1611702796,
      eventType: 'DSComplianceUIntRuleSet',
      parameters: {
        ruleName: 'blockFlowbackEndTime',
        prevValue: '0',
        newValue: '0'
      }
    },
    {
      id: 'log_eefd0efc',
      transactionId: '0xd979ea4a0450ba2d622f467f7a401e292fba820d29d57c112d6aa619a4fb6a33',
      blockNumber: 4768,
      blockTime: 1611702796,
      eventType: 'DSComplianceUIntRuleSet',
      parameters: { ruleName: 'nonUSLockPeriod', prevValue: '0', newValue: '0' }
    },
    {
      id: 'log_c2f9a5ef',
      transactionId: '0xd979ea4a0450ba2d622f467f7a401e292fba820d29d57c112d6aa619a4fb6a33',
      blockNumber: 4768,
      blockTime: 1611702796,
      eventType: 'DSComplianceUIntRuleSet',
      parameters: {
        ruleName: 'minimumTotalInvestors',
        prevValue: '0',
        newValue: '0'
      }
    },
    {
      id: 'log_ecb2a64b',
      transactionId: '0xd979ea4a0450ba2d622f467f7a401e292fba820d29d57c112d6aa619a4fb6a33',
      blockNumber: 4768,
      blockTime: 1611702796,
      eventType: 'DSComplianceUIntRuleSet',
      parameters: {
        ruleName: 'minimumHoldingsPerInvestor',
        prevValue: '0',
        newValue: '0'
      }
    },
    {
      id: 'log_4f4d8cb8',
      transactionId: '0xd979ea4a0450ba2d622f467f7a401e292fba820d29d57c112d6aa619a4fb6a33',
      blockNumber: 4768,
      blockTime: 1611702796,
      eventType: 'DSComplianceUIntRuleSet',
      parameters: {
        ruleName: 'maximumHoldingsPerInvestor',
        prevValue: '0',
        newValue: '0'
      }
    },
    {
      id: 'log_b8523fe8',
      transactionId: '0xd979ea4a0450ba2d622f467f7a401e292fba820d29d57c112d6aa619a4fb6a33',
      blockNumber: 4768,
      blockTime: 1611702796,
      eventType: 'DSComplianceUIntRuleSet',
      parameters: {
        ruleName: 'euRetailInvestorsLimit',
        prevValue: '0',
        newValue: '150'
      }
    },
    {
      id: 'log_dd3fbf0e',
      transactionId: '0xd979ea4a0450ba2d622f467f7a401e292fba820d29d57c112d6aa619a4fb6a33',
      blockNumber: 4768,
      blockTime: 1611702796,
      eventType: 'DSComplianceUIntRuleSet',
      parameters: {
        ruleName: 'usLockPeriod',
        prevValue: '0',
        newValue: '31536000'
      }
    },
    {
      id: 'log_4a6f9380',
      transactionId: '0xd979ea4a0450ba2d622f467f7a401e292fba820d29d57c112d6aa619a4fb6a33',
      blockNumber: 4768,
      blockTime: 1611702796,
      eventType: 'DSComplianceUIntRuleSet',
      parameters: { ruleName: 'jpInvestorsLimit', prevValue: '0', newValue: '0' }
    },
    {
      id: 'log_9272252b',
      transactionId: '0xd979ea4a0450ba2d622f467f7a401e292fba820d29d57c112d6aa619a4fb6a33',
      blockNumber: 4768,
      blockTime: 1611702796,
      eventType: 'DSComplianceBoolRuleSet',
      parameters: {
        ruleName: 'forceFullTransfer',
        prevValue: false,
        newValue: false
      }
    },
    {
      id: 'log_6d5e0857',
      transactionId: '0xd979ea4a0450ba2d622f467f7a401e292fba820d29d57c112d6aa619a4fb6a33',
      blockNumber: 4768,
      blockTime: 1611702796,
      eventType: 'DSComplianceBoolRuleSet',
      parameters: {
        ruleName: 'forceAccredited',
        prevValue: false,
        newValue: false
      }
    },
    {
      id: 'log_33230b0d',
      transactionId: '0xd979ea4a0450ba2d622f467f7a401e292fba820d29d57c112d6aa619a4fb6a33',
      blockNumber: 4768,
      blockTime: 1611702796,
      eventType: 'DSComplianceBoolRuleSet',
      parameters: {
        ruleName: 'forceAccreditedUS',
        prevValue: false,
        newValue: false
      }
    },
    {
      id: 'log_677810d8',
      transactionId: '0xd979ea4a0450ba2d622f467f7a401e292fba820d29d57c112d6aa619a4fb6a33',
      blockNumber: 4768,
      blockTime: 1611702796,
      eventType: 'DSComplianceBoolRuleSet',
      parameters: {
        ruleName: 'worldWideForceFullTransfer',
        prevValue: false,
        newValue: false
      }
    },
    {
      id: 'log_bb07e1c6',
      transactionId: '0x4ef63b9cc9357113f34977b95b6e724875a04bb166f84ed6613ea200a71cc2f0',
      blockNumber: 4769,
      blockTime: 1611702796,
      eventType: 'DSComplianceStringToUIntMapRuleSet',
      parameters: {
        ruleName: 'countryCompliance',
        keyValue: 'AT',
        prevValue: '0',
        newValue: '2'
      }
    },
    {
      id: 'log_3345b4df',
      transactionId: '0x46180667d81ee9b1796a2480104f64c775c36d1b040d344801ef7c534287cb2e',
      blockNumber: 4789,
      blockTime: 1611702798,
      eventType: 'DSTrustServiceRoleRemoved',
      parameters: {
        targetAddress: '0x33cC6c1c002311D26B561e1C8df3466F62bAe1f9',
        role: '1',
        sender: '0x33cC6c1c002311D26B561e1C8df3466F62bAe1f9'
      }
    },
    {
      id: 'log_17d99b69',
      transactionId: '0x46180667d81ee9b1796a2480104f64c775c36d1b040d344801ef7c534287cb2e',
      blockNumber: 4789,
      blockTime: 1611702798,
      eventType: 'DSTrustServiceRoleAdded',
      parameters: {
        targetAddress: '0x33cC6c1c002311D26B561e1C8df3466F62bAe1f9',
        role: '1',
        sender: '0x33cC6c1c002311D26B561e1C8df3466F62bAe1f9'
      }
    },
    {
      id: 'log_cbd34e8a',
      transactionId: '0x297aee788626e49d81fee683083eb5fc09bea64c298441c518dc641831e3fd97',
      blockNumber: 4792,
      blockTime: 1611710101,
      eventType: 'DSRegistryServiceInvestorAdded',
      parameters: {
        investorId: '73877b5c373ba90dd76d4ed9ad2a8c52',
        sender: '0x33cC6c1c002311D26B561e1C8df3466F62bAe1f9'
      }
    },
    {
      id: 'log_2b16e1d8',
      transactionId: '0xfb29d81b5165463a3415bd5cd40cca61bb120bba73381fd17a41639689a4c329',
      blockNumber: 4793,
      blockTime: 1611710103,
      eventType: 'DSRegistryServiceInvestorAdded',
      parameters: {
        investorId: '59ebf41c202c6b76071e6ac7cdfc9ea0',
        sender: '0x33cC6c1c002311D26B561e1C8df3466F62bAe1f9'
      }
    },
    {
      id: 'log_d06a3f9a',
      transactionId: '0x276d32ad5aab1e175626ef179e3d4523a42ea159088c9c5e09af61e68cb0f07e',
      blockNumber: 4794,
      blockTime: 1611710106,
      eventType: 'DSRegistryServiceInvestorCountryChanged',
      parameters: {
        investorId: '59ebf41c202c6b76071e6ac7cdfc9ea0',
        country: 'ES',
        sender: '0x33cC6c1c002311D26B561e1C8df3466F62bAe1f9'
      }
    },
    {
      id: 'log_d3abd3ea',
      transactionId: '0x22bd271fdbc833fa173fc5e9b53c20ee46a25798b5c1a35e1c4b008f15956654',
      blockNumber: 4795,
      blockTime: 1611710108,
      eventType: 'DSRegistryServiceInvestorAdded',
      parameters: {
        investorId: 'ce54035fe0067b319462fc3398c43002',
        sender: '0x33cC6c1c002311D26B561e1C8df3466F62bAe1f9'
      }
    },
    {
      id: 'log_5122a66c',
      transactionId: '0x800cadf925172df451521c0fea9820eaa49cf4711b08f8d6a06e955fe2584ed1',
      blockNumber: 4796,
      blockTime: 1611710111,
      eventType: 'DSRegistryServiceInvestorRemoved',
      parameters: {
        investorId: 'ce54035fe0067b319462fc3398c43002',
        sender: '0x33cC6c1c002311D26B561e1C8df3466F62bAe1f9'
      }
    },
    {
      id: 'log_3eb4ea81',
      transactionId: '0xf539e34e8e89e7a78a655166eef7f9a527c284835df5b47394f93438c8f8318e',
      blockNumber: 4797,
      blockTime: 1611710113,
      eventType: 'DSRegistryServiceInvestorAdded',
      parameters: {
        investorId: 'c839bcfeea4dd54f24638ce5aaa36f4d',
        sender: '0x33cC6c1c002311D26B561e1C8df3466F62bAe1f9'
      }
    },
    {
      id: 'log_8918c3af',
      transactionId: '0xba4ab6a29092d4d645d18b918c853bd28dcc450b499259956ee0d1d7ef687ba0',
      blockNumber: 4798,
      blockTime: 1611710116,
      eventType: 'DSRegistryServiceWalletAdded',
      parameters: {
        wallet: '0x27E96bEe245E6B92dC00a562B9f2B2A2685Da912',
        investorId: 'c839bcfeea4dd54f24638ce5aaa36f4d',
        sender: '0x634c4cAc02bf2168299A4834438CB05340EF15bc'
      }
    },
    {
      id: 'log_0c43516f',
      transactionId: '0x11737c442a24e72ccde8d48773274aadb038909066ed3bfd9f00e568890c20b2',
      blockNumber: 4799,
      blockTime: 1611710118,
      eventType: 'DSRegistryServiceWalletAdded',
      parameters: {
        wallet: '0xFaaF7857eDcf15c143D944a3ACf9d2E0523EA91B',
        investorId: '73877b5c373ba90dd76d4ed9ad2a8c52',
        sender: '0x634c4cAc02bf2168299A4834438CB05340EF15bc'
      }
    },
    {
      id: 'log_f6f79ea9',
      transactionId: '0xf252913759e08bef3df4d30650a185d6119b4610fc086ec3a85a4f486cf4b48d',
      blockNumber: 4800,
      blockTime: 1611710121,
      eventType: 'DSRegistryServiceInvestorAdded',
      parameters: {
        investorId: '52a1bfd0cae322e90ef3bda012250051',
        sender: '0x33cC6c1c002311D26B561e1C8df3466F62bAe1f9'
      }
    },
    {
      id: 'log_a314ae97',
      transactionId: '0xeaf289e2a20af98f328b75c8a23a9228764a0e0d5db37a393ccdb05923f5cdc5',
      blockNumber: 4801,
      blockTime: 1611710123,
      eventType: 'DSRegistryServiceWalletAdded',
      parameters: {
        wallet: '0x3f4c9a61521b8f5dc866f517c78B61f9209c56A2',
        investorId: '52a1bfd0cae322e90ef3bda012250051',
        sender: '0x634c4cAc02bf2168299A4834438CB05340EF15bc'
      }
    },
    {
      id: 'log_23b8f7c6',
      transactionId: '0x2aa8cf0478d4d5a58129f9940d00e9a55ccdf10bc64c7c7b81c390816eb51c3c',
      blockNumber: 4802,
      blockTime: 1611710126,
      eventType: 'DSRegistryServiceWalletAdded',
      parameters: {
        wallet: '0xdC8C95775c8B2919E1e967FeA8b595d727738eeB',
        investorId: '52a1bfd0cae322e90ef3bda012250051',
        sender: '0x634c4cAc02bf2168299A4834438CB05340EF15bc'
      }
    },
    {
      id: 'log_091298cd',
      transactionId: '0x52dade21c828767b6e0410689076bf63b562b0732ede0780f05cab65b765c86b',
      blockNumber: 4803,
      blockTime: 1611710128,
      eventType: 'DSRegistryServiceInvestorAdded',
      parameters: {
        investorId: '68b3fb4b9a74eb6d3afed48dafee78e3',
        sender: '0x33cC6c1c002311D26B561e1C8df3466F62bAe1f9'
      }
    },
    {
      id: 'log_8366ed94',
      transactionId: '0x0d760271f24a7181e904f7dd6fff98cbb3c97bf3d1e0c623cbbb2e2c575b191d',
      blockNumber: 4804,
      blockTime: 1611710131,
      eventType: 'DSRegistryServiceWalletAdded',
      parameters: {
        wallet: '0xa8A9AE412f40C1d76e1a8ED9A76688D818D62eBF',
        investorId: '68b3fb4b9a74eb6d3afed48dafee78e3',
        sender: '0x634c4cAc02bf2168299A4834438CB05340EF15bc'
      }
    },
    {
      id: 'log_dbeea6f6',
      transactionId: '0x1a7492bae8ce1b1bbb90739facd210283164660652189ef96833f0722de57901',
      blockNumber: 4805,
      blockTime: 1611710133,
      eventType: 'DSRegistryServiceWalletRemoved',
      parameters: {
        wallet: '0xa8A9AE412f40C1d76e1a8ED9A76688D818D62eBF',
        investorId: '68b3fb4b9a74eb6d3afed48dafee78e3',
        sender: '0x33cC6c1c002311D26B561e1C8df3466F62bAe1f9'
      }
    },
    {
      id: 'log_a84a5131',
      transactionId: '0x0567db8216a8d416793c0998a2971e4af78c141c978d7008852544762cb45947',
      blockNumber: 4806,
      blockTime: 1611710135,
      eventType: 'DSRegistryServiceInvestorAdded',
      parameters: {
        investorId: 'e1af634d26085fde843a99e1036f12a4',
        sender: '0x33cC6c1c002311D26B561e1C8df3466F62bAe1f9'
      }
    },
    {
      id: 'log_8a5b838b',
      transactionId: '0x79941da4331a66b67ed1fe53417cd1b6a0ca65d3b7770be348f3e905967108d9',
      blockNumber: 4807,
      blockTime: 1611710138,
      eventType: 'DSRegistryServiceInvestorAttributeChanged',
      parameters: {
        investorId: 'e1af634d26085fde843a99e1036f12a4',
        attributeId: '2',
        value: '1',
        expiry: '0',
        proofHash: '',
        sender: '0x33cC6c1c002311D26B561e1C8df3466F62bAe1f9'
      }
    },
    {
      id: 'log_ece0ea1b',
      transactionId: '0x06907742164834c3a5d5fe2d356101448414ce1db13331a86fd15a3f233a3c72',
      blockNumber: 4808,
      blockTime: 1611710140,
      eventType: 'DSRegistryServiceInvestorAdded',
      parameters: {
        investorId: '7d2e0603437200550f3334079785d75e',
        sender: '0x33cC6c1c002311D26B561e1C8df3466F62bAe1f9'
      }
    },
    {
      id: 'log_440d8a9c',
      transactionId: '0x453686c1e4296a97438b6d23cad196f312ab50b476454b769caa3dadd492710a',
      blockNumber: 4809,
      blockTime: 1611710142,
      eventType: 'DSRegistryServiceInvestorAttributeChanged',
      parameters: {
        investorId: '7d2e0603437200550f3334079785d75e',
        attributeId: '2',
        value: '1',
        expiry: '0',
        proofHash: '',
        sender: '0x33cC6c1c002311D26B561e1C8df3466F62bAe1f9'
      }
    },
    {
      id: 'log_103c94ad',
      transactionId: '0x36ae139dc259820345f70daf14afe8c1fd7fd466b86b36d0e4264cc8fc17f790',
      blockNumber: 4810,
      blockTime: 1611710145,
      eventType: 'DSRegistryServiceInvestorAdded',
      parameters: {
        investorId: '414ef3dd4c0bf9562bcea9ab32cd09cd',
        sender: '0x33cC6c1c002311D26B561e1C8df3466F62bAe1f9'
      }
    },
    {
      id: 'log_a84c9c86',
      transactionId: '0x6afec9d7ed0f8cd950ac8ca80d8166aa54ddc63cccce55b75863dd09e74c1fde',
      blockNumber: 4811,
      blockTime: 1611710147,
      eventType: 'DSRegistryServiceInvestorAttributeChanged',
      parameters: {
        investorId: '414ef3dd4c0bf9562bcea9ab32cd09cd',
        attributeId: '2',
        value: '1',
        expiry: '0',
        proofHash: '',
        sender: '0x33cC6c1c002311D26B561e1C8df3466F62bAe1f9'
      }
    },
    {
      id: 'log_3c508596',
      transactionId: '0x26496c81fb29daeb8ed4922bff33c4d253750a066659f6cbf0018194d60eafb9',
      blockNumber: 4812,
      blockTime: 1611710149,
      eventType: 'DSRegistryServiceInvestorAttributeChanged',
      parameters: {
        investorId: '414ef3dd4c0bf9562bcea9ab32cd09cd',
        attributeId: '2',
        value: '0',
        expiry: '0',
        proofHash: '',
        sender: '0x33cC6c1c002311D26B561e1C8df3466F62bAe1f9'
      }
    },
    {
      id: 'log_143ecfd0',
      transactionId: '0xd04ffc5438c97afd917b91d4ecaadcc02a792b7183b186729828b050465ec230',
      blockNumber: 4813,
      blockTime: 1611710152,
      eventType: 'DSRegistryServiceInvestorAdded',
      parameters: {
        investorId: '48bb0cfa1c28ad6078d99534181a18f3',
        sender: '0x33cC6c1c002311D26B561e1C8df3466F62bAe1f9'
      }
    },
    {
      id: 'log_4173454e',
      transactionId: '0xa77eefae2a4e64027c723f7a0c174f52ce5da8c97558bd06267c43a26b647344',
      blockNumber: 4814,
      blockTime: 1611710154,
      eventType: 'DSRegistryServiceWalletAdded',
      parameters: {
        wallet: '0x2d76088E241192af1Ec1C015F03eceA8bC8Ed4Ad',
        investorId: '48bb0cfa1c28ad6078d99534181a18f3',
        sender: '0x634c4cAc02bf2168299A4834438CB05340EF15bc'
      }
    },
    {
      id: 'log_7305a5db',
      transactionId: '0xd2dcb3c2b53470fd51dd011063afaa6684e303f5a4372af14d60fc2526f7f412',
      blockNumber: 4815,
      blockTime: 1611710157,
      eventType: 'Issue',
      parameters: {
        to: '0x2d76088E241192af1Ec1C015F03eceA8bC8Ed4Ad',
        value: '100',
        valueLocked: '0'
      }
    },
    {
      id: 'log_958f6c3d',
      transactionId: '0x537f3d55d34c27ae9e7b1af348b4f8f0a7ab89e1a0ac1dc8710f8c25b8786cdf',
      blockNumber: 4816,
      blockTime: 1611710162,
      eventType: 'DSComplianceUIntRuleSet',
      parameters: {
        ruleName: 'totalInvestorsLimit',
        prevValue: '0',
        newValue: '100'
      }
    },
    {
      id: 'log_1316288c',
      transactionId: '0x537f3d55d34c27ae9e7b1af348b4f8f0a7ab89e1a0ac1dc8710f8c25b8786cdf',
      blockNumber: 4816,
      blockTime: 1611710162,
      eventType: 'DSComplianceUIntRuleSet',
      parameters: { ruleName: 'minUSTokens', prevValue: '0', newValue: '0' }
    },
    {
      id: 'log_4e3e7cc7',
      transactionId: '0x537f3d55d34c27ae9e7b1af348b4f8f0a7ab89e1a0ac1dc8710f8c25b8786cdf',
      blockNumber: 4816,
      blockTime: 1611710162,
      eventType: 'DSComplianceUIntRuleSet',
      parameters: { ruleName: 'minEUTokens', prevValue: '0', newValue: '0' }
    },
    {
      id: 'log_5eaec451',
      transactionId: '0x537f3d55d34c27ae9e7b1af348b4f8f0a7ab89e1a0ac1dc8710f8c25b8786cdf',
      blockNumber: 4816,
      blockTime: 1611710162,
      eventType: 'DSComplianceUIntRuleSet',
      parameters: { ruleName: 'usInvestorsLimit', prevValue: '0', newValue: '100' }
    },
    {
      id: 'log_a6935a61',
      transactionId: '0x537f3d55d34c27ae9e7b1af348b4f8f0a7ab89e1a0ac1dc8710f8c25b8786cdf',
      blockNumber: 4816,
      blockTime: 1611710162,
      eventType: 'DSComplianceUIntRuleSet',
      parameters: {
        ruleName: 'usAccreditedInvestorsLimit',
        prevValue: '0',
        newValue: '100'
      }
    },
    {
      id: 'log_f1ce13e2',
      transactionId: '0x537f3d55d34c27ae9e7b1af348b4f8f0a7ab89e1a0ac1dc8710f8c25b8786cdf',
      blockNumber: 4816,
      blockTime: 1611710162,
      eventType: 'DSComplianceUIntRuleSet',
      parameters: {
        ruleName: 'nonAccreditedInvestorsLimit',
        prevValue: '0',
        newValue: '100'
      }
    },
    {
      id: 'log_6281493d',
      transactionId: '0x537f3d55d34c27ae9e7b1af348b4f8f0a7ab89e1a0ac1dc8710f8c25b8786cdf',
      blockNumber: 4816,
      blockTime: 1611710162,
      eventType: 'DSComplianceUIntRuleSet',
      parameters: {
        ruleName: 'maxUSInvestorsPercentage',
        prevValue: '0',
        newValue: '100'
      }
    },
    {
      id: 'log_cfac72d9',
      transactionId: '0x537f3d55d34c27ae9e7b1af348b4f8f0a7ab89e1a0ac1dc8710f8c25b8786cdf',
      blockNumber: 4816,
      blockTime: 1611710162,
      eventType: 'DSComplianceUIntRuleSet',
      parameters: {
        ruleName: 'blockFlowbackEndTime',
        prevValue: '0',
        newValue: '0'
      }
    },
    {
      id: 'log_3a0f32fa',
      transactionId: '0x537f3d55d34c27ae9e7b1af348b4f8f0a7ab89e1a0ac1dc8710f8c25b8786cdf',
      blockNumber: 4816,
      blockTime: 1611710162,
      eventType: 'DSComplianceUIntRuleSet',
      parameters: { ruleName: 'nonUSLockPeriod', prevValue: '0', newValue: '0' }
    },
    {
      id: 'log_0a180b1f',
      transactionId: '0x537f3d55d34c27ae9e7b1af348b4f8f0a7ab89e1a0ac1dc8710f8c25b8786cdf',
      blockNumber: 4816,
      blockTime: 1611710162,
      eventType: 'DSComplianceUIntRuleSet',
      parameters: {
        ruleName: 'minimumTotalInvestors',
        prevValue: '0',
        newValue: '0'
      }
    },
    {
      id: 'log_c72c806f',
      transactionId: '0x537f3d55d34c27ae9e7b1af348b4f8f0a7ab89e1a0ac1dc8710f8c25b8786cdf',
      blockNumber: 4816,
      blockTime: 1611710162,
      eventType: 'DSComplianceUIntRuleSet',
      parameters: {
        ruleName: 'minimumHoldingsPerInvestor',
        prevValue: '0',
        newValue: '0'
      }
    },
    {
      id: 'log_92270df2',
      transactionId: '0x537f3d55d34c27ae9e7b1af348b4f8f0a7ab89e1a0ac1dc8710f8c25b8786cdf',
      blockNumber: 4816,
      blockTime: 1611710162,
      eventType: 'DSComplianceUIntRuleSet',
      parameters: {
        ruleName: 'maximumHoldingsPerInvestor',
        prevValue: '0',
        newValue: '10000'
      }
    },
    {
      id: 'log_34addce9',
      transactionId: '0x537f3d55d34c27ae9e7b1af348b4f8f0a7ab89e1a0ac1dc8710f8c25b8786cdf',
      blockNumber: 4816,
      blockTime: 1611710162,
      eventType: 'DSComplianceUIntRuleSet',
      parameters: {
        ruleName: 'euRetailInvestorsLimit',
        prevValue: '150',
        newValue: '150'
      }
    },
    {
      id: 'log_802edb42',
      transactionId: '0x537f3d55d34c27ae9e7b1af348b4f8f0a7ab89e1a0ac1dc8710f8c25b8786cdf',
      blockNumber: 4816,
      blockTime: 1611710162,
      eventType: 'DSComplianceUIntRuleSet',
      parameters: {
        ruleName: 'usLockPeriod',
        prevValue: '31536000',
        newValue: '31536000'
      }
    },
    {
      id: 'log_80ad4059',
      transactionId: '0x537f3d55d34c27ae9e7b1af348b4f8f0a7ab89e1a0ac1dc8710f8c25b8786cdf',
      blockNumber: 4816,
      blockTime: 1611710162,
      eventType: 'DSComplianceUIntRuleSet',
      parameters: { ruleName: 'jpInvestorsLimit', prevValue: '0', newValue: '0' }
    },
    {
      id: 'log_32d90d27',
      transactionId: '0x537f3d55d34c27ae9e7b1af348b4f8f0a7ab89e1a0ac1dc8710f8c25b8786cdf',
      blockNumber: 4816,
      blockTime: 1611710162,
      eventType: 'DSComplianceBoolRuleSet',
      parameters: {
        ruleName: 'forceFullTransfer',
        prevValue: false,
        newValue: false
      }
    },
    {
      id: 'log_85d92995',
      transactionId: '0x537f3d55d34c27ae9e7b1af348b4f8f0a7ab89e1a0ac1dc8710f8c25b8786cdf',
      blockNumber: 4816,
      blockTime: 1611710162,
      eventType: 'DSComplianceBoolRuleSet',
      parameters: {
        ruleName: 'forceAccredited',
        prevValue: false,
        newValue: false
      }
    },
    {
      id: 'log_dcf9455d',
      transactionId: '0x537f3d55d34c27ae9e7b1af348b4f8f0a7ab89e1a0ac1dc8710f8c25b8786cdf',
      blockNumber: 4816,
      blockTime: 1611710162,
      eventType: 'DSComplianceBoolRuleSet',
      parameters: {
        ruleName: 'forceAccreditedUS',
        prevValue: false,
        newValue: false
      }
    },
    {
      id: 'log_7924cee7',
      transactionId: '0x537f3d55d34c27ae9e7b1af348b4f8f0a7ab89e1a0ac1dc8710f8c25b8786cdf',
      blockNumber: 4816,
      blockTime: 1611710162,
      eventType: 'DSComplianceBoolRuleSet',
      parameters: {
        ruleName: 'worldWideForceFullTransfer',
        prevValue: false,
        newValue: false
      }
    },
    {
      id: 'log_b930506e',
      transactionId: '0xa9ca9d16e92b65c60dc8de1c0565008fdf236e979044b16b71c365eceef3fdfa',
      blockNumber: 4817,
      blockTime: 1611710165,
      eventType: 'DSRegistryServiceInvestorAdded',
      parameters: {
        investorId: '9aa332bf35511cdbe27fffc0afe02593',
        sender: '0x33cC6c1c002311D26B561e1C8df3466F62bAe1f9'
      }
    },
    {
      id: 'log_ab179556',
      transactionId: '0xa9ca9d16e92b65c60dc8de1c0565008fdf236e979044b16b71c365eceef3fdfa',
      blockNumber: 4817,
      blockTime: 1611710165,
      eventType: 'DSRegistryServiceInvestorAttributeChanged',
      parameters: {
        investorId: '9aa332bf35511cdbe27fffc0afe02593',
        attributeId: '2',
        value: '1',
        expiry: '0',
        proofHash: '',
        sender: '0x33cC6c1c002311D26B561e1C8df3466F62bAe1f9'
      }
    },
    {
      id: 'log_53f1c929',
      transactionId: '0x4d3e3ae86dcf44f14afaeeab389714e13b27c5ce94941d41217346be7fae25d0',
      blockNumber: 4818,
      blockTime: 1611710167,
      eventType: 'DSRegistryServiceWalletAdded',
      parameters: {
        wallet: '0x3C626167E2f88b9CB66df571e92dF7eEc4e153d7',
        investorId: '9aa332bf35511cdbe27fffc0afe02593',
        sender: '0x634c4cAc02bf2168299A4834438CB05340EF15bc'
      }
    },
    {
      id: 'log_d0af67fc',
      transactionId: '0xf6e7350d4e975c8dec0a69fada9462760dd2f29345453af134b7ca145b0f2067',
      blockNumber: 4819,
      blockTime: 1611710170,
      eventType: 'Issue',
      parameters: {
        to: '0x3C626167E2f88b9CB66df571e92dF7eEc4e153d7',
        value: '100',
        valueLocked: '0'
      }
    },
    {
      id: 'log_596acd2b',
      transactionId: '0x5eb645ff4ea5b8349781ac113e335f7e3f09df7b31f340288d1d0e64074a6668',
      blockNumber: 4820,
      blockTime: 1611710173,
      eventType: 'DSRegistryServiceInvestorAdded',
      parameters: {
        investorId: '2feb1aa2a80f983e9a4b350abd586644',
        sender: '0xeCf7CbA7637Faf97F4CCddAde4C91D2FE69c13a8'
      }
    },
    {
      id: 'log_bbe17fd4',
      transactionId: '0x5eb645ff4ea5b8349781ac113e335f7e3f09df7b31f340288d1d0e64074a6668',
      blockNumber: 4820,
      blockTime: 1611710173,
      eventType: 'DSRegistryServiceInvestorCountryChanged',
      parameters: {
        investorId: '2feb1aa2a80f983e9a4b350abd586644',
        country: 'US',
        sender: '0xeCf7CbA7637Faf97F4CCddAde4C91D2FE69c13a8'
      }
    },
    {
      id: 'log_e5da936a',
      transactionId: '0x5eb645ff4ea5b8349781ac113e335f7e3f09df7b31f340288d1d0e64074a6668',
      blockNumber: 4820,
      blockTime: 1611710173,
      eventType: 'DSRegistryServiceWalletAdded',
      parameters: {
        wallet: '0x568C9435FfaaAfe4371CebD09102BB1644D7675A',
        investorId: '2feb1aa2a80f983e9a4b350abd586644',
        sender: '0xeCf7CbA7637Faf97F4CCddAde4C91D2FE69c13a8'
      }
    },
    {
      id: 'log_2c9f9ccf',
      transactionId: '0x5eb645ff4ea5b8349781ac113e335f7e3f09df7b31f340288d1d0e64074a6668',
      blockNumber: 4820,
      blockTime: 1611710173,
      eventType: 'DSRegistryServiceInvestorAttributeChanged',
      parameters: {
        investorId: '2feb1aa2a80f983e9a4b350abd586644',
        attributeId: '1',
        value: '0',
        expiry: '0',
        proofHash: '',
        sender: '0xeCf7CbA7637Faf97F4CCddAde4C91D2FE69c13a8'
      }
    },
    {
      id: 'log_c45ed7da',
      transactionId: '0x5eb645ff4ea5b8349781ac113e335f7e3f09df7b31f340288d1d0e64074a6668',
      blockNumber: 4820,
      blockTime: 1611710173,
      eventType: 'DSRegistryServiceInvestorAttributeChanged',
      parameters: {
        investorId: '2feb1aa2a80f983e9a4b350abd586644',
        attributeId: '2',
        value: '0',
        expiry: '0',
        proofHash: '',
        sender: '0xeCf7CbA7637Faf97F4CCddAde4C91D2FE69c13a8'
      }
    },
    {
      id: 'log_1d36aab5',
      transactionId: '0x5eb645ff4ea5b8349781ac113e335f7e3f09df7b31f340288d1d0e64074a6668',
      blockNumber: 4820,
      blockTime: 1611710173,
      eventType: 'DSRegistryServiceInvestorAttributeChanged',
      parameters: {
        investorId: '2feb1aa2a80f983e9a4b350abd586644',
        attributeId: '4',
        value: '0',
        expiry: '0',
        proofHash: '',
        sender: '0xeCf7CbA7637Faf97F4CCddAde4C91D2FE69c13a8'
      }
    },
    {
      id: 'log_71eb4f39',
      transactionId: '0x5eb645ff4ea5b8349781ac113e335f7e3f09df7b31f340288d1d0e64074a6668',
      blockNumber: 4820,
      blockTime: 1611710173,
      eventType: 'Issue',
      parameters: {
        to: '0x568C9435FfaaAfe4371CebD09102BB1644D7675A',
        value: '75',
        valueLocked: '0'
      }
    },
    {
      id: 'log_2bd61a89',
      transactionId: '0x118dcc28337f988cf3cd07716ab11fed985608eeb9396111fe173feb85439e9a',
      blockNumber: 4821,
      blockTime: 1611710176,
      eventType: 'DSRegistryServiceInvestorAdded',
      parameters: {
        investorId: '58709430b2dcba79c2c647f35771821c',
        sender: '0x33cC6c1c002311D26B561e1C8df3466F62bAe1f9'
      }
    },
    {
      id: 'log_46e3fa25',
      transactionId: '0x118dcc28337f988cf3cd07716ab11fed985608eeb9396111fe173feb85439e9a',
      blockNumber: 4821,
      blockTime: 1611710176,
      eventType: 'DSRegistryServiceInvestorAttributeChanged',
      parameters: {
        investorId: '58709430b2dcba79c2c647f35771821c',
        attributeId: '2',
        value: '1',
        expiry: '0',
        proofHash: '',
        sender: '0x33cC6c1c002311D26B561e1C8df3466F62bAe1f9'
      }
    },
    {
      id: 'log_4d7b07b7',
      transactionId: '0xad107ead4aab3f8f41a9df1000cd5dac3b655dc4b68b4fc34281f5d3b4cc402f',
      blockNumber: 4822,
      blockTime: 1611710178,
      eventType: 'DSRegistryServiceWalletAdded',
      parameters: {
        wallet: '0x33cC6c1c002311D26B561e1C8df3466F62bAe1f9',
        investorId: '58709430b2dcba79c2c647f35771821c',
        sender: '0x634c4cAc02bf2168299A4834438CB05340EF15bc'
      }
    },
    {
      id: 'log_4bcff2f0',
      transactionId: '0xe78587533c93ba7a98ec63c90bd7a251f6fafbe9abbd248ac7f6e604509ac91b',
      blockNumber: 4823,
      blockTime: 1611710181,
      eventType: 'DSRegistryServiceInvestorAdded',
      parameters: {
        investorId: 'a844470f186a315b3d5972c2869fa86c',
        sender: '0x33cC6c1c002311D26B561e1C8df3466F62bAe1f9'
      }
    },
    {
      id: 'log_05a939cc',
      transactionId: '0xe78587533c93ba7a98ec63c90bd7a251f6fafbe9abbd248ac7f6e604509ac91b',
      blockNumber: 4823,
      blockTime: 1611710181,
      eventType: 'DSRegistryServiceInvestorAttributeChanged',
      parameters: {
        investorId: 'a844470f186a315b3d5972c2869fa86c',
        attributeId: '2',
        value: '1',
        expiry: '0',
        proofHash: '',
        sender: '0x33cC6c1c002311D26B561e1C8df3466F62bAe1f9'
      }
    },
    {
      id: 'log_7a211104',
      transactionId: '0xedff3268334ffc02d6cab7e281598213ba62b9fe500126ec19955c3854048612',
      blockNumber: 4824,
      blockTime: 1611710183,
      eventType: 'DSRegistryServiceWalletAdded',
      parameters: {
        wallet: '0x255Ce9218e000cbC4BceC74e96aBd3f0dA7757AF',
        investorId: 'a844470f186a315b3d5972c2869fa86c',
        sender: '0x634c4cAc02bf2168299A4834438CB05340EF15bc'
      }
    },
    {
      id: 'log_a947e3ee',
      transactionId: '0xcc15e0eb453ac169d2fcefbb464d586a3ef3a0d2435cfdf0f70174b22e7fb2d8',
      blockNumber: 4825,
      blockTime: 1611710186,
      eventType: 'Issue',
      parameters: {
        to: '0x33cC6c1c002311D26B561e1C8df3466F62bAe1f9',
        value: '250',
        valueLocked: '0'
      }
    },
    {
      id: 'log_4cd35423',
      transactionId: '0xf76d3b050cf9056919ed36d4c670006de92d0b729fcb7fcd34d168f84351e67f',
      blockNumber: 4826,
      blockTime: 1611710189,
      eventType: 'Transfer',
      parameters: {
        from: '0x33cC6c1c002311D26B561e1C8df3466F62bAe1f9',
        to: '0x255Ce9218e000cbC4BceC74e96aBd3f0dA7757AF',
        value: '110'
      }
    },
    {
      id: 'log_cd06f456',
      transactionId: '0x820c4d7dbcd74ab77b16a4b6bcc967790ec38d4fee2ebf0cdccda2dfd3e99320',
      blockNumber: 4827,
      blockTime: 1611710192,
      eventType: 'DSRegistryServiceInvestorAdded',
      parameters: {
        investorId: 'a19c8f9329a1d5318e18089e0582fb1e',
        sender: '0x33cC6c1c002311D26B561e1C8df3466F62bAe1f9'
      }
    },
    {
      id: 'log_e7b922b5',
      transactionId: '0x820c4d7dbcd74ab77b16a4b6bcc967790ec38d4fee2ebf0cdccda2dfd3e99320',
      blockNumber: 4827,
      blockTime: 1611710192,
      eventType: 'DSRegistryServiceInvestorAttributeChanged',
      parameters: {
        investorId: 'a19c8f9329a1d5318e18089e0582fb1e',
        attributeId: '2',
        value: '1',
        expiry: '0',
        proofHash: '',
        sender: '0x33cC6c1c002311D26B561e1C8df3466F62bAe1f9'
      }
    },
    {
      id: 'log_5be9de17',
      transactionId: '0xd99bbc82c30a86f559002b90dccacade91640f8e7bdb1c4a24af7d5b323f3714',
      blockNumber: 4828,
      blockTime: 1611710194,
      eventType: 'DSRegistryServiceWalletAdded',
      parameters: {
        wallet: '0xB8c04E94A0B56f77c552AAA8e3119942ef8871ef',
        investorId: 'a19c8f9329a1d5318e18089e0582fb1e',
        sender: '0x634c4cAc02bf2168299A4834438CB05340EF15bc'
      }
    },
    {
      id: 'log_543d91b4',
      transactionId: '0x3d67ccf95ace1a9da04023dede9c0578ee8fb0b00d92095d2dec2d125cacb52a',
      blockNumber: 4829,
      blockTime: 1611710197,
      eventType: 'DSRegistryServiceInvestorAdded',
      parameters: {
        investorId: 'd4637060a0f78824a53b0cafc3c66ecd',
        sender: '0x33cC6c1c002311D26B561e1C8df3466F62bAe1f9'
      }
    },
    {
      id: 'log_46e97b7f',
      transactionId: '0x3d67ccf95ace1a9da04023dede9c0578ee8fb0b00d92095d2dec2d125cacb52a',
      blockNumber: 4829,
      blockTime: 1611710197,
      eventType: 'DSRegistryServiceInvestorAttributeChanged',
      parameters: {
        investorId: 'd4637060a0f78824a53b0cafc3c66ecd',
        attributeId: '2',
        value: '1',
        expiry: '0',
        proofHash: '',
        sender: '0x33cC6c1c002311D26B561e1C8df3466F62bAe1f9'
      }
    },
    {
      id: 'log_105fac03',
      transactionId: '0xce15f48d96f6f3764c60a6262df1d98ac86384adae084a63b6cbe7cafd1d9681',
      blockNumber: 4830,
      blockTime: 1611710199,
      eventType: 'DSRegistryServiceWalletAdded',
      parameters: {
        wallet: '0x3FC8437e31c0530124fbF3a03d62CFFFc5EeE268',
        investorId: 'd4637060a0f78824a53b0cafc3c66ecd',
        sender: '0x634c4cAc02bf2168299A4834438CB05340EF15bc'
      }
    },
    {
      id: 'log_16f6edf9',
      transactionId: '0xb7d4ed0453140c03117b03199a18da23b8d2c78280dd2a17f73db77d62ef414c',
      blockNumber: 4831,
      blockTime: 1611710202,
      eventType: 'Issue',
      parameters: {
        to: '0x3FC8437e31c0530124fbF3a03d62CFFFc5EeE268',
        value: '150',
        valueLocked: '0'
      }
    },
    {
      id: 'log_1270d312',
      transactionId: '0x503c0f55a3af6250453833e0f990946582f9dbcc7a2fd2638cdf3c5c5af5b280',
      blockNumber: 4832,
      blockTime: 1611710205,
      eventType: 'DSRegistryServiceInvestorAdded',
      parameters: {
        investorId: '446fd53be404b3bfc16bb0edc574d2ee',
        sender: '0x33cC6c1c002311D26B561e1C8df3466F62bAe1f9'
      }
    },
    {
      id: 'log_d525c02f',
      transactionId: '0x503c0f55a3af6250453833e0f990946582f9dbcc7a2fd2638cdf3c5c5af5b280',
      blockNumber: 4832,
      blockTime: 1611710205,
      eventType: 'DSRegistryServiceInvestorAttributeChanged',
      parameters: {
        investorId: '446fd53be404b3bfc16bb0edc574d2ee',
        attributeId: '2',
        value: '1',
        expiry: '0',
        proofHash: '',
        sender: '0x33cC6c1c002311D26B561e1C8df3466F62bAe1f9'
      }
    },
    {
      id: 'log_28b96c4b',
      transactionId: '0x522f7dc130fac9421607a18049a74ce18382f6c95638a64c0b9e995d45548fd4',
      blockNumber: 4833,
      blockTime: 1611710207,
      eventType: 'DSRegistryServiceWalletAdded',
      parameters: {
        wallet: '0x504f6A028d1D353374c4A615425584326DBB0325',
        investorId: '446fd53be404b3bfc16bb0edc574d2ee',
        sender: '0x634c4cAc02bf2168299A4834438CB05340EF15bc'
      }
    },
    {
      id: 'log_3bc2b1d3',
      transactionId: '0x218a53a19971b9346ab4fe11627c1846071a6fb86c157a08d05f7532b5910f59',
      blockNumber: 4834,
      blockTime: 1611710210,
      eventType: 'Issue',
      parameters: {
        to: '0x504f6A028d1D353374c4A615425584326DBB0325',
        value: '200',
        valueLocked: '0'
      }
    },
    {
      id: 'log_3fa05470',
      transactionId: '0x1890ee1e12f76dbd762f809508ea9e05bd3f3ab50a810d1e49e239f867a5552d',
      blockNumber: 4835,
      blockTime: 1611710212,
      eventType: 'Burn',
      parameters: {
        burner: '0x504f6A028d1D353374c4A615425584326DBB0325',
        value: '150',
        reason: 'Because we are testing burn functionality'
      }
    },
    ... 353 more items
  ]
}
```

