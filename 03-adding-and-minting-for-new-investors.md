# Creating investors an minting some tokens

```bash
diego-securitize@heavy-machine:~/dsclient/master$ dsclient holder:create investor01 --accredited=approved --country=ES --kyc=approved --wallet=0x0a065Eaf3b6e037F8213fa13e76A025d2846Bd48
0xcaab12877c6d6847fc76b9255bd81eca07a7edfce9a6f16e91b3b3c6c5d6079b
diego-securitize@heavy-machine:~/dsclient/master$ dsclient holder:create investor02 --accredited=approved --country=ES --kyc=approved --wallet=0xe2099bCfCA27f8cae93FD3C8726e4a8250FcA015
0xac062f782202e03564849faabe523af30d55e733c3938fa455962ac42c37f797
diego-securitize@heavy-machine:~/dsclient/master$ dsclient holder:create investor03 --accredited=approved --country=ES --kyc=approved --wallet=0x275B0d8f14a77DceDB4A2b7cD4532a39C9AAb2A4
0x13b9e24429fbfc89a7d4e01e89c43a19416b169a129576e5929ab0528670fab5
diego-securitize@heavy-machine:~/dsclient/master$ dsclient token:mint 0x0a065Eaf3b6e037F8213fa13e76A025d2846Bd48 150
0x24964fc13b538b755492e7a750d1d46c9ffea59f168ca9de54540fb8a8a0a410
diego-securitize@heavy-machine:~/dsclient/master$ dsclient token:mint 0xe2099bCfCA27f8cae93FD3C8726e4a8250FcA015 230
0x0bc0f1b74e493de97dad1b4a8d7cb6aa18cf9e168e32cc331f7ad56cbd7718dd
diego-securitize@heavy-machine:~/dsclient/master$ dsclient token:mint 0x275B0d8f14a77DceDB4A2b7cD4532a39C9AAb2A4 180
0x93b478e27be8bc9193f65da6c23a198be72c061faf8c851e2d5ad54efa28e3cb
```

# Creating some locks

```bash
diego-securitize@heavy-machine:~/dsclient/master$ dsclient lock:create investor01 10 evasor
0xc78bcacd6f35e243daba88482b9510e10b1f49e5e7a85f3ed93675ad509df197
diego-securitize@heavy-machine:~/dsclient/master$ dsclient lock:create investor01 20 evasor2
diego-securitize@heavy-machine:~/dsclient/master$ dsclient lock:create investor02 50 test-sucutrule
```
