```bash
diego-securitize@heavy-machine:~/dsclient/master$ dsclient connect
Provider URL [http://0.0.0.0:3011]: 
? Select authentication scheme for the selected provider No Authentication

- Connecting to the provider -

Provider Details:
{
  name: 'DS_ETHEREUM_PROVIDER',
  description: 'DS Provider for Ethereum',
  underlying: 'ETHEREUM',
  network: 'development'
}
Token Address (optional) [0x576A4026a3B943C10178235333DDE20AE11e499e]: 0x86Cd5d893C2aE7F8dFbdDC6972A204a505D2A63f
Caller Identity (optional) [0x33cC6c1c002311D26B561e1C8df3466F62bAe1f9]: 

Token Details:
{
  name: 'PoC-Token-A01',
  symbol: 'PoCTokA01',
  decimals: 0,
  totalIssued: '0',
  totalWallets: 0,
  isPaused: false
}

Create a default connection file? (y/n): y
A connection file was created and stored in /home/diego-securitize/dsclient/master/.dsprotocol
diego-securitize@heavy-machine:~/dsclient/master$ dsclient test:all


  HOLDER related operations tests
0x297aee788626e49d81fee683083eb5fc09bea64c298441c518dc641831e3fd97
    ✓ should create a new holder id (6622ms)
0xfb29d81b5165463a3415bd5cd40cca61bb120bba73381fd17a41639689a4c329
0x276d32ad5aab1e175626ef179e3d4523a42ea159088c9c5e09af61e68cb0f07e
    ✓ should be able to update the holder country (4995ms)
0x22bd271fdbc833fa173fc5e9b53c20ee46a25798b5c1a35e1c4b008f15956654
0x800cadf925172df451521c0fea9820eaa49cf4711b08f8d6a06e955fe2584ed1
    ✓ should be able to delete a holder (4829ms)
0xf539e34e8e89e7a78a655166eef7f9a527c284835df5b47394f93438c8f8318e
0xba4ab6a29092d4d645d18b918c853bd28dcc450b499259956ee0d1d7ef687ba0
    ✓ should not allow to delete a holder with a wallet assigned to it (5230ms)
0x11737c442a24e72ccde8d48773274aadb038909066ed3bfd9f00e568890c20b2
    ✓ should assign a wallet to existing tokens holder (2539ms)
0xf252913759e08bef3df4d30650a185d6119b4610fc086ec3a85a4f486cf4b48d
0xeaf289e2a20af98f328b75c8a23a9228764a0e0d5db37a393ccdb05923f5cdc5
0x2aa8cf0478d4d5a58129f9940d00e9a55ccdf10bc64c7c7b81c390816eb51c3c
    ✓ should be able to add multiple wallets to a holder (7520ms)
0x52dade21c828767b6e0410689076bf63b562b0732ede0780f05cab65b765c86b
0x0d760271f24a7181e904f7dd6fff98cbb3c97bf3d1e0c623cbbb2e2c575b191d
0x1a7492bae8ce1b1bbb90739facd210283164660652189ef96833f0722de57901
    ✓ should be able to remove a wallet form the holder (7132ms)
0x0567db8216a8d416793c0998a2971e4af78c141c978d7008852544762cb45947
0x79941da4331a66b67ed1fe53417cd1b6a0ca65d3b7770be348f3e905967108d9
    ✓ should be able to add a valid attribute to an existing holder (4757ms)
0x06907742164834c3a5d5fe2d356101448414ce1db13331a86fd15a3f233a3c72
0x453686c1e4296a97438b6d23cad196f312ab50b476454b769caa3dadd492710a
    ✓ Should be able to retrieve a specific attribute from the holder (4719ms)
0x36ae139dc259820345f70daf14afe8c1fd7fd466b86b36d0e4264cc8fc17f790
0x6afec9d7ed0f8cd950ac8ca80d8166aa54ddc63cccce55b75863dd09e74c1fde
0x26496c81fb29daeb8ed4922bff33c4d253750a066659f6cbf0018194d60eafb9
    ✓ should be able to remove an attribute from the holder (6987ms)
0xd04ffc5438c97afd917b91d4ecaadcc02a792b7183b186729828b050465ec230
0xa77eefae2a4e64027c723f7a0c174f52ce5da8c97558bd06267c43a26b647344
0xd2dcb3c2b53470fd51dd011063afaa6684e303f5a4372af14d60fc2526f7f412
    ✓ should NOT allow to remove a wallet that is still holding tokens (8142ms)

  TOKEN related operations tests
0x537f3d55d34c27ae9e7b1af348b4f8f0a7ab89e1a0ac1dc8710f8c25b8786cdf
    ✓ should retrieve the information for the test Token (90ms)
0xa9ca9d16e92b65c60dc8de1c0565008fdf236e979044b16b71c365eceef3fdfa
0x4d3e3ae86dcf44f14afaeeab389714e13b27c5ce94941d41217346be7fae25d0
0xf6e7350d4e975c8dec0a69fada9462760dd2f29345453af134b7ca145b0f2067
    ✓ should be able to mint tokens to Holder 1 (7743ms)
0x5eb645ff4ea5b8349781ac113e335f7e3f09df7b31f340288d1d0e64074a6668
    ✓ should be able to create a Holder and include a mint operation in the same transaction (3452ms)
0x118dcc28337f988cf3cd07716ab11fed985608eeb9396111fe173feb85439e9a
0xad107ead4aab3f8f41a9df1000cd5dac3b655dc4b68b4fc34281f5d3b4cc402f
0xe78587533c93ba7a98ec63c90bd7a251f6fafbe9abbd248ac7f6e604509ac91b
0xedff3268334ffc02d6cab7e281598213ba62b9fe500126ec19955c3854048612
0xcc15e0eb453ac169d2fcefbb464d586a3ef3a0d2435cfdf0f70174b22e7fb2d8
0xf76d3b050cf9056919ed36d4c670006de92d0b729fcb7fcd34d168f84351e67f
    ✓ should be able to transfer tokens between Holder 1 and Holder 2 (16038ms)
0x820c4d7dbcd74ab77b16a4b6bcc967790ec38d4fee2ebf0cdccda2dfd3e99320
0xd99bbc82c30a86f559002b90dccacade91640f8e7bdb1c4a24af7d5b323f3714
    ✓ should not allow to transfer more tokens than the current balance (4733ms)
    ✓ should not transfer tokens to an address not associated with a holder
Current supply of tokens: 
525
0x3d67ccf95ace1a9da04023dede9c0578ee8fb0b00d92095d2dec2d125cacb52a
0xce15f48d96f6f3764c60a6262df1d98ac86384adae084a63b6cbe7cafd1d9681
0xb7d4ed0453140c03117b03199a18da23b8d2c78280dd2a17f73db77d62ef414c
Total supply after minting 150: 
675
    ✓ should get the correct value for total Supply (7966ms)
0x503c0f55a3af6250453833e0f990946582f9dbcc7a2fd2638cdf3c5c5af5b280
0x522f7dc130fac9421607a18049a74ce18382f6c95638a64c0b9e995d45548fd4
0x218a53a19971b9346ab4fe11627c1846071a6fb86c157a08d05f7532b5910f59
0x1890ee1e12f76dbd762f809508ea9e05bd3f3ab50a810d1e49e239f867a5552d
    ✓ should allow to burn tokens to the owner (10139ms)
0x1826677d1813038d9105721431b74c5d0d661e4cb7942ef5a67722ecf4b7807f
    ✓ should emit an event when a holder is created (3269ms)
0x09c74dbd365acd283d45dad442f9a12e0f42c90b280f492532cbc4541caa19a8
0xfbcbfbc0686a851cc370b0bbd1a2126223b249c1ff387b739b6dc0276879b013
    ✓ should emit an event when a wallet is added to a holder (5406ms)
0xd112b3efb65a123cf6bd256124f6e009647ea991c6f2934ad45762cbf5039d2a
0x722d2fee7cfd4d5a320936f390fd5fb96c8bfb08fbca0bfbc4c194ff9954c739
    ✓ should emit an event when the country is changed for an investor (5399ms)
0x3304eb537745c86bb13495da2f264b97d1ed40935bf604fb33011c58883317ef
0x7fee2df7d6d3bf96f2acbc50c3f60c63a2f29709e3d36640d8827ea1e411c219
0x6880587aa6a606a10e30ca416a86884ce08bf56ceeffe8e6687583d9b112a7eb
0xc0b0613cdc4841677431158090c17dd0699adcd778ef5c73f65f833d0c989608
0x8851e69ce6a1407b4fc73eb5c8f221c92ab7919ec002c1c5040b2a8435ef6ecd
    ✓ should return the correct amount for the balance of a holder including all their wallets (13662ms)
    ✓ should be able to filter events (37113ms)
0x5bfd7481020c1f764030e5d548e4aafb9f25deb6b80e7e510f1f8200dfa8e7e8
0xbcf8014633fe1894a731c7c38c8d3a824c0b1fa5b09f5dddd9ea35807aa002c5
0x65c77db0b63d58bcabc59b965039754e9d1f7a71791fa10b6e63eeb27b22ff82
0x56f602d904247f5c53c82cf4b1c936e09690dc9ff2f7e469e793c45b8cfc73c9
0xa859dad363b0a11e5ad7ad7aa2355a2284ab54baea3bfaf4de361023c163753e
    ✓ should be able to pause the token (12781ms)
0xcaf6807301f68e5941faaa02a34718f6d6355d4eb138395cc6dfe3aa146979e0
    ✓ should be able to approve a transfer from a spender (2199ms)
    - should be able to transferFrom a holder to a spender with the correct allowance
    - should be able to seize tokens from a holder

  ROLE management related tests
    ✓ should be able to get the role of a holder (48ms)
0x2d1e4bce85cce80263d6a6a13a51101aa56dd376285368bab6dc3a6a3abc3335
0xd0f7910eb00cc7cfa9ea5f35fe281e42838784e6e6c38d05da9aeb77742a964c
0xa523883e439a3022f5cf6165fa9eac96f64abd4b7201baf0447028fd16675d64
    ✓ should be able to set the role of a holder (7115ms)
    - should not allow an ISSUER to assign roles in the network

  COMPLIANCE related functionality tests
0x6dcc7cbc870043f0c6af37f1f76c525440690a1fe230d217ebf3ee01d50f430d
0xc4bc411c2bc6231bf0bddc1193d3facc54df76b33c3ff710deff06be80a36fe2
0xe7ae63df5c7c65429212fd5733ff6b683c84bfd6b0adb06e8245b79a7bee3758
0x9bd34e1552948351d5587ccddca08988e852c6524d885687cb6ceb7885ce493d
0xc8f5aa1a39be535062d0f57951b05453af82d73605d4249bc3427aec8d63faa8
0x697fecd6a04bea9d58d73c6638b37cff4aa494c4a66832bd3bc1f45f96116514
    ✓ should get the compliance information for the active token (60ms)
    ✓ Should return compliance counters (483ms)
0xe7bf07d56908c7e725fb7bca2e3ea1e70b460aeaeab3f2d376ae25e97b78e665
    ✓ should be able to set the compliance status for US (2224ms)
0x0796e91ab5b6efecf1be4f2ce03f8afcf7dc49655c3595f560963e1842f0c4e7
    ✓ should be able to set compliance rules as the OWNER (4151ms)
0x456d32b03c7edbf8fe14f82e65004e4fed1f9e8a8ce0919d78a6b030f59eb63e
    ✓ should apply totalInvestorsLimit rule when MINT is attempted (4189ms)
0x13d27c2a3b6c81e80b4ce2f5f16072fe5d80550d04e54da182f99e306423b630
0x269fc163d239ff0e158cb673351eefee3ec2bf87e4a877c6ea9f15238dcb9f8e
0xd31f26164d45bd82f8f29d4ed15e4319a081a03ec2ac891b3d7c03ad1671f1da
0x8c555b1ffb02d0ab67c4e5c32b1efbb6901894d5f996d1d48ab50832e5d70d22
0xb09aee6ae7252f0bacc65b958a2e0b56ad0f19ef1c5fe1e0ac6e23e14bcaf5ef
    ✓ should apply totalInvestorsLimit compliance rule when TRANSFER is attempted (16309ms)
    - should allow a full transfer between holders when the number of holders is at its maximum
0xd9247a0793930cd5c3fe1788d4e744847410dadaeee8f7a5bf36f3a0c81cf6b3
0x654bdbdaadf3a03378f0ec24b04f3590d855675f1ecd47594901117582a74900
0x870920693f0052fee820895d843bdf4105069b0c5d8106409db4e17312e9b5af
    ✓ should enforce the maximum holdings per investor limit rule (11324ms)
0xd201860fdf3c6065c58542afe5123e63b81f0631db0b83217c57ac5298669824
0xb5b6e782b1af7fc785aa70c12f7db32e0a98f5eacd37491dfd73a786c90610b7
0x7e1ecfe26b62a39984200ed9e0ec9011de4c21ad732585ef66df681f9fabf262
    ✓ should not allow to mint a token amount above maxHoldingsPerInvestor (9180ms)
0xb331a1b94a33ba421a70202401d5ae6ed8fcc3d65a0f8b3c814f73f909a41d69
0x870fda9cb1ae708778e8c73317a66c0dbd3bf9ffe4116a33e7c6d3dbb9d0a463
0xc36bac0b73e7b6ef73d90bd8456c49459d3accbb2c126c25c9f808b1ed0085a7
0x38658196355c3622d5664135f6bf68acdf80d06993560f2f9ce93677c237798f
0x53462b98312979dbd02836fd2b248a81c9bb8c45a70184b473fc433974ac6cc4
0xd405ecb5522babfad23eaacc82e581da894d34bfbc9b18de3f006e5178d1d8af
0x14bdad8f00400a01dc8d01dc7c4743d62fede04b5d929c7ffc7e116b49a138be
0x710a84ab20d4666bc595087ec324d59f56e970cc4925b82c2c1e4941617e0315
0x061824f369aca8cc1a00573fbacb0d450c2b511f64db6f56dc0f6d6ff47c322d
0xe36cd9ca94285eadcedd0da4e6fbe5187323257df193653b77fad7a91b1e7f2d
0x309b1ca37c33c276a65a6848d3e36ba1d7605c78ee7c75ff8de31773a53a74de
    ✓ should not allow to transfer locked tokens for US investors if issuance time was less than 365 days ago (29545ms)
0xa04829b65d437363982a5fb471c2093ccb47a9d644af18007f4de3b337299399
0x2229b3fb651167cf8f392ac84fc60ab98035bfd72ed7d6948cce4aaf8867497e
0xda526d1d17c198d1f66c67be46021949c69a317e72cb220b7edf952dd89c0a6b
    ✓ should apply the forceFullTransfer rule for US investors if set (10897ms)
0xee75508c6e7a0d16b6128e4213d0a3c46480e5f25f6aa6fa7e68b2a69745379b
0xc6f2a51324fa9fab202926681d356f4f7305c88475f6027786448b9e205da91a
0xf4e3036b89555277ab47d36463054ffee96ab246e5790f92e4683f95663e18a6
0xdd140a0333b4f6634a41c32a9298b4686f80b8bc824980f40da3c209a06adc47
0x99c8a56b5475ae349780bed2531f660464c411809e92935de421d121a471d561
0x37642200bb0cc60928ae6278d9d2e75a4a710207bb345600bedbee34dc14a50b
    ✓ should not allow to transfer tokens locked during MINT (17012ms)
0x115081eb2e926fc25230d16ea5b6be98050f4dac455404b9fe5c9294506130c1
0xb3bb6480efa96c15d20457a0609f040950290815d4a2e6740095bdeaa8bab074
0xe1987fca8f270457ce9f706aa09e55b82fa831e1610e8f27d604de013285546f
0xb39f242297b74f158730f8c7b1e4ce6b2693cd96ffdacd80ac360baa18c66500
0xe71a4642d97f9d85b85ab99a947c614720c265a1cfc9929835d3a15d8bb59962
0x95c85bd1d5c550318d9bcf4684fb9f3f05bf9180aced6b509f5ea40b9ba9d265
0x90f7e8fa5082aaec41f60a8f279dc9212b5dee393249afeb4cc1988eab2a05a5
0x7b95bc60d90e1eb8744977bb690bd1f95df4d20d0e15b40c0a634cc74daa3a46
0x2ff5750b4e74151b05e704bce8ceca95b1e214ea1b01fb7bc924661fd3555269
    ✓ should not allow to transfer locked tokens (25524ms)
0xada7fd451d1e121feb7c2fd77f5d8ea6b4f1ad7b04962a612d0360c304a4df25
0x01efea456fe12fb9bb1ca117d1a1e585c2329d6effb9e97d0bc2f03bd7f3f510
0x1805e80b0fc5ef4b1a749b63ef1818ebebdbcdd3ecfb4e3db14b2e62382b5c27
0x1209a91e73a8a387a51b8610fdfb2c7acd061c4f44b0a9e86cea320dafa32dd5
0x1f5b6dde8f52d18f883d49b5857a8583915b2d2a3b86358ce27899f2fdfc60c3
0x92a99dfe90b8837ac4d327a334381c3f037a5e89d3a2eabdb9f194be7a09b899
    ✓ should not allow flowback from a non-US investor (17298ms)
0x78e67d0646bdf5258fe0fc8201cf91111efea4b5904e1554df041b43b7157903
0x9ffa68841721448f87a5f41fb7be7b7438bb6636633df9fd9814211393f37c6c
0x9c3af3c2ed1cef8b5a007ecc39c283d750f7ffef36cc908670634dfb9f593e9b
0x67878f618d7a0153adf3d8050efa480a1989f3dd708c0fd21a30dcabe237b395
0x1fbfb9ea2f1e3bd0bf317038317433a2849e93a5c681f8e95faa6bb2872d3e1d
0x30360ceadda7230fcf470a3fc61148a5a3e22e5db482941e77bb831d776a11bb
0x36423127dfa9106baa6a4d4d17980d29f6905fc212b1b59cbc53e754cb7de2e2
0x001dd15a64d21fae1ff00c7149502192c54a4e4069c669a90ec1f6034c6320f7
    ✓ should not allow to transfer to a non US accredited account when forceAccreditedUS rule is set (22801ms)

  TRANSACTION handling related tests
    - should speed up a pending transaction by increasing the gas price
    - should cancel a pending transaction
    - should get the next nonce for the identity address
    - should fail with a nonce related error when specifying an invalid nonce
    - should be able to specify a valid nonce and create the transaction
    - should be able to use nonce parameter to generate several transactions before sending them


  42 passing (7m)
  10 pending

```
