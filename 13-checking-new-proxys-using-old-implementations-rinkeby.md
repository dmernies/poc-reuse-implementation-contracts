# Checking integration with etherscan - ERC-20 view

Wi will use the new branch to deploy a new set of proxy contracts point to other implementation contracts in Rinkeby.

The goal is that the new proxy token looks like:
[https://rinkeby.etherscan.io/token/0xc5340Da8503165Ff845EEB0515D97f6906b13071](https://rinkeby.etherscan.io/token/0xc5340Da8503165Ff845EEB0515D97f6906b13071)

We will use implementation contracts of a different token also in Rinkeby. We don't want to touch data about Guille's token because it could be used by QA team.

[https://rinkeby.etherscan.io/token/0x9Df41D3D5F31Afc62CD71007E073AdA1fE6b8074](https://rinkeby.etherscan.io/token/0x9Df41D3D5F31Afc62CD71007E073AdA1fE6b8074)


We can show deployed proxy contracts from dev database finding by deploymentId **2f6349e7-0e09-4c37-bc7a-f1d72bf8049d**

```
complianceServiceRegulated	    0x46fA4fc298DC9BE5E6B1345738E91C5B07D7758D
registryService	                0x77c542AeF3922128F5257D94B219c921A5254B10
token	                        0x9Df41D3D5F31Afc62CD71007E073AdA1fE6b8074
trustService	                0xB8885a5934e7bF0311B7207c83A8b661F44DbCcb
walletRegistrar	                0xCfb4f2BB2e29317C29cc390Cc6EF272D66cb62c3
walletManager	                0x4bdADB8377C9563715d92bBfE2aF0f1165bC33AB
tokenLibrary	                0x265820aAA05BDE8DAe8FA5F8D149e29CB63D048C
investorLockManager	            0xA3e175A74960A6DB2218A2cBf92532E47163880D
complianceConfigurationService	0xD10D8e61A1409663307E3413D1F2c857E7ACE80D
tokenIssuer	                    0x80D6B6C7f39F53e0fF1279512876b4eC8f1993E7
```

## Getting implementation contracts

### ComplianceServiceRegulated

Implementation 0xBa9889F11Ef558cC799D92A7959b41e5AC75Dc61

```bash
diego-securitize@heavy-machine:~/src/dstoken$ npx truffle console --network rinkeby
truffle(rinkeby)> const proxyCompliance = await Proxy.at('0x46fA4fc298DC9BE5E6B1345738E91C5B07D7758D')
undefined
truffle(rinkeby)> proxyCompliance.target.call()
'0xBa9889F11Ef558cC799D92A7959b41e5AC75Dc61'
```

### RegistryService

Implementation: 0xdf3aA4C048428C473E2eC038F9391679660FeA7A

```bash
truffle(rinkeby)> const registryService = await Proxy.at('0x77c542AeF3922128F5257D94B219c921A5254B10')
undefined
truffle(rinkeby)> registryService.target.call()
'0xdf3aA4C048428C473E2eC038F9391679660FeA7A'
```

### Token

Implementation: 0x9ba6B4588d2866B142abF40816b8722158eC30cA

```bash
truffle(rinkeby)> const proxyToken = await Proxy.at('0x9Df41D3D5F31Afc62CD71007E073AdA1fE6b8074')
undefined
truffle(rinkeby)> proxyToken.target.call()
'0x9ba6B4588d2866B142abF40816b8722158eC30cA'
```

### TrustService

Implementation 0x18e42425a6dA13cFC05d0b59bf9446419615747D

```bash
truffle(rinkeby)> const proxyTrustServvice = await Proxy.at('0xB8885a5934e7bF0311B7207c83A8b661F44DbCcb')
undefined
truffle(rinkeby)> proxyTrustServvice.target.call()
'0x18e42425a6dA13cFC05d0b59bf9446419615747D'
```

### WalletRegistrar

Implementation: 0x89B694A97A0732ed05C1408E30bda4eB5141fDDe
```bash
truffle(rinkeby)> const proxyWallet = await Proxy.at('0xCfb4f2BB2e29317C29cc390Cc6EF272D66cb62c3')
undefined
truffle(rinkeby)> proxyWallet.target.call()
'0x89B694A97A0732ed05C1408E30bda4eB5141fDDe'
```

### walletManager

Implementation: 0x7867B2C92CEc4A6b6383d083E1eF45fB189e87e9

```bash
truffle(rinkeby)> const walletManagerProxy = await Proxy.at('0x4bdADB8377C9563715d92bBfE2aF0f1165bC33AB')
undefined
truffle(rinkeby)> walletManagerProxy.target.call()
'0x7867B2C92CEc4A6b6383d083E1eF45fB189e87e9'
```

### investorLockManager

Implementation 0x1bb634B921F2321803BC55959091a348b2654c74

```bash
truffle(rinkeby)> const proxyLockManager = await Proxy.at('0xA3e175A74960A6DB2218A2cBf92532E47163880D')
undefined
truffle(rinkeby)> proxyLockManager.target.call()
'0x1bb634B921F2321803BC55959091a348b2654c74'
```

### complianceConfigurationService

Implementation: 0x91dB195ae8d9C453Ca9FFF9B55ce361eE365aD87

```bash
truffle(rinkeby)> const proxyConfigService = await Proxy.at('0xD10D8e61A1409663307E3413D1F2c857E7ACE80D')
undefined
truffle(rinkeby)> proxyConfigService.target.call()
'0x91dB195ae8d9C453Ca9FFF9B55ce361eE365aD87'
```

### TokenIssuer

Implementation: 0x39f53632CBf62cc68F42318c45c7888c0FDae4b5

```bash
truffle(rinkeby)> const proxyTokenIssuer = await Proxy.at('0x80D6B6C7f39F53e0fF1279512876b4eC8f1993E7')
undefined
truffle(rinkeby)> proxyTokenIssuer.target.call()
'0x39f53632CBf62cc68F42318c45c7888c0FDae4b5'
```

## Deploying

```bash
truffle migrate --name NewProxies2 --symbol NewProxies2 --decimals 0 --network rinkeby --owners "0xe2099bCfCA27f8cae93FD3C8726e4a8250FcA015 0x0a065Eaf3b6e037F8213fa13e76A025d2846Bd48" --required_confirmations 2 --no_omnibus_wallet
```


```
Starting migrations...
======================
> Network name:    'rinkeby'
> Network id:      4
> Block gas limit: 10000000 (0x989680)


1_initial_migration.js
======================
{
  _: [ 'migrate' ],
  name: 'NewProxies2',
  symbol: 'NewProxies2',
  decimals: 0,
  network: 'rinkeby',
  owners: '0xe2099bCfCA27f8cae93FD3C8726e4a8250FcA015 0x0a065Eaf3b6e037F8213fa13e76A025d2846Bd48',
  required_confirmations: 2,
  no_omnibus_wallet: true
}

   Deploying 'Migrations'
   ----------------------
   > transaction hash:    0x33a7cdc6c36b51ae2597108c93cd8f04dd6457863f950ba3e1e80abd8eb246a6
   > Blocks: 2            Seconds: 22
   > contract address:    0x1B868346bB460d6E45E64fcFDa57fd4b9892f2A2
   > block number:        7980974
   > block timestamp:     1611942830
   > account:             0xd51128F302755666c42e3920d72FF2FE632856a9
   > balance:             22.8952790388
   > gas used:            274286 (0x42f6e)
   > gas price:           20 gwei
   > value sent:          0 ETH
   > total cost:          0.00548572 ETH


   > Saving migration to chain.
   > Saving artifacts
   -------------------------------------
   > Total cost:          0.00548572 ETH


2_deploy_trust_manager.js
=========================

   Deploying 'Proxy'
   -----------------
   > transaction hash:    0xfff6af79ca21bff98fae04eaff9823aa9c93ccca037a01c4e16839ae9e097d16
   > Blocks: 0            Seconds: 5
   > contract address:    0x636Bc254E8D83845281ce606F91cBd34A460f401
   > block number:        7980976
   > block timestamp:     1611942860
   > account:             0xd51128F302755666c42e3920d72FF2FE632856a9
   > balance:             22.8896872788
   > gas used:            237287 (0x39ee7)
   > gas price:           20 gwei
   > value sent:          0 ETH
   > total cost:          0.00474574 ETH


   > Saving migration to chain.
   > Saving artifacts
   -------------------------------------
   > Total cost:          0.00474574 ETH


3_deploy_registry.js
====================

   Replacing 'Proxy'
   -----------------
   > transaction hash:    0x4639146ad50168f2af4a1ea1fd1cd94e6ffb4c4b57ff7d9e1cc21309c8826605
   > Blocks: 1            Seconds: 9
   > contract address:    0x68593Fb1B5A36bf8a38874d00156a1921fffDCde
   > block number:        7980980
   > block timestamp:     1611942920
   > account:             0xd51128F302755666c42e3920d72FF2FE632856a9
   > balance:             22.8808115788
   > gas used:            237287 (0x39ee7)
   > gas price:           20 gwei
   > value sent:          0 ETH
   > total cost:          0.00474574 ETH


   > Saving migration to chain.
   > Saving artifacts
   -------------------------------------
   > Total cost:          0.00474574 ETH


4_deploy_compliance_manager.js
==============================

   Replacing 'Proxy'
   -----------------
   > transaction hash:    0x37bb13e392bbb6786a03d647269d5b82ab881be09b553c7207dc0642587378b4
   > Blocks: 0            Seconds: 13
   > contract address:    0x6C2F7240E2ADd025d37AbdCC45bd4895757c7BDE
   > block number:        7980985
   > block timestamp:     1611942995
   > account:             0xd51128F302755666c42e3920d72FF2FE632856a9
   > balance:             22.8714546588
   > gas used:            237287 (0x39ee7)
   > gas price:           20 gwei
   > value sent:          0 ETH
   > total cost:          0.00474574 ETH


   > Saving migration to chain.
   > Saving artifacts
   -------------------------------------
   > Total cost:          0.00474574 ETH


5_deploy_complianceConfiguration.js
===================================

   Replacing 'Proxy'
   -----------------
   > transaction hash:    0xe4dbf03f7ddc0cf079fe496d2792cb12a0d208afcd271a761daebe342e65a966
   > Blocks: 2            Seconds: 17
   > contract address:    0x3024649e6581D34d52C5Eb02C32E8E4a795445C6
   > block number:        7980990
   > block timestamp:     1611943070
   > account:             0xd51128F302755666c42e3920d72FF2FE632856a9
   > balance:             22.8611447988
   > gas used:            237287 (0x39ee7)
   > gas price:           20 gwei
   > value sent:          0 ETH
   > total cost:          0.00474574 ETH


   > Saving migration to chain.
   > Saving artifacts
   -------------------------------------
   > Total cost:          0.00474574 ETH


6_deploy_wallet_manager.js
==========================

   Replacing 'Proxy'
   -----------------
   > transaction hash:    0x70f4430639f549959b9f621615ab9f0f1590abbec19fd5946fcd1e3dae8d654b
   > Blocks: 0            Seconds: 0
   > contract address:    0x11498a56Df28Ef342A02838De8e7c837bc3f6E4E
   > block number:        7980994
   > block timestamp:     1611943130
   > account:             0xd51128F302755666c42e3920d72FF2FE632856a9
   > balance:             22.8517870188
   > gas used:            237287 (0x39ee7)
   > gas price:           20 gwei
   > value sent:          0 ETH
   > total cost:          0.00474574 ETH


   > Saving migration to chain.
   > Saving artifacts
   -------------------------------------
   > Total cost:          0.00474574 ETH


7_deploy_lock_manager.js
========================

   Replacing 'Proxy'
   -----------------
   > transaction hash:    0x454897cde369e15944fb243e1076047682bdd9a2a78e65e6b26027fe03f66e25
   > Blocks: 1            Seconds: 9
   > contract address:    0xb32069E0e5A0356bFE365198D56eC3fEb9Da3ac8
   > block number:        7980998
   > block timestamp:     1611943190
   > account:             0xd51128F302755666c42e3920d72FF2FE632856a9
   > balance:             22.8424265988
   > gas used:            237287 (0x39ee7)
   > gas price:           20 gwei
   > value sent:          0 ETH
   > total cost:          0.00474574 ETH


   > Saving migration to chain.
   > Saving artifacts
   -------------------------------------
   > Total cost:          0.00474574 ETH


8_deploy_partitions_manager.js
==============================

   > Saving migration to chain.
   -------------------------------------
   > Total cost:                   0 ETH


9_deploy_token.js
=================
ConfigurationManager {
  proxiesAddresses: {
    TrustService: '0x636Bc254E8D83845281ce606F91cBd34A460f401',
    RegistryService: '0x68593Fb1B5A36bf8a38874d00156a1921fffDCde',
    ComplianceServiceRegulated: '0x6C2F7240E2ADd025d37AbdCC45bd4895757c7BDE',
    ComplianceConfigurationService: '0x3024649e6581D34d52C5Eb02C32E8E4a795445C6',
    WalletManager: '0x11498a56Df28Ef342A02838De8e7c837bc3f6E4E',
    InvestorLockManager: '0xb32069E0e5A0356bFE365198D56eC3fEb9Da3ac8',
    DSToken: '0x644479e80d54A27F299F4022b0e527DfD2a9eF76',
    TokenIssuer: '0xC32bb6243A67248fBF2BfFe683774785b33cA258',
    WalletRegistrar: '0x318CfAa09c39D6905b509ccE7384f9107a351F5b'
  },
  decimals: 0,
  name: 'NewProxies2',
  symbol: 'NewProxies2',
  owners: [
    '0xe2099bCfCA27f8cae93FD3C8726e4a8250FcA015',
    '0x0a065Eaf3b6e037F8213fa13e76A025d2846Bd48'
  ],
  requiredConfirmations: 2,
  chainId: 1,
  complianceManagerType: 'NORMAL',
  lockManagerType: 'INVESTOR',
  noRegistry: undefined,
  partitioned: undefined,
  noOmnibusWallet: true,
  omnibusWallet: undefined
}

   Replacing 'Proxy'
   -----------------
   > transaction hash:    0x84853f8cdb2e19c1a5e5eeb14ce5d9678b4ebd71ceae06df9a6f6ae56409a29f
   > Blocks: 1            Seconds: 9
   > contract address:    0x644479e80d54A27F299F4022b0e527DfD2a9eF76
   > block number:        7981003
   > block timestamp:     1611943265
   > account:             0xd51128F302755666c42e3920d72FF2FE632856a9
   > balance:             22.8325214788
   > gas used:            237287 (0x39ee7)
   > gas price:           20 gwei
   > value sent:          0 ETH
   > total cost:          0.00474574 ETH


   > Saving migration to chain.
   > Saving artifacts
   -------------------------------------
   > Total cost:          0.00474574 ETH


10_deploy_token_issuer.js
=========================

   Replacing 'Proxy'
   -----------------
   > transaction hash:    0xcc9e5a55ff7f961956c27e6987a928d1fbe593be50fc73dbd44164d8d153a62d
   > Blocks: 1            Seconds: 9
   > contract address:    0xC32bb6243A67248fBF2BfFe683774785b33cA258
   > block number:        7981007
   > block timestamp:     1611943325
   > account:             0xd51128F302755666c42e3920d72FF2FE632856a9
   > balance:             22.8218095788
   > gas used:            237287 (0x39ee7)
   > gas price:           20 gwei
   > value sent:          0 ETH
   > total cost:          0.00474574 ETH


   > Saving migration to chain.
   > Saving artifacts
   -------------------------------------
   > Total cost:          0.00474574 ETH


11_deploy_omnibus_wallet_controller.js
======================================
Skipping omnibus wallet controller

   > Saving migration to chain.
   -------------------------------------
   > Total cost:                   0 ETH


12_deploy_wallet_registrar.js
=============================

   Replacing 'Proxy'
   -----------------
   > transaction hash:    0x8b27d398c92508b70e9d74892702d1774215110489e786072c6ad15627daaed5
   > Blocks: 1            Seconds: 9
   > contract address:    0x318CfAa09c39D6905b509ccE7384f9107a351F5b
   > block number:        7981014
   > block timestamp:     1611943430
   > account:             0xd51128F302755666c42e3920d72FF2FE632856a9
   > balance:             22.8119035788
   > gas used:            237287 (0x39ee7)
   > gas price:           20 gwei
   > value sent:          0 ETH
   > total cost:          0.00474574 ETH


   > Saving migration to chain.
   > Saving artifacts
   -------------------------------------
   > Total cost:          0.00474574 ETH


13_set_roles.js
===============
Give issuer permissions to token issuer
Give issuer permissions to wallet registrar

   > Saving migration to chain.
   -------------------------------------
   > Total cost:                   0 ETH


14_set_services.js
==================
Connecting compliance configuration to trust service
Connecting compliance manager to trust service
Connecting compliance manager to compliance configuration service
Connecting compliance manager to wallet manager
Connecting compliance manager to lock manager
Connecting compliance service to token
Connecting registry to trust service
Connecting registry to wallet manager
Connecting registry to token
Connecting registry to compliance service
Connecting token to registry
Connecting token to token issuer
Connecting token to wallet registrar
Connecting token issuer to registry
Connecting wallet registrar to registry
Connecting wallet manager to registry
Connecting lock manager to registry
Connecting compliance manager to registry
Connecting token to trust service
Connecting token to compliance service
Connecting token to compliance configuration service
Connecting token to wallet manager
Connecting token to lock manager
Connecting wallet manager to trust service
Connecting lock manager to trust service
Connecting lock manager to compliance service
Connecting lock manager to token
Connecting token issuer to trust service
Connecting token issuer to lock manager
Connecting token issuer to token
Connecting wallet registrar to trust service


Token "NewProxies2" (NewProxies2) [decimals: 0] deployment complete
-------------------------
Token is at address: 0x644479e80d54A27F299F4022b0e527DfD2a9eF76 | Version: 3,4,5,4,4
Trust service is at address: 0x636Bc254E8D83845281ce606F91cBd34A460f401 | Version: 3,3
Investor registry is at address: 0x68593Fb1B5A36bf8a38874d00156a1921fffDCde | Version: 4,4,5,4
Compliance service is at address: 0x6C2F7240E2ADd025d37AbdCC45bd4895757c7BDE, and is of type NORMAL | Version: 5,4,5,5,3,8
Compliance configuration service is at address: 0x3024649e6581D34d52C5Eb02C32E8E4a795445C6 | Version: 5,4,5,5
Wallet manager is at address: 0x11498a56Df28Ef342A02838De8e7c837bc3f6E4E | Version: 3,4,5,3
Lock manager is at address: 0xb32069E0e5A0356bFE365198D56eC3fEb9Da3ac8, and is of type INVESTOR. | Version: 3,4,5,3
Token issuer is at address: 0xC32bb6243A67248fBF2BfFe683774785b33cA258 | Version: 3,4,5,3
Wallet registrar is at address: 0x318CfAa09c39D6905b509ccE7384f9107a351F5b | Version: 3,4,5,3
```

ERC-20 integration was OK!
