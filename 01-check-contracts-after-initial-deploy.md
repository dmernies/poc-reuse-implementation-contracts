# Checks after initial deploy

## Implementation storage after initial deploy

- 0x6Bf2a1a0907DCcbAed513bc0De7cD027b9EaD028
- 0x597c515ece0f68Ba1528Ee9aa4c38E3aD9C82D45
- 0x5f14051c79fEC81f89b3790D8F2427ed5b25F817
- 0x0C765D9b48d14794b9036C4A4543Fd23AaEe06E4
- 0x66cC3E64C7fA28364Ee74C6e376686d87E02475c
- 0x8a27B3748D2D996a1ed7737cb6Ca29861CAf44A1
- 0xa4E9916B1fa346750a5fA97041Fe475dcc6C476F
- 0x5421717658233442D50D56Df4e5DC37819980d59
- 0xeb3F2c390Dd8f767F6bf1d8b85C4B0b7576e754e
- 0xf86F48b08C75c9D553397ca89E6a5FC37072AACE

```js
console.log('Checking storage of token implementation ');
truffle(development)> web3.eth.getStorageAt('0x6Bf2a1a0907DCcbAed513bc0De7cD027b9EaD028', 0) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0x6Bf2a1a0907DCcbAed513bc0De7cD027b9EaD028', 1) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0x6Bf2a1a0907DCcbAed513bc0De7cD027b9EaD028', 2) 
'0x01'
truffle(development)> web3.eth.getStorageAt('0x6Bf2a1a0907DCcbAed513bc0De7cD027b9EaD028', 3) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0x6Bf2a1a0907DCcbAed513bc0De7cD027b9EaD028', 4) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0x6Bf2a1a0907DCcbAed513bc0De7cD027b9EaD028', 5) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0x6Bf2a1a0907DCcbAed513bc0De7cD027b9EaD028', 6) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0x6Bf2a1a0907DCcbAed513bc0De7cD027b9EaD028', 7) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0x6Bf2a1a0907DCcbAed513bc0De7cD027b9EaD028', 8)
'0x0'
truffle(development)> web3.eth.getStorageAt('0x6Bf2a1a0907DCcbAed513bc0De7cD027b9EaD028', 9)
'0x0'
truffle(development)> web3.eth.getStorageAt('0x6Bf2a1a0907DCcbAed513bc0De7cD027b9EaD028', 10)
'0x0'
truffle(development)> web3.eth.getStorageAt('0x6Bf2a1a0907DCcbAed513bc0De7cD027b9EaD028', 11)
'0x0'
truffle(development)> web3.eth.getStorageAt('0x6Bf2a1a0907DCcbAed513bc0De7cD027b9EaD028', 12)
'0x0'

```

```js
console.log('Checking storage of token implementation ');
truffle(development)> web3.eth.getStorageAt('0x597c515ece0f68Ba1528Ee9aa4c38E3aD9C82D45', 0) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0x597c515ece0f68Ba1528Ee9aa4c38E3aD9C82D45', 1) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0x597c515ece0f68Ba1528Ee9aa4c38E3aD9C82D45', 2) 
'0x01'
truffle(development)> web3.eth.getStorageAt('0x597c515ece0f68Ba1528Ee9aa4c38E3aD9C82D45', 3) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0x597c515ece0f68Ba1528Ee9aa4c38E3aD9C82D45', 4) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0x597c515ece0f68Ba1528Ee9aa4c38E3aD9C82D45', 5)
'0x0'
truffle(development)> web3.eth.getStorageAt('0x597c515ece0f68Ba1528Ee9aa4c38E3aD9C82D45', 6)
'0x0'
truffle(development)> web3.eth.getStorageAt('0x597c515ece0f68Ba1528Ee9aa4c38E3aD9C82D45', 7)
'0x0'
truffle(development)> web3.eth.getStorageAt('0x597c515ece0f68Ba1528Ee9aa4c38E3aD9C82D45', 8)
'0x0'
truffle(development)> web3.eth.getStorageAt('0x597c515ece0f68Ba1528Ee9aa4c38E3aD9C82D45', 9)
'0x0'
truffle(development)> web3.eth.getStorageAt('0x597c515ece0f68Ba1528Ee9aa4c38E3aD9C82D45', 10)
'0x0'
truffle(development)> web3.eth.getStorageAt('0x597c515ece0f68Ba1528Ee9aa4c38E3aD9C82D45', 11)
'0x0'
truffle(development)> web3.eth.getStorageAt('0x597c515ece0f68Ba1528Ee9aa4c38E3aD9C82D45', 12)

```

```js
console.log('Checking storage of token implementation ');
truffle(development)> web3.eth.getStorageAt('0x5f14051c79fEC81f89b3790D8F2427ed5b25F817', 0) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0x5f14051c79fEC81f89b3790D8F2427ed5b25F817', 1) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0x5f14051c79fEC81f89b3790D8F2427ed5b25F817', 2) 
'0x01'
truffle(development)> web3.eth.getStorageAt('0x5f14051c79fEC81f89b3790D8F2427ed5b25F817', 3) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0x5f14051c79fEC81f89b3790D8F2427ed5b25F817', 4) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0x5f14051c79fEC81f89b3790D8F2427ed5b25F817', 5) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0x5f14051c79fEC81f89b3790D8F2427ed5b25F817', 6) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0x5f14051c79fEC81f89b3790D8F2427ed5b25F817', 7)
'0x0'                                                              ^
truffle(development)> web3.eth.getStorageAt('0x5f14051c79fEC81f89b3790D8F2427ed5b25F817', 8)
'0x0'
truffle(development)> web3.eth.getStorageAt('0x5f14051c79fEC81f89b3790D8F2427ed5b25F817', 9)
'0x0'
truffle(development)> web3.eth.getStorageAt('0x5f14051c79fEC81f89b3790D8F2427ed5b25F817', 10)
'0x0'
truffle(development)> web3.eth.getStorageAt('0x5f14051c79fEC81f89b3790D8F2427ed5b25F817', 11)
'0x0'
truffle(development)> web3.eth.getStorageAt('0x5f14051c79fEC81f89b3790D8F2427ed5b25F817', 12)
'0x0'
truffle(development)> web3.eth.getStorageAt('0x5f14051c79fEC81f89b3790D8F2427ed5b25F817', 12)
'0x0'
truffle(development)> web3.eth.getStorageAt('0x5f14051c79fEC81f89b3790D8F2427ed5b25F817', 14)
'0x0'

```

```js
console.log('Checking storage of compliance configuration token ');
truffle(development)> web3.eth.getStorageAt('0xf86F48b08C75c9D553397ca89E6a5FC37072AACE', 0) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0xf86F48b08C75c9D553397ca89E6a5FC37072AACE', 1) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0xf86F48b08C75c9D553397ca89E6a5FC37072AACE', 2) 
'0x01'
truffle(development)> web3.eth.getStorageAt('0xf86F48b08C75c9D553397ca89E6a5FC37072AACE', 3) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0xf86F48b08C75c9D553397ca89E6a5FC37072AACE', 4) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0xf86F48b08C75c9D553397ca89E6a5FC37072AACE', 5) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0xf86F48b08C75c9D553397ca89E6a5FC37072AACE', 6) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0xf86F48b08C75c9D553397ca89E6a5FC37072AACE', 7) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0xf86F48b08C75c9D553397ca89E6a5FC37072AACE', 8) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0xf86F48b08C75c9D553397ca89E6a5FC37072AACE', 9) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0xf86F48b08C75c9D553397ca89E6a5FC37072AACE', 10)
'0x0'
truffle(development)> web3.eth.getStorageAt('0xf86F48b08C75c9D553397ca89E6a5FC37072AACE', 11)
'0x0'
truffle(development)> web3.eth.getStorageAt('0xf86F48b08C75c9D553397ca89E6a5FC37072AACE', 12)
'0x0'
```

```js
console.log('Checking storage of token proxy ');
truffle(development)> web3.eth.getStorageAt('0x0C765D9b48d14794b9036C4A4543Fd23AaEe06E4', 0) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0x0C765D9b48d14794b9036C4A4543Fd23AaEe06E4', 1) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0x0C765D9b48d14794b9036C4A4543Fd23AaEe06E4', 2) 
'0x01'
truffle(development)> web3.eth.getStorageAt('0x0C765D9b48d14794b9036C4A4543Fd23AaEe06E4', 3) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0x0C765D9b48d14794b9036C4A4543Fd23AaEe06E4', 4) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0x0C765D9b48d14794b9036C4A4543Fd23AaEe06E4', 5) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0x0C765D9b48d14794b9036C4A4543Fd23AaEe06E4', 6) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0x0C765D9b48d14794b9036C4A4543Fd23AaEe06E4', 7) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0x0C765D9b48d14794b9036C4A4543Fd23AaEe06E4', 8) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0x0C765D9b48d14794b9036C4A4543Fd23AaEe06E4', 9) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0x0C765D9b48d14794b9036C4A4543Fd23AaEe06E4', 10) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0x0C765D9b48d14794b9036C4A4543Fd23AaEe06E4', 11) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0x0C765D9b48d14794b9036C4A4543Fd23AaEe06E4', 12) 
```

```js
truffle(development)> web3.eth.getStorageAt('0x66cC3E64C7fA28364Ee74C6e376686d87E02475c', 0) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0x66cC3E64C7fA28364Ee74C6e376686d87E02475c', 1) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0x66cC3E64C7fA28364Ee74C6e376686d87E02475c', 2) 
'0x01'
truffle(development)> web3.eth.getStorageAt('0x66cC3E64C7fA28364Ee74C6e376686d87E02475c', 3) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0x66cC3E64C7fA28364Ee74C6e376686d87E02475c', 4)
'0x0'
truffle(development)> web3.eth.getStorageAt('0x66cC3E64C7fA28364Ee74C6e376686d87E02475c', 5)
'0x0'
truffle(development)> web3.eth.getStorageAt('0x66cC3E64C7fA28364Ee74C6e376686d87E02475c', 6)
'0x0'
truffle(development)> web3.eth.getStorageAt('0x66cC3E64C7fA28364Ee74C6e376686d87E02475c', 7)
'0x0'
truffle(development)> web3.eth.getStorageAt('0x66cC3E64C7fA28364Ee74C6e376686d87E02475c', 8)
'0x0'
truffle(development)> web3.eth.getStorageAt('0x66cC3E64C7fA28364Ee74C6e376686d87E02475c', 9)
'0x0'
truffle(development)> web3.eth.getStorageAt('0x66cC3E64C7fA28364Ee74C6e376686d87E02475c', 10)
'0x0'
truffle(development)> web3.eth.getStorageAt('0x66cC3E64C7fA28364Ee74C6e376686d87E02475c', 11)
'0x0'
truffle(development)> web3.eth.getStorageAt('0x66cC3E64C7fA28364Ee74C6e376686d87E02475c', 12)
'0x0'
```

```js
truffle(development)> web3.eth.getStorageAt('0x8a27B3748D2D996a1ed7737cb6Ca29861CAf44A1', 0) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0x8a27B3748D2D996a1ed7737cb6Ca29861CAf44A1', 1) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0x8a27B3748D2D996a1ed7737cb6Ca29861CAf44A1', 2) 
'0x01'
truffle(development)> web3.eth.getStorageAt('0x8a27B3748D2D996a1ed7737cb6Ca29861CAf44A1', 3) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0x8a27B3748D2D996a1ed7737cb6Ca29861CAf44A1', 4) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0x8a27B3748D2D996a1ed7737cb6Ca29861CAf44A1', 5) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0x8a27B3748D2D996a1ed7737cb6Ca29861CAf44A1', 6) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0x8a27B3748D2D996a1ed7737cb6Ca29861CAf44A1', 7) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0x8a27B3748D2D996a1ed7737cb6Ca29861CAf44A1', 8) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0x8a27B3748D2D996a1ed7737cb6Ca29861CAf44A1', 9) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0x8a27B3748D2D996a1ed7737cb6Ca29861CAf44A1', 10) 
'0x0'
```

```js
truffle(development)> web3.eth.getStorageAt('0xa4E9916B1fa346750a5fA97041Fe475dcc6C476F', 0)
'0x0'
truffle(development)> web3.eth.getStorageAt('0xa4E9916B1fa346750a5fA97041Fe475dcc6C476F', 1)
'0x0'
truffle(development)> web3.eth.getStorageAt('0xa4E9916B1fa346750a5fA97041Fe475dcc6C476F', 2)
'0x01'
truffle(development)> web3.eth.getStorageAt('0xa4E9916B1fa346750a5fA97041Fe475dcc6C476F', 3)
'0x0'
truffle(development)> web3.eth.getStorageAt('0xa4E9916B1fa346750a5fA97041Fe475dcc6C476F', 4)
'0x0'
truffle(development)> web3.eth.getStorageAt('0xa4E9916B1fa346750a5fA97041Fe475dcc6C476F', 5)
'0x0'
truffle(development)> web3.eth.getStorageAt('0xa4E9916B1fa346750a5fA97041Fe475dcc6C476F', 6)
'0x0'
truffle(development)> web3.eth.getStorageAt('0xa4E9916B1fa346750a5fA97041Fe475dcc6C476F', 7)
'0x0'
truffle(development)> web3.eth.getStorageAt('0xa4E9916B1fa346750a5fA97041Fe475dcc6C476F', 8)
'0x0'
truffle(development)> web3.eth.getStorageAt('0xa4E9916B1fa346750a5fA97041Fe475dcc6C476F', 9)
'0x0'
truffle(development)> web3.eth.getStorageAt('0xa4E9916B1fa346750a5fA97041Fe475dcc6C476F', 10)
'0x0'
truffle(development)> web3.eth.getStorageAt('0xa4E9916B1fa346750a5fA97041Fe475dcc6C476F', 11)
'0x0'
truffle(development)> web3.eth.getStorageAt('0xa4E9916B1fa346750a5fA97041Fe475dcc6C476F', 12)
'0x0'
```

```js
truffle(development)> web3.eth.getStorageAt('0x5421717658233442D50D56Df4e5DC37819980d59', 0) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0x5421717658233442D50D56Df4e5DC37819980d59', 1) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0x5421717658233442D50D56Df4e5DC37819980d59', 2) 
'0x01'
truffle(development)> web3.eth.getStorageAt('0x5421717658233442D50D56Df4e5DC37819980d59', 3) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0x5421717658233442D50D56Df4e5DC37819980d59', 4) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0x5421717658233442D50D56Df4e5DC37819980d59', 5) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0x5421717658233442D50D56Df4e5DC37819980d59', 6) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0x5421717658233442D50D56Df4e5DC37819980d59', 7) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0x5421717658233442D50D56Df4e5DC37819980d59', 8) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0x5421717658233442D50D56Df4e5DC37819980d59', 9) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0x5421717658233442D50D56Df4e5DC37819980d59', 10) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0x5421717658233442D50D56Df4e5DC37819980d59', 11) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0x5421717658233442D50D56Df4e5DC37819980d59', 12) 
'0x0'
```

```js
truffle(development)> web3.eth.getStorageAt('0xeb3F2c390Dd8f767F6bf1d8b85C4B0b7576e754e', 0) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0xeb3F2c390Dd8f767F6bf1d8b85C4B0b7576e754e', 1) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0xeb3F2c390Dd8f767F6bf1d8b85C4B0b7576e754e', 2) 
'0x01'
truffle(development)> web3.eth.getStorageAt('0xeb3F2c390Dd8f767F6bf1d8b85C4B0b7576e754e', 3) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0xeb3F2c390Dd8f767F6bf1d8b85C4B0b7576e754e', 4) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0xeb3F2c390Dd8f767F6bf1d8b85C4B0b7576e754e', 5) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0xeb3F2c390Dd8f767F6bf1d8b85C4B0b7576e754e', 6) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0xeb3F2c390Dd8f767F6bf1d8b85C4B0b7576e754e', 7) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0xeb3F2c390Dd8f767F6bf1d8b85C4B0b7576e754e', 8) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0xeb3F2c390Dd8f767F6bf1d8b85C4B0b7576e754e', 9) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0xeb3F2c390Dd8f767F6bf1d8b85C4B0b7576e754e', 10) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0xeb3F2c390Dd8f767F6bf1d8b85C4B0b7576e754e', 11) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0xeb3F2c390Dd8f767F6bf1d8b85C4B0b7576e754e', 12) 
'0x0'
```

## Proxy storage after initial deploy

- 0xeCf7CbA7637Faf97F4CCddAde4C91D2FE69c13a8
- 0x17C68AcDA7453192bA57567bBef52e5d230293Ff
- 0x86Cd5d893C2aE7F8dFbdDC6972A204a505D2A63f
- 0x634c4cAc02bf2168299A4834438CB05340EF15bc
- 0x10CD7fD6d2c59EE5c2d42031a343F09CeBA12D06
- 0xc965d94aCf8C0Cf8D5aE1B985384999eba4CA0F7
- 0xd46e66Abff6995a61dbD6A6aCCAD6037bFD33376
- 0x973dcA232B4f794aA5bF2FbEd723359563C92E78
- 0xa054560a1e76D547a1E75E2a31d3D9C98d3B75B6
- 0x0a4992c7d794f2030a699d4b99418a98fb3b4b8c


```js
truffle(development)> web3.eth.getStorageAt('0xeCf7CbA7637Faf97F4CCddAde4C91D2FE69c13a8', 0) 
'0x33cc6c1c002311d26b561e1c8df3466f62bae1f9'
truffle(development)> web3.eth.getStorageAt('0xeCf7CbA7637Faf97F4CCddAde4C91D2FE69c13a8', 1) 
'0x016bf2a1a0907dccbaed513bc0de7cd027b9ead028'
truffle(development)> web3.eth.getStorageAt('0xeCf7CbA7637Faf97F4CCddAde4C91D2FE69c13a8', 2) 
'0x04'
truffle(development)> web3.eth.getStorageAt('0xeCf7CbA7637Faf97F4CCddAde4C91D2FE69c13a8', 3) 
'0x33cc6c1c002311d26b561e1c8df3466f62bae1f9'
truffle(development)> web3.eth.getStorageAt('0xeCf7CbA7637Faf97F4CCddAde4C91D2FE69c13a8', 4) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0xeCf7CbA7637Faf97F4CCddAde4C91D2FE69c13a8', 5) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0xeCf7CbA7637Faf97F4CCddAde4C91D2FE69c13a8', 6) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0xeCf7CbA7637Faf97F4CCddAde4C91D2FE69c13a8', 7) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0xeCf7CbA7637Faf97F4CCddAde4C91D2FE69c13a8', 8) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0xeCf7CbA7637Faf97F4CCddAde4C91D2FE69c13a8', 9) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0xeCf7CbA7637Faf97F4CCddAde4C91D2FE69c13a8', 10) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0xeCf7CbA7637Faf97F4CCddAde4C91D2FE69c13a8', 11) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0xeCf7CbA7637Faf97F4CCddAde4C91D2FE69c13a8', 12) 
'0x0'
```

```js
truffle(development)> web3.eth.getStorageAt('0x17C68AcDA7453192bA57567bBef52e5d230293Ff', 0) 
'0x33cc6c1c002311d26b561e1c8df3466f62bae1f9'
truffle(development)> web3.eth.getStorageAt('0x17C68AcDA7453192bA57567bBef52e5d230293Ff', 1) 
'0x01597c515ece0f68ba1528ee9aa4c38e3ad9c82d45'
truffle(development)> web3.eth.getStorageAt('0x17C68AcDA7453192bA57567bBef52e5d230293Ff', 2) 
'0x06'
truffle(development)> web3.eth.getStorageAt('0x17C68AcDA7453192bA57567bBef52e5d230293Ff', 3) 
'0x33cc6c1c002311d26b561e1c8df3466f62bae1f9'
truffle(development)> web3.eth.getStorageAt('0x17C68AcDA7453192bA57567bBef52e5d230293Ff', 4) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0x17C68AcDA7453192bA57567bBef52e5d230293Ff', 5) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0x17C68AcDA7453192bA57567bBef52e5d230293Ff', 6) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0x17C68AcDA7453192bA57567bBef52e5d230293Ff', 7) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0x17C68AcDA7453192bA57567bBef52e5d230293Ff', 8) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0x17C68AcDA7453192bA57567bBef52e5d230293Ff', 9) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0x17C68AcDA7453192bA57567bBef52e5d230293Ff', 10) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0x17C68AcDA7453192bA57567bBef52e5d230293Ff', 11) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0x17C68AcDA7453192bA57567bBef52e5d230293Ff', 12) 
'0x0'
```

```js
truffle(development)> web3.eth.getStorageAt('0x86Cd5d893C2aE7F8dFbdDC6972A204a505D2A63f', 0) 
'0x33cc6c1c002311d26b561e1c8df3466f62bae1f9'
truffle(development)> web3.eth.getStorageAt('0x86Cd5d893C2aE7F8dFbdDC6972A204a505D2A63f', 1) 
'0x015f14051c79fec81f89b3790d8f2427ed5b25f817'
truffle(development)> web3.eth.getStorageAt('0x86Cd5d893C2aE7F8dFbdDC6972A204a505D2A63f', 2) 
'0x05'
truffle(development)> web3.eth.getStorageAt('0x86Cd5d893C2aE7F8dFbdDC6972A204a505D2A63f', 3) 
'0x33cc6c1c002311d26b561e1c8df3466f62bae1f9'
truffle(development)> web3.eth.getStorageAt('0x86Cd5d893C2aE7F8dFbdDC6972A204a505D2A63f', 4) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0x86Cd5d893C2aE7F8dFbdDC6972A204a505D2A63f', 5) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0x86Cd5d893C2aE7F8dFbdDC6972A204a505D2A63f', 6) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0x86Cd5d893C2aE7F8dFbdDC6972A204a505D2A63f', 7) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0x86Cd5d893C2aE7F8dFbdDC6972A204a505D2A63f', 8) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0x86Cd5d893C2aE7F8dFbdDC6972A204a505D2A63f', 9) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0x86Cd5d893C2aE7F8dFbdDC6972A204a505D2A63f', 10) 
'0x0'
```

```js
truffle(development)> web3.eth.getStorageAt('0x634c4cAc02bf2168299A4834438CB05340EF15bc', 0) 
'0x33cc6c1c002311d26b561e1c8df3466f62bae1f9'
truffle(development)> web3.eth.getStorageAt('0x634c4cAc02bf2168299A4834438CB05340EF15bc', 1) 
'0x010c765d9b48d14794b9036c4a4543fd23aaee06e4'
truffle(development)> web3.eth.getStorageAt('0x634c4cAc02bf2168299A4834438CB05340EF15bc', 2) 
'0x04'
truffle(development)> web3.eth.getStorageAt('0x634c4cAc02bf2168299A4834438CB05340EF15bc', 3) 
'0x33cc6c1c002311d26b561e1c8df3466f62bae1f9'
truffle(development)> web3.eth.getStorageAt('0x634c4cAc02bf2168299A4834438CB05340EF15bc', 4) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0x634c4cAc02bf2168299A4834438CB05340EF15bc', 5) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0x634c4cAc02bf2168299A4834438CB05340EF15bc', 6) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0x634c4cAc02bf2168299A4834438CB05340EF15bc', 7) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0x634c4cAc02bf2168299A4834438CB05340EF15bc', 8) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0x634c4cAc02bf2168299A4834438CB05340EF15bc', 9) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0x634c4cAc02bf2168299A4834438CB05340EF15bc', 10) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0x634c4cAc02bf2168299A4834438CB05340EF15bc', 11) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0x634c4cAc02bf2168299A4834438CB05340EF15bc', 12) 
'0x0'
```

```js
truffle(development)> web3.eth.getStorageAt('0x10CD7fD6d2c59EE5c2d42031a343F09CeBA12D06', 0) 
'0x33cc6c1c002311d26b561e1c8df3466f62bae1f9'
truffle(development)> web3.eth.getStorageAt('0x10CD7fD6d2c59EE5c2d42031a343F09CeBA12D06', 1) 
'0x0166cc3e64c7fa28364ee74c6e376686d87e02475c'
truffle(development)> web3.eth.getStorageAt('0x10CD7fD6d2c59EE5c2d42031a343F09CeBA12D06', 2) 
'0x03'
truffle(development)> web3.eth.getStorageAt('0x10CD7fD6d2c59EE5c2d42031a343F09CeBA12D06', 3) 
'0x33cc6c1c002311d26b561e1c8df3466f62bae1f9'
truffle(development)> web3.eth.getStorageAt('0x10CD7fD6d2c59EE5c2d42031a343F09CeBA12D06', 4) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0x10CD7fD6d2c59EE5c2d42031a343F09CeBA12D06', 5) 
'0xdf68e624d7bbcd3b101e324fbdd49af148700c66'
truffle(development)> web3.eth.getStorageAt('0x10CD7fD6d2c59EE5c2d42031a343F09CeBA12D06', 6) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0x10CD7fD6d2c59EE5c2d42031a343F09CeBA12D06', 7) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0x10CD7fD6d2c59EE5c2d42031a343F09CeBA12D06', 8) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0x10CD7fD6d2c59EE5c2d42031a343F09CeBA12D06', 9) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0x10CD7fD6d2c59EE5c2d42031a343F09CeBA12D06', 10) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0x10CD7fD6d2c59EE5c2d42031a343F09CeBA12D06', 11) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0x10CD7fD6d2c59EE5c2d42031a343F09CeBA12D06', 12) 
'0x0'
```

```js
truffle(development)> web3.eth.getStorageAt('0xc965d94aCf8C0Cf8D5aE1B985384999eba4CA0F7', 0) 
'0x33cc6c1c002311d26b561e1c8df3466f62bae1f9'
truffle(development)> web3.eth.getStorageAt('0xc965d94aCf8C0Cf8D5aE1B985384999eba4CA0F7', 1) 
'0x018a27b3748d2d996a1ed7737cb6ca29861caf44a1'
truffle(development)> web3.eth.getStorageAt('0xc965d94aCf8C0Cf8D5aE1B985384999eba4CA0F7', 2) 
'0x04'
truffle(development)> web3.eth.getStorageAt('0xc965d94aCf8C0Cf8D5aE1B985384999eba4CA0F7', 3) 
'0x33cc6c1c002311d26b561e1c8df3466f62bae1f9'
truffle(development)> web3.eth.getStorageAt('0xc965d94aCf8C0Cf8D5aE1B985384999eba4CA0F7', 4) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0xc965d94aCf8C0Cf8D5aE1B985384999eba4CA0F7', 5) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0xc965d94aCf8C0Cf8D5aE1B985384999eba4CA0F7', 6) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0xc965d94aCf8C0Cf8D5aE1B985384999eba4CA0F7', 7) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0xc965d94aCf8C0Cf8D5aE1B985384999eba4CA0F7', 8) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0xc965d94aCf8C0Cf8D5aE1B985384999eba4CA0F7', 9) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0xc965d94aCf8C0Cf8D5aE1B985384999eba4CA0F7', 10) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0xc965d94aCf8C0Cf8D5aE1B985384999eba4CA0F7', 11) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0xc965d94aCf8C0Cf8D5aE1B985384999eba4CA0F7', 12) 
'0x0'
```

```js
truffle(development)> web3.eth.getStorageAt('0xd46e66Abff6995a61dbD6A6aCCAD6037bFD33376', 0) 
'0x33cc6c1c002311d26b561e1c8df3466f62bae1f9'
truffle(development)> web3.eth.getStorageAt('0xd46e66Abff6995a61dbD6A6aCCAD6037bFD33376', 1) 
'0x01a4e9916b1fa346750a5fa97041fe475dcc6c476f'
truffle(development)> web3.eth.getStorageAt('0xd46e66Abff6995a61dbD6A6aCCAD6037bFD33376', 2) 
'0x02'
truffle(development)> web3.eth.getStorageAt('0xd46e66Abff6995a61dbD6A6aCCAD6037bFD33376', 3) 
'0x33cc6c1c002311d26b561e1c8df3466f62bae1f9'
truffle(development)> web3.eth.getStorageAt('0xd46e66Abff6995a61dbD6A6aCCAD6037bFD33376', 4) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0xd46e66Abff6995a61dbD6A6aCCAD6037bFD33376', 5) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0xd46e66Abff6995a61dbD6A6aCCAD6037bFD33376', 6) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0xd46e66Abff6995a61dbD6A6aCCAD6037bFD33376', 7) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0xd46e66Abff6995a61dbD6A6aCCAD6037bFD33376', 8) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0xd46e66Abff6995a61dbD6A6aCCAD6037bFD33376', 9) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0xd46e66Abff6995a61dbD6A6aCCAD6037bFD33376', 10) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0xd46e66Abff6995a61dbD6A6aCCAD6037bFD33376', 11) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0xd46e66Abff6995a61dbD6A6aCCAD6037bFD33376', 12) 
'0x0'
```

```js
truffle(development)> web3.eth.getStorageAt('0x973dcA232B4f794aA5bF2FbEd723359563C92E78', 0) 
'0x33cc6c1c002311d26b561e1c8df3466f62bae1f9'
truffle(development)> web3.eth.getStorageAt('0x973dcA232B4f794aA5bF2FbEd723359563C92E78', 1) 
'0x015421717658233442d50d56df4e5dc37819980d59'
truffle(development)> web3.eth.getStorageAt('0x973dcA232B4f794aA5bF2FbEd723359563C92E78', 2) 
'0x04'
truffle(development)> web3.eth.getStorageAt('0x973dcA232B4f794aA5bF2FbEd723359563C92E78', 3) 
'0x33cc6c1c002311d26b561e1c8df3466f62bae1f9'
truffle(development)> web3.eth.getStorageAt('0x973dcA232B4f794aA5bF2FbEd723359563C92E78', 4) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0x973dcA232B4f794aA5bF2FbEd723359563C92E78', 5) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0x973dcA232B4f794aA5bF2FbEd723359563C92E78', 6) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0x973dcA232B4f794aA5bF2FbEd723359563C92E78', 7) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0x973dcA232B4f794aA5bF2FbEd723359563C92E78', 8) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0x973dcA232B4f794aA5bF2FbEd723359563C92E78', 9) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0x973dcA232B4f794aA5bF2FbEd723359563C92E78', 10) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0x973dcA232B4f794aA5bF2FbEd723359563C92E78', 11)
'0x0'
truffle(development)> web3.eth.getStorageAt('0x973dcA232B4f794aA5bF2FbEd723359563C92E78', 12)
'0x0'
```

```js
truffle(development)> web3.eth.getStorageAt('0xa054560a1e76D547a1E75E2a31d3D9C98d3B75B6', 0) 
'0x33cc6c1c002311d26b561e1c8df3466f62bae1f9'
truffle(development)> web3.eth.getStorageAt('0xa054560a1e76D547a1E75E2a31d3D9C98d3B75B6', 1) 
'0x01eb3f2c390dd8f767f6bf1d8b85c4b0b7576e754e'
truffle(development)> web3.eth.getStorageAt('0xa054560a1e76D547a1E75E2a31d3D9C98d3B75B6', 2) 
'0x04'
truffle(development)> web3.eth.getStorageAt('0xa054560a1e76D547a1E75E2a31d3D9C98d3B75B6', 3) 
'0x33cc6c1c002311d26b561e1c8df3466f62bae1f9'
truffle(development)> web3.eth.getStorageAt('0xa054560a1e76D547a1E75E2a31d3D9C98d3B75B6', 4) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0xa054560a1e76D547a1E75E2a31d3D9C98d3B75B6', 5) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0xa054560a1e76D547a1E75E2a31d3D9C98d3B75B6', 6) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0xa054560a1e76D547a1E75E2a31d3D9C98d3B75B6', 7) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0xa054560a1e76D547a1E75E2a31d3D9C98d3B75B6', 8) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0xa054560a1e76D547a1E75E2a31d3D9C98d3B75B6', 9) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0xa054560a1e76D547a1E75E2a31d3D9C98d3B75B6', 10) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0xa054560a1e76D547a1E75E2a31d3D9C98d3B75B6', 11) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0xa054560a1e76D547a1E75E2a31d3D9C98d3B75B6', 12) 
'0x0'
```

```js
truffle(development)> web3.eth.getStorageAt('0x0a4992c7d794f2030a699d4b99418a98fb3b4b8c', 0) 
'0x33cc6c1c002311d26b561e1c8df3466f62bae1f9'
truffle(development)> web3.eth.getStorageAt('0x0a4992c7d794f2030a699d4b99418a98fb3b4b8c', 1) 
'0x01f86f48b08c75c9d553397ca89e6a5fc37072aace'
truffle(development)> web3.eth.getStorageAt('0x0a4992c7d794f2030a699d4b99418a98fb3b4b8c', 2) 
'0x04'
truffle(development)> web3.eth.getStorageAt('0x0a4992c7d794f2030a699d4b99418a98fb3b4b8c', 3) 
'0x33cc6c1c002311d26b561e1c8df3466f62bae1f9'
truffle(development)> web3.eth.getStorageAt('0x0a4992c7d794f2030a699d4b99418a98fb3b4b8c', 4) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0x0a4992c7d794f2030a699d4b99418a98fb3b4b8c', 5) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0x0a4992c7d794f2030a699d4b99418a98fb3b4b8c', 6) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0x0a4992c7d794f2030a699d4b99418a98fb3b4b8c', 7) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0x0a4992c7d794f2030a699d4b99418a98fb3b4b8c', 8) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0x0a4992c7d794f2030a699d4b99418a98fb3b4b8c', 9) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0x0a4992c7d794f2030a699d4b99418a98fb3b4b8c', 10) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0x0a4992c7d794f2030a699d4b99418a98fb3b4b8c', 11) 
'0x0'
truffle(development)> web3.eth.getStorageAt('0x0a4992c7d794f2030a699d4b99418a98fb3b4b8c', 12) 
'0x0'

```