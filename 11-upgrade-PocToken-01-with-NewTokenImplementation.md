# Upgrade Proxy PocToken-01 to use a new implementation contract

From step 10 we know new implementation contracts: 

- token: 0x309a5fe7A5C369342e51C37212a4633E4B02F4B3
- complianceServiceRegulated: 0xEc52A1cD5d945d7CF7a56A71FDA1a0f81B06479D

```bash
$ truffle(development)> const pocToken = await DSToken.at ('0x86Cd5d893C2aE7F8dFbdDC6972A204a505D2A63f')
$ truffle(development)> await pocToken.name.call()
'PoC-Token-A01'
$ truffle(development)> await pocToken.owner.call()
'0x33cC6c1c002311D26B561e1C8df3466F62bAe1f9'
truffle(development)> const pocProxy = await Proxy.at('0x86Cd5d893C2aE7F8dFbdDC6972A204a505D2A63f')
truffle(development)> await pocProxy.setTarget('0x309a5fe7A5C369342e51C37212a4633E4B02F4B3')
{
  tx: '0x9a8b2a0fba780d473bc6064fb2f259e77503d08dc3f228922cd0a243661eeb5d',
  receipt: {
    transactionHash: '0x9a8b2a0fba780d473bc6064fb2f259e77503d08dc3f228922cd0a243661eeb5d',
    transactionIndex: 0,
    blockHash: '0xc635e6805d5b29068253f7781d8c6d28d23a24269659e4c25396255ff93bf742',
    blockNumber: 5747,
    from: '0x33cc6c1c002311d26b561e1c8df3466f62bae1f9',
    to: '0x86cd5d893c2ae7f8dfbddc6972a204a505d2a63f',
    gasUsed: 29401,
    cumulativeGasUsed: 29401,
    contractAddress: null,
    logs: [ [Object] ],
    status: true,
    logsBloom: '0x00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000200000000000000000000000000000000000000000100000000000000000000000100020008000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000040000000000000000000000000',
    rawLogs: [ [Object] ]
  },
  logs: [
    {
      logIndex: 0,
      transactionIndex: 0,
      transactionHash: '0x9a8b2a0fba780d473bc6064fb2f259e77503d08dc3f228922cd0a243661eeb5d',
      blockHash: '0xc635e6805d5b29068253f7781d8c6d28d23a24269659e4c25396255ff93bf742',
      blockNumber: 5747,
      address: '0x86Cd5d893C2aE7F8dFbdDC6972A204a505D2A63f',
      type: 'mined',
      id: 'log_78c90593',
      event: 'ProxyTargetSet',
      args: [Result]
    }
  ]
}

truffle(development)> await pocProxy.target.call()
'0x309a5fe7A5C369342e51C37212a4633E4B02F4B3'

truffle(development)> await proxyComplianceServiceTokenPoc01.target.call()
'0x597c515ece0f68Ba1528Ee9aa4c38E3aD9C82D45'
truffle(development)> await proxyComplianceServiceTokenPoc01.setTarget('0xEc52A1cD5d945d7CF7a56A71FDA1a0f81B06479D')
{
  tx: '0xe58eabaefbade5507aa962f693d1afdb1624374256ef3f385fe1a6ac64cc5c84',
  receipt: {
    transactionHash: '0xe58eabaefbade5507aa962f693d1afdb1624374256ef3f385fe1a6ac64cc5c84',
    transactionIndex: 0,
    blockHash: '0xc4ee866a0cfe5b5861fe344417de07531156936ba2770e301b626079edf7aebc',
    blockNumber: 5748,
    from: '0x33cc6c1c002311d26b561e1c8df3466f62bae1f9',
    to: '0x17c68acda7453192ba57567bbef52e5d230293ff',
    gasUsed: 29401,
    cumulativeGasUsed: 29401,
    contractAddress: null,
    logs: [ [Object] ],
    status: true,
    logsBloom: '0x00000000000000000000000000000000020000000000000000000000000000000000000000000000001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000200000000020000000000000000000000000000000000000000000000000000000100000008000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000',
    rawLogs: [ [Object] ]
  },
  logs: [
    {
      logIndex: 0,
      transactionIndex: 0,
      transactionHash: '0xe58eabaefbade5507aa962f693d1afdb1624374256ef3f385fe1a6ac64cc5c84',
      blockHash: '0xc4ee866a0cfe5b5861fe344417de07531156936ba2770e301b626079edf7aebc',
      blockNumber: 5748,
      address: '0x17C68AcDA7453192bA57567bBef52e5d230293Ff',
      type: 'mined',
      id: 'log_4d60ebfb',
      event: 'ProxyTargetSet',
      args: [Result]
    }
  ]
}
truffle(development)> await proxyComplianceServiceTokenPoc01.target.call()
'0xEc52A1cD5d945d7CF7a56A71FDA1a0f81B06479D'

```

## Checks after upgrade

We expect:

- All proxies are alive.
- That all implementation contracts are alive.
- That the storage of the old implementations remains the same.
- That the token status is the same: minted tokens, investors, lockes, etc...
- That we can run all dsclient test without problems

## Are all proxies alive?

YES! The  are alive!

- PocToken01: 0x86Cd5d893C2aE7F8dFbdDC6972A204a505D2A63f (checked before)
- Proxy02: 0xBDeEDd640E63c32da1a81D52D65b0Def402F435F
- Proxy03: 0xd287A90417b877aa7Fa3523e74C043EC5a4299B4

```bash
truffle(development)> await pocProxy.target.call()
'0x309a5fe7A5C369342e51C37212a4633E4B02F4B3'
truffle(development)> const tokenProxy02 = await DSToken.at('0xBDeEDd640E63c32da1a81D52D65b0Def402F435F')
undefined
truffle(development)> await tokenProxy02.name.call()
'Proxy02'
truffle(development)> const proxyProxy02 = await Proxy.at('0xBDeEDd640E63c32da1a81D52D65b0Def402F435F')
undefined
truffle(development)> await proxyProxy02.target.call()
'0x5f14051c79fEC81f89b3790D8F2427ed5b25F817'
truffle(development)> const tokenProxy03 = await DSToken.at('0xd287A90417b877aa7Fa3523e74C043EC5a4299B4')
undefined
truffle(development)> await tokenProxy03.name.call()
'Proxy03'
truffle(development)> const proxyProxy03 = await Proxy.at('0xd287A90417b877aa7Fa3523e74C043EC5a4299B4')
undefined
truffle(development)> await proxyProxy03.target.call()
'0x5f14051c79fEC81f89b3790D8F2427ed5b25F817'
truffle(development)> const complianceServiceTokenPoc01 = await ComplianceServiceRegulated.at('0x17C68AcDA7453192bA57567bBef52e5d230293Ff')
truffle(development)> const count01 = await complianceServiceTokenPoc01.getUSAccreditedInvestorsCount()
undefined
truffle(development)> count01.toString()
'0'
```

## Did old implementation contract's storage change?

NO! It did not! This is very cool!

```bash
truffle(development)> web3.eth.getStorageAt('0x5f14051c79fEC81f89b3790D8F2427ed5b25F817', 0)
'0x0'
truffle(development)> web3.eth.getStorageAt('0x5f14051c79fEC81f89b3790D8F2427ed5b25F817', 1)
'0x0'
truffle(development)> web3.eth.getStorageAt('0x5f14051c79fEC81f89b3790D8F2427ed5b25F817', 2)
'0x01'
truffle(development)> web3.eth.getStorageAt('0x5f14051c79fEC81f89b3790D8F2427ed5b25F817', 3)
'0x0'
truffle(development)> web3.eth.getStorageAt('0x5f14051c79fEC81f89b3790D8F2427ed5b25F817', 4)
'0x0'
truffle(development)> web3.eth.getStorageAt('0x5f14051c79fEC81f89b3790D8F2427ed5b25F817', 5)
'0x0'
truffle(development)> web3.eth.getStorageAt('0x5f14051c79fEC81f89b3790D8F2427ed5b25F817', 6)
'0x0'
truffle(development)> web3.eth.getStorageAt('0x5f14051c79fEC81f89b3790D8F2427ed5b25F817', 7)
'0x0'
truffle(development)> web3.eth.getStorageAt('0x5f14051c79fEC81f89b3790D8F2427ed5b25F817', 8)
'0x0'
truffle(development)> web3.eth.getStorageAt('0x5f14051c79fEC81f89b3790D8F2427ed5b25F817', 9)
'0x0'
truffle(development)> web3.eth.getStorageAt('0x5f14051c79fEC81f89b3790D8F2427ed5b25F817', 10)
'0x0'
truffle(development)> web3.eth.getStorageAt('0x5f14051c79fEC81f89b3790D8F2427ed5b25F817', 11)
'0x0'
```

```bash
truffle(development)> web3.eth.getStorageAt('0x597c515ece0f68Ba1528Ee9aa4c38E3aD9C82D45', 0)
'0x0'
truffle(development)> web3.eth.getStorageAt('0x597c515ece0f68Ba1528Ee9aa4c38E3aD9C82D45', 1)
'0x0'
truffle(development)> web3.eth.getStorageAt('0x597c515ece0f68Ba1528Ee9aa4c38E3aD9C82D45', 2)
'0x01'
truffle(development)> web3.eth.getStorageAt('0x597c515ece0f68Ba1528Ee9aa4c38E3aD9C82D45', 3)
'0x0'
truffle(development)> web3.eth.getStorageAt('0x597c515ece0f68Ba1528Ee9aa4c38E3aD9C82D45', 4)
'0x0'
truffle(development)> web3.eth.getStorageAt('0x597c515ece0f68Ba1528Ee9aa4c38E3aD9C82D45', 5)
'0x0'
truffle(development)> web3.eth.getStorageAt('0x597c515ece0f68Ba1528Ee9aa4c38E3aD9C82D45', 6)
'0x0'
truffle(development)> web3.eth.getStorageAt('0x597c515ece0f68Ba1528Ee9aa4c38E3aD9C82D45', 7)
'0x0'
truffle(development)> web3.eth.getStorageAt('0x597c515ece0f68Ba1528Ee9aa4c38E3aD9C82D45', 8)
'0x0'
truffle(development)> web3.eth.getStorageAt('0x597c515ece0f68Ba1528Ee9aa4c38E3aD9C82D45', 9)
'0x0'
truffle(development)> web3.eth.getStorageAt('0x597c515ece0f68Ba1528Ee9aa4c38E3aD9C82D45', 10)
'0x0'
truffle(development)> web3.eth.getStorageAt('0x597c515ece0f68Ba1528Ee9aa4c38E3aD9C82D45', 11)
'0x0'
truffle(development)> web3.eth.getStorageAt('0x597c515ece0f68Ba1528Ee9aa4c38E3aD9C82D45', 12)
'0x0'

```

## That the token status is the same

All OK! No changes after upgrade

```bash
diego-securitize@heavy-machine:~/dsclient/master-TokenPoC-01$ dsclient token:info
{"tokenParams":{"name":"PoC-Token-A01","symbol":"PoCTokA01","decimals":0,"totalIssued":"3186","totalWallets":13,"isPaused":false}}
diego-securitize@heavy-machine:~/dsclient/master-TokenPoC-01$ dsclient token:totalSupply
1456
diego-securitize@heavy-machine:~/dsclient/master-TokenPoC-01$ dsclient compliance:counters
{
  euRetailInvestorsCounts: [
    { countryCode: 'AT', count: 0 },
    { countryCode: 'BE', count: 0 },
    { countryCode: 'BG', count: 0 },
    { countryCode: 'HR', count: 0 },
    { countryCode: 'CY', count: 0 },
    { countryCode: 'CZ', count: 0 },
    { countryCode: 'DK', count: 0 },
    { countryCode: 'EE', count: 0 },
    { countryCode: 'FI', count: 0 },
    { countryCode: 'FR', count: 0 },
    { countryCode: 'DE', count: 0 },
    { countryCode: 'GR', count: 0 },
    { countryCode: 'HU', count: 0 },
    { countryCode: 'IE', count: 0 },
    { countryCode: 'IT', count: 0 },
    { countryCode: 'LV', count: 0 },
    { countryCode: 'LT', count: 0 },
    { countryCode: 'LU', count: 0 },
    { countryCode: 'MT', count: 0 },
    { countryCode: 'NL', count: 0 },
    { countryCode: 'PL', count: 0 },
    { countryCode: 'PT', count: 0 },
    { countryCode: 'RO', count: 0 },
    { countryCode: 'SK', count: 0 },
    { countryCode: 'SI', count: 0 },
    { countryCode: 'ES', count: 0 },
    { countryCode: 'SE', count: 0 },
    { countryCode: 'GB', count: 0 },
    { countryCode: 'IS', count: 0 },
    { countryCode: 'LI', count: 0 },
    { countryCode: 'NO', count: 0 },
    { countryCode: 'CH', count: 0 }
  ],
  usInvestorsCount: 2,
  totalInvestorsCount: 12,
  accreditedInvestorsCount: 8,
  usAccreditedInvestorsCount: 0,
  jpInvestorsCount: 0
}
diego-securitize@heavy-machine:~/dsclient/master-TokenPoC-01$ dsclient compliance:getCountry US
us
diego-securitize@heavy-machine:~/dsclient/master-TokenPoC-01$ dsclient compliance:info
{
  totalInvestorsLimit: 5000,
  minUSTokens: '0',
  minEUTokens: '0',
  usInvestorsLimit: 5000,
  euRetailInvestorsLimit: 150,
  jpInvestorsLimit: 0,
  usAccreditedInvestorsLimit: 5000,
  nonAccreditedInvestorsLimit: 5000,
  maxUSInvestorsPercentage: 100,
  blockFlowbackEndTime: 4000000000,
  nonUSLockPeriod: 0,
  minimumTotalInvestors: 0,
  minimumHoldingsPerInvestor: '0',
  maximumHoldingsPerInvestor: '5000',
  usLockPeriod: 31536000,
  forceFullTransfer: false,
  forceAccredited: false,
  forceAccreditedUS: true,
  worldWideForceFullTransfer: false
}
diego-securitize@heavy-machine:~/dsclient/master-TokenPoC-01$ dsclient token:info
{"tokenParams":{"name":"PoC-Token-A01","symbol":"PoCTokA01","decimals":0,"totalIssued":"3186","totalWallets":13,"isPaused":false}}
diego-securitize@heavy-machine:~/dsclient/master-TokenPoC-01$ dsclient token:totalSupply       
1456
diego-securitize@heavy-machine:~/dsclient/master-TokenPoC-01$ dsclient compliance:counters     
{
  euRetailInvestorsCounts: [
    { countryCode: 'AT', count: 0 },
    { countryCode: 'BE', count: 0 },
    { countryCode: 'BG', count: 0 },
    { countryCode: 'HR', count: 0 },
    { countryCode: 'CY', count: 0 },
    { countryCode: 'CZ', count: 0 },
    { countryCode: 'DK', count: 0 },
    { countryCode: 'EE', count: 0 },
    { countryCode: 'FI', count: 0 },
    { countryCode: 'FR', count: 0 },
    { countryCode: 'DE', count: 0 },
    { countryCode: 'GR', count: 0 },
    { countryCode: 'HU', count: 0 },
    { countryCode: 'IE', count: 0 },
    { countryCode: 'IT', count: 0 },
    { countryCode: 'LV', count: 0 },
    { countryCode: 'LT', count: 0 },
    { countryCode: 'LU', count: 0 },
    { countryCode: 'MT', count: 0 },
    { countryCode: 'NL', count: 0 },
    { countryCode: 'PL', count: 0 },
    { countryCode: 'PT', count: 0 },
    { countryCode: 'RO', count: 0 },
    { countryCode: 'SK', count: 0 },
    { countryCode: 'SI', count: 0 },
    { countryCode: 'ES', count: 0 },
    { countryCode: 'SE', count: 0 },
    { countryCode: 'GB', count: 0 },
    { countryCode: 'IS', count: 0 },
    { countryCode: 'LI', count: 0 },
    { countryCode: 'NO', count: 0 },
    { countryCode: 'CH', count: 0 }
  ],
  usInvestorsCount: 2,
  totalInvestorsCount: 12,
  accreditedInvestorsCount: 8,
  usAccreditedInvestorsCount: 0,
  jpInvestorsCount: 0
}
diego-securitize@heavy-machine:~/dsclient/master-TokenPoC-01$ dsclient compliance:info
{
  totalInvestorsLimit: 5000,
  minUSTokens: '0',
  minEUTokens: '0',
  usInvestorsLimit: 5000,
  euRetailInvestorsLimit: 150,
  jpInvestorsLimit: 0,
  usAccreditedInvestorsLimit: 5000,
  nonAccreditedInvestorsLimit: 5000,
  maxUSInvestorsPercentage: 100,
  blockFlowbackEndTime: 4000000000,
  nonUSLockPeriod: 0,
  minimumTotalInvestors: 0,
  minimumHoldingsPerInvestor: '0',
  maximumHoldingsPerInvestor: '5000',
  usLockPeriod: 31536000,
  forceFullTransfer: false,
  forceAccredited: false,
  forceAccreditedUS: true,
  worldWideForceFullTransfer: false
}

diego-securitize@heavy-machine:~/dsclient/master-TokenPoC-01$ dsclient holder:info investor01
{
  id: 'investor01',
  country: 'ES',
  attributes: [
    { name: 'kyc', status: 'approved' },
    { name: 'accredited', status: 'approved' }
  ]
}
diego-securitize@heavy-machine:~/dsclient/master-TokenPoC-01$ dsclient holder:info investor02
{
  id: 'investor02',
  country: 'ES',
  attributes: [
    { name: 'kyc', status: 'approved' },
    { name: 'accredited', status: 'approved' }
  ]
}
diego-securitize@heavy-machine:~/dsclient/master-TokenPoC-01$ dsclient holder:info investor03
{
  id: 'investor03',
  country: 'ES',
  attributes: [
    { name: 'kyc', status: 'approved' },
    { name: 'accredited', status: 'approved' }
  ]
}

diego-securitize@heavy-machine:~/dsclient/master-TokenPoC-01$ dsclient lock:list investor01
[
  { value: '10', reason: 'evasor', reasonCode: 0 },
  { value: '20', reason: 'evasor2', reasonCode: 0 }
]
diego-securitize@heavy-machine:~/dsclient/master-TokenPoC-01$ dsclient lock:list investor02
[ { value: '50', reason: 'test-sucutrule', reasonCode: 0 } ]
diego-securitize@heavy-machine:~/dsclient/master-TokenPoC-01$ dsclient lock:list investor03
[]
```

## Runnig test suit

All test OK!

```bash
diego-securitize@heavy-machine:~/dsclient/master-TokenPoC-01$ dsclient test:all


  HOLDER related operations tests
0x4d080cbbbd73faad1eb28692042ab12206b09a89bc20626f81d87060fd4258c4
    ✓ should create a new holder id (2858ms)
0x3fd8a0e29d45ff202f0bf32f52e264a0bf2b44d176a9b7d3ed184c315dd87659
0x77db35760498607b02511f079ce6a9dabf04371d07c9b1f2d2e2e14af76ab992
    ✓ should be able to update the holder country (5366ms)
0x5cb0c43857c5aeecaf39703773d1e7e72480385276c598957942e0c9e8a70682
0x53a6330f2a3f842ce8a0b55a0e085aee0a313f8b3d173c614caba30457a482ed
    ✓ should be able to delete a holder (5023ms)
0xcf27e798bd65acfaeaf46608b30b29cd016de63d8e80106059a93af731b5885f
0x55df3d5d8be95a7858eae3f483685c6f06d0d01f9d6b3ddeeffed3b2fc56d69b
    ✓ should not allow to delete a holder with a wallet assigned to it (5354ms)
0x8aca2c2b461543a5147c4a7a274ec0c98107c3f226a4cec432f5838af5175c3e
    ✓ should assign a wallet to existing tokens holder (2757ms)
0x3e5a3009407ed21c5b506768a0c0c863ea91376027dc331867fe2abe0175dbda
0x4734c576c71d5360e78df15749b96af881b7d2fc22e6182227a730bc48e3e0f4
0x56cb13da3c464625cbf387028504175e3d4b8737e6a4b1a3820781faa4842a74
    ✓ should be able to add multiple wallets to a holder (7957ms)
0x3d0a8be23c6f00505945652c6d8036fd55c95b7a5309c36fc33635624155a494
0xbb494ff2021dd3aa5117713cf7d8ed588b9f58b503a973b7e7a62a366ddf1d53
0x25941e26355a100183680d587465afe204d6766d29d5c70471645aeb85e99787
    ✓ should be able to remove a wallet form the holder (7444ms)
0x4d2c6cec4e65b2b5644afca5175a3345118b226570f669a387cac8370c4b7b9e
0x6e45b5ad31d5a269d504d4595b97410d3846e16e7e906f994dedf917452a2732
    ✓ should be able to add a valid attribute to an existing holder (4740ms)
0xc4f07c695db4dd051a4ddb757200b61e1be735705221784da8e797cceab2dd93
0xa4f69c1bcef24820f8667b09e40ed0390695b1c85611772da992a65ac7781438
    ✓ Should be able to retrieve a specific attribute from the holder (4752ms)
0x36ff279e4e009ab3c7f2fa02f957477360aedb228d440a0a89b9a1ddb415f964
0xb33eccfab2ad8b0982740fcc2e19d8d5a0daf16f0947666379c9003e4da3e811
0x13c94f3afc4fc1c40a48bf601e143ee459de6a569ce2703c982083ae5464069f
    ✓ should be able to remove an attribute from the holder (7219ms)
0x9404e66ab49693337df6c3e3e1c9ba6c4c9d535dffdb150faebecd92815a95bf
0xb80d75370d52dd913091e17ec92ac0f26a271317db0c30d0e70cbb0fc0e10c0c
0xc94551385380f58a776499017f2de12bda6917ddfbfede9f33c958da60094e52
    ✓ should NOT allow to remove a wallet that is still holding tokens (8379ms)

  TOKEN related operations tests
0xb45564f8bdcd8578d891215db80ac5044721d22d5c6fe74cf4bcb4d8c37d74ef
    ✓ should retrieve the information for the test Token (96ms)
0x2c728f2f74deca580d8d3db356eca52d468db3a12996c0dff27894e6d70eaf41
0x9e014ccc309e63b280058fededcc3cda7599ed21021f0d40b9532f293113e991
0x22e323dfce9ce1157ad7968baa81cd5fa2a5e499e1255a010937c631e5a42413
    ✓ should be able to mint tokens to Holder 1 (8241ms)
0xe5487d4e7170f0a0656c821d801628794b1ae6e85ef905625e733fad15f75c0e
    ✓ should be able to create a Holder and include a mint operation in the same transaction (3708ms)
0x55dd7b8226df829e74b62d842eca049253bb974a7805dcdee2735ea845203c01
0x6bd70eae1b52f942bf67829f35da9cb8997fccbe65b4c67f119d448f04893c84
0x05429cb9b2589a9e6d727f608314d3552b683c15ba0eddd4cce07dd023de39b4
0x18c8adac3efcbdfc084ce5292d8d9130dd5ecff6fd529102b9eb63ae6f35586b
    ✓ should be able to transfer tokens between Holder 1 and Holder 2 (11692ms)
0x2ddbfb1635f15cc2403efaeff81e41055f775ffdb4d376ede20287e81a8b2ae1
0xcbc8c5b443c25b3afe25fa6cc237313a0b97e6088aca8a6a589fc96ab9d85c59
    ✓ should not allow to transfer more tokens than the current balance (5036ms)
    ✓ should not transfer tokens to an address not associated with a holder (57ms)
Current supply of tokens: 
1981
0x552e9fb7d1cc82b7c6fb5a7a385f879acf6ce0a973474194944f40651a3164e6
0x7283b8309d47e01f21d999a85e372ac24912bc84436e0157c12689f66da5f583
0xaba8f4739c5ec8ef062cad869b2d65351480fbe2db079af519260c49214acc75
Total supply after minting 150: 
2131
    ✓ should get the correct value for total Supply (8244ms)
0x4d40e519b6475cb06acf68767b7424358d6c1af1f9c877959367c96f01650abf
0x32395458ee3177a27be49b498d034f2f99063a90aba34925d63637bcfb06fa03
0xcd7b37b474c2f3909cfcd5a9745102509334464562b5cf2fa2a3706a69a6d495
0xfec9effd6c1d8771a5a63228d72bcdafb959103990702b3677afde90ad1fe2fb
    ✓ should allow to burn tokens to the owner (10631ms)
0x8a2bcc9615501deea466b39ee944b3eaf3f1791af0b5f0c9b3dbfb9563854351
    ✓ should emit an event when a holder is created (3249ms)
0xf17d4e720e6a18a354a433cc1da2f85515964257ba81974bc0fd77bc5bf0ada7
0x2ad87319340c5454166216224736c356f4e96a6e6c7fd25fbc5a6cf40fc31931
    ✓ should emit an event when a wallet is added to a holder (5510ms)
0x2282151c1f706ac5f0b28ff55aee92cf6332f47cd495d77a51b345be9725d0b9
0xc7dcd3fda4d893677aefb0580c4c604e65c92f6057b903a272130f681919207b
    ✓ should emit an event when the country is changed for an investor (5721ms)
0xdd5e63fb31db243aef8144c463fdd338ad61f34f2ebc5e101931216a5e6cfb2e
0x4cd1b2a46f8dab8e8d67252e62b2de82bc199b7110d315abc4b8ccc064f1a2dd
0xde01130e4d384cd1ac35219ce23f8fcb15440c68e9272e2b47a6f99b8c08d3e4
0xa2ef1d6be8c64265a429bdb287ed1cf9ca598c369b65ba45bb1091cc0ca41225
0xf5f5d1418c1cd9f2ca87fabd4bdb383837218c0a5ae2c58acb2264d24239531e
    ✓ should return the correct amount for the balance of a holder including all their wallets (14288ms)
    ✓ should be able to filter events (35484ms)
0x6d5786a9d2c83207339b3e1415c4600d37595a16c5b34470323186ea76503a82
0x16a5423762317534a66c7a85f3413bd2eca74ec9582e1b7cfb461625a7b547d6
0x642e2b21e069465ce5634b56cb3077175ea8be65dfc89e5193f63aa26845e7db
0x55e0cc4da4119c09a6d454ef8559e9be2e82f66b3c161f82b10ad8bd64900973
0xc017544a73534ada05b80a7af50e35fcab033dbf6e540b23ee5128f952be7834
    ✓ should be able to pause the token (13185ms)
0x44585013d82d82e900768b6a9d59e1eb073682243ca5ebe4b929e9f7cc9538b0
    ✓ should be able to approve a transfer from a spender (2134ms)
    - should be able to transferFrom a holder to a spender with the correct allowance
    - should be able to seize tokens from a holder

  ROLE management related tests
    ✓ should be able to get the role of a holder
0x8b4dbee98305e5991e70ced6464ca92e669e18038f5bfe4a2f843fa98852eee3
0x8053d2987f2324cc41dc0982f5b085e9d9242680396552ddf4c72890abc98eca
0xefc5b8f9d3849e8be9be89755e4804638b3631870ce891759ff5fada49380da1
    ✓ should be able to set the role of a holder (7336ms)
    - should not allow an ISSUER to assign roles in the network

  COMPLIANCE related functionality tests
0xbd4317b250d167ce030a0e6acb8358ce0e4692796b0fa0c75a17bef84db69f71
0xdbafe00e7da5024c4cf91ab4970641d794e52b10e43295f00e9e74e51c43acf1
0xa6bfd8681dc0ab3e5d2a20c849604f5d85a5f0f9b6bf959b594e2542700645a8
0x11b219e8f36888583bd377c38d6bca9a621585404c8a7b42e6ba9ea0568478c1
0x6114ba6ada3eaaf2ce213aac1c1d1b4716ce3f009ad842a964470de20af72a1a
0x5a4aecc40f60fba2053c553d0d947cb048eec525594b9a1e67e485070a3c1e81
    ✓ should get the compliance information for the active token (70ms)
    ✓ Should return compliance counters (591ms)
0x40c377fcfa8ef3f5e32b6c4a4ad1da30d4626f5bf598d46b36b1dc7a3f13d75d
    ✓ should be able to set the compliance status for US (2747ms)
0x29f820a699d071a2766b76f69fec6b672fa47c20467120346c27d9e29c9cb57a
    ✓ should be able to set compliance rules as the OWNER (4632ms)
0xcf7f89cc94d3568d84c7b24073e42766628590b6df8ceb2fee7afabdf08b27d1
    ✓ should apply totalInvestorsLimit rule when MINT is attempted (4421ms)
0x0f9014f06976bfc3c60be9c8ee185642fa959a7cd2392d214e4de7b615a2ef7c
0x7cbc31cc0b67e3462bc89f7cf6063d7a673439919afc8815e9eafc55c30cd44d
0xf669ae05b65f586eb4953f059e6e2f465a6c0c3ae490f5dc7b72a92031b2af81
0xa8ba7e5ea400e5e25a140bb74112dbbbf73f418e1665231560716be81762d2f8
0x50572ddcbb2a0c711acff343a0a93099f88226b7189dd6f63d9fb9bd8a728a4b
    ✓ should apply totalInvestorsLimit compliance rule when TRANSFER is attempted (17223ms)
    - should allow a full transfer between holders when the number of holders is at its maximum
0xec7417ceb5785aa5a5ea2f3ba3c9811536e38dc3bdf63304884ae591d3e146f2
0xd7f1f3bbd6851f45f95f616d058bdc19d538cf8cc75b239af526c791a73c3cfd
0x3f12e601b802fa050186e1d9a77b423249059126c8fa12b72104023276b5a458
    ✓ should enforce the maximum holdings per investor limit rule (12213ms)
0x601207f661640f0220beb19d9b6064b8a5917743b7b494d803a97332dea98ef2
0xb88fc78d175c0cd84b217fbfe983dab74f2df89ae485bb23195f6e6435679c03
0xa642851868080634ae5c4ce8bdf94bc945917a77be18972a5b1e7733f95e4d1e
    ✓ should not allow to mint a token amount above maxHoldingsPerInvestor (9502ms)
0x2e1bd5cffb1960b124e26548c8162970b132dbbdca396ab45cd3de90dfb76a4b
0x8a3d72cf0c2087761901c8916b278d9fc7fbac08ccb87684a3baaa8919f11c73
0xdb3bc1bf1c0a57624d0d891165eb2119b248ba9c183a09bc14fb2ddf0313aa5d
0xf335b41b050ab7224d8a42629b305924a4fa9640916971e6d7bfe02ed215ba37
0x4e61159037f6eeecf6c174ad43a958b557354e40fca79e71a269f876af88c278
0x93e90103643ba37673f045392891919974c90e9397271abb87d31fb1676b437d
0x489b886c077100be5a6929c86854276f78ad92b99313ae8250f36af4703bea35
0x74fbbc4d7afd4381ae481d54ed3778460f33c60dc90052af11ba28243b71b1d9
0xbfd07999367486cf75fdb1750e3044147af6c65f89cc2ea0c03c89d6b97898fa
0xcf9b4a4f7f8a02885c3a4a1c8dc47f17d3f9faa1cd400011da15a00e269825ec
0xb6d5cf171f70cf71a785f5b0859ae66b01cfcab0a1512e24acda207148b0003f
    ✓ should not allow to transfer locked tokens for US investors if issuance time was less than 365 days ago (31791ms)
0x2e3140eb59a074c0102a94bebe5d2ad57bf5c4921948c1d02349a72288e1da31
0x1f9ec186fc1e253495b25471dbdad256398206f4e8cea0573ee273d81ccf03c1
0x8b9baeee4baa8393a2ca6dc14dd5729fcd0173e6742f3a03e75e573680a22765
    ✓ should apply the forceFullTransfer rule for US investors if set (10510ms)
0x0b75a63f0fac35b8fd00ecfc7027a8b14f23de64cb9a84c004722a4661fb576a
0x4fa6a04bc962d511280079ab532d5252345042ec2dfa192ef2cdeb7a6e794374
0xd2ce53c9c329882be3044128a4025cda031770d453757b6b2e2741d344c88f4f
0x043b033d4afd3ab5444d650d3f497f3e42777485cf84af469e58bfbd9f051ac3
0xefa7254b179146c1d2263eff8a9dd704d51697a7c123c05c189ad633e67a5073
0x0bc4906fc32e4763c776ab4120d6b584072ce9c1d1dea3ed95c97d26acec24ff
    ✓ should not allow to transfer tokens locked during MINT (17937ms)
0x861ed0b4297c392033857a0420759efb83e42adc906dd5e506c29406502f983e
0x0d63d756410621fbbca23e22dee360b21010d26f06c1eb3e66a7ec2e5739f5e7
0x5f5be253f153c781818b233a11e89f9065ac4e69a5d187ede1841c24f6843248
0xedecf775ff6760bed98caf94460c670d293a97ca4ca5ca22608243a1166c6ecb
0xbf8a2890f1e0bc681c678903c32bd99ae369ff78835848d6708447c6c60b4fdf
0x6dcdc25124b5909fc19b14c4e2a37cac12f6c1a1c484855184368e13c1f9f264
0x4a9ef27c8eb2c34d0db8e40eb6af47d9c7a605e06280a38bb366b6f8800ffdaf
0x36ee02321285492cc5a320c5661ff7feb47247c25ba5db5d52b4a51613d4a802
0x54f7a21c0363ec69b22bb64defa3ad33ee79e5bfa5e5c7b10c4683cb7a315529
    ✓ should not allow to transfer locked tokens (26337ms)
0x7181bc72bc6cfa0f2a5642f87c5066f8c365450ce2add10f853ca7ac35722dfb
0xb6745ee7980df490bde8290b151d36ab4795c9cd6c7f69fad00d88cabb60e006
0x47c44ec57bd922d8bbac04fe458898b5431dca091ec596753c75efa46d2d7bb6
0xef30a13600a736a9eb27ecdcca6e45d15a5c17a06719d8575327c30909bd6aaa
0x4f9889a0efd62f48e2624f0441342b2a39f78f8d17c4a41003b2389ffd3e2abb
0x7d7ab61704011d534291dd7e5f592b00aa4ac00487578cb19acf0e8908c80bdc
    ✓ should not allow flowback from a non-US investor (17815ms)
0x23aa78f6890b514c42876a28a27421e918986088105e9d2d5912d928ed893ce9
0x0e04fd0b28455e724bef9cc9c85fbb6035bfb2929e73f04f932d032c7c993fe9
0xc6c9d67da78f29cf12a3fca8a6ba998d21b57b90caf1e48bd2f4060d94d23aa7
0x582cd768218f709faf29fd949fbc08e91a8d7d116c42579683c4fb43da5ec377
0xafc808b7584bf5fe732ab3dfa7cc32a43c49c0cdcf487770b22d7adb32198d08
0x15cf472c8d150b8002c6e922cced19d5bf871a2e154ab0301181ca5b877a7a6c
0xaeec5a2f53a76f8bcde00fdb6896f1165be5170b1ef27848a9067a37cede6f9a
0x4c9e86cf7cb6a921d01fdc8a11cf15843411ae1beb0197d9695374f53c03c62f
    ✓ should not allow to transfer to a non US accredited account when forceAccreditedUS rule is set (23053ms)

  TRANSACTION handling related tests
    - should speed up a pending transaction by increasing the gas price
    - should cancel a pending transaction
    - should get the next nonce for the identity address
    - should fail with a nonce related error when specifying an invalid nonce
    - should be able to specify a valid nonce and create the transaction
    - should be able to use nonce parameter to generate several transactions before sending them


  42 passing (7m)
  10 pending
```

And the status after unit test running

```bash
diego-securitize@heavy-machine:~/dsclient/master-TokenPoC-01$ dsclient token:info
{"tokenParams":{"name":"PoC-Token-A01","symbol":"PoCTokA01","decimals":0,"totalIssued":"5812","totalWallets":22,"isPaused":false}}
```

Other contracts are different universes: Different proxy and implementation contracts