# Deploying new proxies

We created a new branch of dstoken to deploy deploy only proxies and set targets.

## Deploying without Omnibus TBE

This was deployed with the same master than token 01. 

```bash
truffle migrate --name Proxy02 --symbol Proxy02 --decimals 0 --private_key 740d5cfa467d5a457a18a08cb3e7a76c6b28c138b3a65304d29ae3d7c991a88b --network development --owners "0xe2099bCfCA27f8cae93FD3C8726e4a8250FcA015 0x0a065Eaf3b6e037F8213fa13e76A025d2846Bd48" --required_confirmations 2 --no_omnibus_wallet
```

```bash
Token "Proxy02" (Proxy02) [decimals: 0] deployment complete
-------------------------
Token is at address: 0xBDeEDd640E63c32da1a81D52D65b0Def402F435F | Version: 3,5,6,5,5
Trust service is at address: 0x00E9D423CA939A6cdE5B4606c1e2Ebea64b47Eaa | Version: 3,3
Investor registry is at address: 0x7e4fB8f580d3c1E6cc73dC30b2183F5F2589832B | Version: 4,4,5,4
Compliance service is at address: 0xaCbc245d534F7E4aE507BCf689D722daEaC2A197, and is of type NORMAL | Version: 5,5,6,5,3,10
Compliance configuration service is at address: 0x792367573c6f6230fB28ad02707b62D52bb6Acb2 | Version: 5,5,6,6
Wallet manager is at address: 0xc338F6d52a086da53dc52E33663241005A1078Cb | Version: 3,4,5,3
Lock manager is at address: 0x9Ce034A64bE9FF71ADC872572117A9A64b1fDB88, and is of type INVESTOR. | Version: 3,4,5,3
Token issuer is at address: 0xa2c5318972DDB53a7E128072373fa46b82B355e4 | Version: 3,4,5,3
Wallet registrar is at address: 0xc8F4382D0fB1daceB876243ee24799719100B67a | Version: 3,4,5,3

```

## Deploying with Omnibus TBE Wallet

This was deployed with a different token

```bash
truffle migrate --name Proxy03 --symbol Proxy03 --decimals 0 --private_key d595193a6417d6cea5aa28b19a11d15b29344ed9f4a62d17b96d508a266ba634 --network developmentMaster2 --owners "0x0a065Eaf3b6e037F8213fa13e76A025d2846Bd48 0xe2099bCfCA27f8cae93FD3C8726e4a8250FcA015" --required_confirmations 2 --omnibus_wallet 0x275B0d8f14a77DceDB4A2b7cD4532a39C9AAb2A4
```

```bash
Token "Proxy03" (Proxy03) [decimals: 0] deployment complete
-------------------------
Token is at address: 0xd287A90417b877aa7Fa3523e74C043EC5a4299B4 | Version: 3,5,6,5,5
Trust service is at address: 0xE14a1ce6dE6e160337D8352Ac2b10e0f3C452111 | Version: 3,3
Investor registry is at address: 0xb351a80BB118Cddcc18787C19B37C92050e0DDd3 | Version: 4,4,5,4
Omnibus TBE controller is at address: 0xAECD202829c8c4cfb7085059C1c8d49Cc33CA617 | Version: 3,5,6
Compliance service is at address: 0x74282c8B078d99d93e179E8216A0748Ec2e3E0FE, and is of type NORMAL | Version: 5,5,6,5,3,10
Compliance configuration service is at address: 0x09AA707448EfB78AA707e156BC86A6908702B263 | Version: 5,5,6,6
Wallet manager is at address: 0x883C12B0A149c57d2B27D3CB2060592015DEFE0A | Version: 3,4,5,3
Lock manager is at address: 0x5F2d0f835704d00EaC4573b14568226b1f5Ade28, and is of type INVESTOR. | Version: 3,4,5,3
Token issuer is at address: 0xa18eD0522416F5d6B65D12aF4c346E91FBFE7784 | Version: 3,4,5,3
Wallet registrar is at address: 0xEf6d86F6960be9DE4B369cD26200E99C85876B1f | Version: 3,4,5,3

```