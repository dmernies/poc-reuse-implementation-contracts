# Deploying

We will deploy 3 proxis pointing to the same implementation contract and then we will check ERC-20 integration with etherscan.
We used 80 Gwei to deploy the contracts.


```bash
truffle migrate --name BlockchainTeam --symbol BLKT --decimals 0 --network rinkeby --owners "0xd51128F302755666c42e3920d72FF2FE632856a9 0x3D3EF8C37c53c34b99C71C1A0518A563F5e8512e" --required_confirmations 2 --no_omnibus_wallet
```

```
Starting migrations...
======================
> Network name:    'rinkeby'
> Network id:      4
> Block gas limit: 10000000 (0x989680)


1_initial_migration.js
======================
{
  _: [ 'migrate' ],
  name: 'BlockchainTeam',
  symbol: 'BLKT',
  decimals: 0,
  network: 'rinkeby',
  owners: '0xd51128F302755666c42e3920d72FF2FE632856a9 0x3D3EF8C37c53c34b99C71C1A0518A563F5e8512e',
  required_confirmations: 2,
  no_omnibus_wallet: true
}

   Deploying 'Migrations'
   ----------------------
   > transaction hash:    0x345136fc154bd47d7fc4935ccbf070d1abdf3b9ed6d9f2bbec5223eae7de23d7
   > Blocks: 1            Seconds: 17
   > contract address:    0xead94788BF0C5EE98aC6FeC634BDb4A78BBEc2c6
   > block number:        7981836
   > block timestamp:     1611955761
   > account:             0xd51128F302755666c42e3920d72FF2FE632856a9
   > balance:             22.7504049788
   > gas used:            274286 (0x42f6e)
   > gas price:           80 gwei
   > value sent:          0 ETH
   > total cost:          0.02194288 ETH


   > Saving migration to chain.
   > Saving artifacts
   -------------------------------------
   > Total cost:          0.02194288 ETH


2_deploy_trust_manager.js
=========================

   Deploying 'Proxy'
   -----------------
   > transaction hash:    0x6b776c10d6b332cb7ad5ec92630479233a15f3b1c9ab54374aef1394ddc35297
   > Blocks: 0            Seconds: 5
   > contract address:    0xc4c50c4D11aa63Da45B9D047b0E787A04cA27E79
   > block number:        7981838
   > block timestamp:     1611955791
   > account:             0xd51128F302755666c42e3920d72FF2FE632856a9
   > balance:             22.7280379388
   > gas used:            237287 (0x39ee7)
   > gas price:           80 gwei
   > value sent:          0 ETH
   > total cost:          0.01898296 ETH


   > Saving migration to chain.
   > Saving artifacts
   -------------------------------------
   > Total cost:          0.01898296 ETH


3_deploy_registry.js
====================

   Replacing 'Proxy'
   -----------------
   > transaction hash:    0x97b5cdae2678ab3a490708d9093121765ff34ab4889af0d82f22d80699f7bb50
   > Blocks: 0            Seconds: 5
   > contract address:    0xeb931842c3b19Abc29326fec5E03eD4f65161144
   > block number:        7981842
   > block timestamp:     1611955852
   > account:             0xd51128F302755666c42e3920d72FF2FE632856a9
   > balance:             22.6925351388
   > gas used:            237287 (0x39ee7)
   > gas price:           80 gwei
   > value sent:          0 ETH
   > total cost:          0.01898296 ETH


   > Saving migration to chain.
   > Saving artifacts
   -------------------------------------
   > Total cost:          0.01898296 ETH


4_deploy_compliance_manager.js
==============================

   Replacing 'Proxy'
   -----------------
   > transaction hash:    0x2d8e71b9898ad9574547463af1302e1bbf10beedaf8fd8ee7824529a653d09d4
   > Blocks: 0            Seconds: 6
   > contract address:    0x4b316716dC8FA76fb516e7C1d1679A396587E4aE
   > block number:        7981846
   > block timestamp:     1611955912
   > account:             0xd51128F302755666c42e3920d72FF2FE632856a9
   > balance:             22.6551074588
   > gas used:            237287 (0x39ee7)
   > gas price:           80 gwei
   > value sent:          0 ETH
   > total cost:          0.01898296 ETH


   > Saving migration to chain.
   > Saving artifacts
   -------------------------------------
   > Total cost:          0.01898296 ETH


5_deploy_complianceConfiguration.js
===================================

   Replacing 'Proxy'
   -----------------
   > transaction hash:    0x70294d1a9fa2d3f0ab29e7c552b831868ee20c563fe80fa9cc6f3fea46b9396f
   > Blocks: 1            Seconds: 9
   > contract address:    0x13b91c5E824BBC30e1a8Fa06d8528E39C3D8d825
   > block number:        7981850
   > block timestamp:     1611955972
   > account:             0xd51128F302755666c42e3920d72FF2FE632856a9
   > balance:             22.6138680188
   > gas used:            237287 (0x39ee7)
   > gas price:           80 gwei
   > value sent:          0 ETH
   > total cost:          0.01898296 ETH


   > Saving migration to chain.
   > Saving artifacts
   -------------------------------------
   > Total cost:          0.01898296 ETH


6_deploy_wallet_manager.js
==========================

   Replacing 'Proxy'
   -----------------
   > transaction hash:    0x79a6f940c7682b30e0d7fd303821828d98035b9468bb58bd845800fb6b1c7c5c
   > Blocks: 1            Seconds: 9
   > contract address:    0x47D7da10e46F3df4E6A6Cb9fC03DF36dbfD99917
   > block number:        7981854
   > block timestamp:     1611956032
   > account:             0xd51128F302755666c42e3920d72FF2FE632856a9
   > balance:             22.5764368988
   > gas used:            237287 (0x39ee7)
   > gas price:           80 gwei
   > value sent:          0 ETH
   > total cost:          0.01898296 ETH


   > Saving migration to chain.
   > Saving artifacts
   -------------------------------------
   > Total cost:          0.01898296 ETH


7_deploy_lock_manager.js
========================

   Replacing 'Proxy'
   -----------------
   > transaction hash:    0xf2609b7cbb61df9e37b15dd1a28f657eb57c0252b81e9351a62e71d9ed736600
   > Blocks: 0            Seconds: 13
   > contract address:    0x01DA7638f525bBC1D3efc895e81dF6FD25085a40
   > block number:        7981859
   > block timestamp:     1611956107
   > account:             0xd51128F302755666c42e3920d72FF2FE632856a9
   > balance:             22.5389952188
   > gas used:            237287 (0x39ee7)
   > gas price:           80 gwei
   > value sent:          0 ETH
   > total cost:          0.01898296 ETH


   > Saving migration to chain.
   > Saving artifacts
   -------------------------------------
   > Total cost:          0.01898296 ETH


8_deploy_partitions_manager.js
==============================

   > Saving migration to chain.
   -------------------------------------
   > Total cost:                   0 ETH


9_deploy_token.js
=================
ConfigurationManager {
  proxiesAddresses: {
    TrustService: '0xc4c50c4D11aa63Da45B9D047b0E787A04cA27E79',
    RegistryService: '0xeb931842c3b19Abc29326fec5E03eD4f65161144',
    ComplianceServiceRegulated: '0x4b316716dC8FA76fb516e7C1d1679A396587E4aE',
    ComplianceConfigurationService: '0x13b91c5E824BBC30e1a8Fa06d8528E39C3D8d825',
    WalletManager: '0x47D7da10e46F3df4E6A6Cb9fC03DF36dbfD99917',
    InvestorLockManager: '0x01DA7638f525bBC1D3efc895e81dF6FD25085a40'
  },
  decimals: 0,
  name: 'BlockchainTeam',
  symbol: 'BLKT',
  owners: [
    '0xd51128F302755666c42e3920d72FF2FE632856a9',
    '0x3D3EF8C37c53c34b99C71C1A0518A563F5e8512e'
  ],
  requiredConfirmations: 2,
  chainId: 1,
  complianceManagerType: 'NORMAL',
  lockManagerType: 'INVESTOR',
  noRegistry: undefined,
  partitioned: undefined,
  noOmnibusWallet: true,
  omnibusWallet: undefined
}

   Replacing 'Proxy'
   -----------------
   > transaction hash:    0xc6520ee415deef78af7e1293c850423d5044d5cf7531dbfd088e798907eda372
   > Blocks: 1            Seconds: 5
   > contract address:    0x1B2c0Fd2457B823E4bdADD396bB2CdCFe53b4f0a
   > block number:        7981864
   > block timestamp:     1611956182
   > account:             0xd51128F302755666c42e3920d72FF2FE632856a9
   > balance:             22.4993747388
   > gas used:            237287 (0x39ee7)
   > gas price:           80 gwei
   > value sent:          0 ETH
   > total cost:          0.01898296 ETH


   > Saving migration to chain.
   > Saving artifacts
   -------------------------------------
   > Total cost:          0.01898296 ETH


10_deploy_token_issuer.js
=========================

   Replacing 'Proxy'
   -----------------
   > transaction hash:    0x044b9ed8e5d2a3225834c65a02378d707e1b3b6e6977c6b23904f3677af29837
   > Blocks: 2            Seconds: 21
   > contract address:    0x93176E10a2962061D92617a06abE3F5850E18CD6
   > block number:        7981869
   > block timestamp:     1611956257
   > account:             0xd51128F302755666c42e3920d72FF2FE632856a9
   > balance:             22.4565309788
   > gas used:            237287 (0x39ee7)
   > gas price:           80 gwei
   > value sent:          0 ETH
   > total cost:          0.01898296 ETH


   > Saving migration to chain.
   > Saving artifacts
   -------------------------------------
   > Total cost:          0.01898296 ETH


11_deploy_omnibus_wallet_controller.js
======================================
Skipping omnibus wallet controller

   > Saving migration to chain.
   -------------------------------------
   > Total cost:                   0 ETH


12_deploy_wallet_registrar.js
=============================

   Replacing 'Proxy'
   -----------------
   > transaction hash:    0xe37989f1a43774cace5fae4d39e24611d5792c7b6759c39e3bbe82d80dee3700
   > Blocks: 1            Seconds: 13
   > contract address:    0x276B99166788022D999f66b967e53AB5fA9ba01B
   > block number:        7981874
   > block timestamp:     1611956332
   > account:             0xd51128F302755666c42e3920d72FF2FE632856a9
   > balance:             22.4169069788
   > gas used:            237287 (0x39ee7)
   > gas price:           80 gwei
   > value sent:          0 ETH
   > total cost:          0.01898296 ETH


   > Saving migration to chain.
   > Saving artifacts
   -------------------------------------
   > Total cost:          0.01898296 ETH


13_set_roles.js
===============
Give issuer permissions to token issuer
Give issuer permissions to wallet registrar

   > Saving migration to chain.
   -------------------------------------
   > Total cost:                   0 ETH


14_set_services.js
==================
Connecting compliance configuration to trust service
Connecting compliance manager to trust service
Connecting compliance manager to compliance configuration service
Connecting compliance manager to wallet manager
Connecting compliance manager to lock manager
Connecting compliance service to token
Connecting registry to trust service
Connecting registry to wallet manager
Connecting registry to token
Connecting registry to compliance service
Connecting token to registry
Connecting token to token issuer
Connecting token to wallet registrar
Connecting token issuer to registry
Connecting wallet registrar to registry
Connecting wallet manager to registry
Connecting lock manager to registry
Connecting compliance manager to registry
Connecting token to trust service
Connecting token to compliance service
Connecting token to compliance configuration service
Connecting token to wallet manager
Connecting token to lock manager
Connecting wallet manager to trust service
Connecting lock manager to trust service
Connecting lock manager to compliance service
Connecting lock manager to token
Connecting token issuer to trust service
Connecting token issuer to lock manager
Connecting token issuer to token
Connecting wallet registrar to trust service


Token "BlockchainTeam" (BLKT) [decimals: 0] deployment complete
-------------------------
Token is at address: 0x1B2c0Fd2457B823E4bdADD396bB2CdCFe53b4f0a | Version: 3,4,5,4,4
Trust service is at address: 0xc4c50c4D11aa63Da45B9D047b0E787A04cA27E79 | Version: 3,3
Investor registry is at address: 0xeb931842c3b19Abc29326fec5E03eD4f65161144 | Version: 4,4,5,4
Compliance service is at address: 0x4b316716dC8FA76fb516e7C1d1679A396587E4aE, and is of type NORMAL | Version: 5,4,5,5,3,8
Compliance configuration service is at address: 0x13b91c5E824BBC30e1a8Fa06d8528E39C3D8d825 | Version: 5,4,5,5
Wallet manager is at address: 0x47D7da10e46F3df4E6A6Cb9fC03DF36dbfD99917 | Version: 3,4,5,3
Lock manager is at address: 0x01DA7638f525bBC1D3efc895e81dF6FD25085a40, and is of type INVESTOR. | Version: 3,4,5,3
Token issuer is at address: 0x93176E10a2962061D92617a06abE3F5850E18CD6 | Version: 3,4,5,3
Wallet registrar is at address: 0x276B99166788022D999f66b967e53AB5fA9ba01B | Version: 3,4,5,3

No omnibus wallet controller was deployed.

   > Saving migration to chain.
   -------------------------------------
   > Total cost:                   0 ETH


Summary
=======
> Total deployments:   10
> Final cost:          0.19278952 ETH
```

```bash
truffle migrate --name NinjitsuTeam --symbol NINJA --decimals 0 --network rinkeby2 --owners "0x99E985fA02a5F6EaE2a7F3e93C1c6d65574Cbc61 0x298890444c7AB229384B98CbD41D7442fc355875" --required_confirmations 2 --no_omnibus_wallet
```

```bash
Starting migrations...
======================
> Network name:    'rinkeby2'
> Network id:      4
> Block gas limit: 10000000 (0x989680)


1_initial_migration.js
======================
{
  _: [ 'migrate' ],
  name: 'NinjitsuTeam',
  symbol: 'NINJA',
  decimals: 0,
  network: 'rinkeby2',
  owners: '0x99E985fA02a5F6EaE2a7F3e93C1c6d65574Cbc61 0x298890444c7AB229384B98CbD41D7442fc355875',
  required_confirmations: 2,
  no_omnibus_wallet: true
}

   Deploying 'Migrations'
   ----------------------
   > transaction hash:    0xe49ef0b63cfefd6323e1d1fe227775d358c011c0360ef1fec3f20c6e686d81e1
   > Blocks: 1            Seconds: 18
   > contract address:    0x3d6e0EFb086C7FDf666d545082530f661957Aebc
   > block number:        7981931
   > block timestamp:     1611957187
   > account:             0x99E985fA02a5F6EaE2a7F3e93C1c6d65574Cbc61
   > balance:             1.9138241384
   > gas used:            274286 (0x42f6e)
   > gas price:           80 gwei
   > value sent:          0 ETH
   > total cost:          0.02194288 ETH


   > Saving migration to chain.
   > Saving artifacts
   -------------------------------------
   > Total cost:          0.02194288 ETH


2_deploy_trust_manager.js
=========================

   Deploying 'Proxy'
   -----------------
   > transaction hash:    0x2e7e46eec87c48f929fa04dcb04d32cc40f786bbdadc9eb89091cf9becf63704
   > Blocks: 1            Seconds: 9
   > contract address:    0x4a3a83e9E25516260DCF2766403dA167fC30F7f6
   > block number:        7981933
   > block timestamp:     1611957217
   > account:             0x99E985fA02a5F6EaE2a7F3e93C1c6d65574Cbc61
   > balance:             1.8914570984
   > gas used:            237287 (0x39ee7)
   > gas price:           80 gwei
   > value sent:          0 ETH
   > total cost:          0.01898296 ETH


   > Saving migration to chain.
   > Saving artifacts
   -------------------------------------
   > Total cost:          0.01898296 ETH


3_deploy_registry.js
====================

   Replacing 'Proxy'
   -----------------
   > transaction hash:    0x835b7c0ef70ad3a590a669587c71421d3e7044e23498e82d384e25e2756bffde
   > Blocks: 1            Seconds: 9
   > contract address:    0x0633F2eC79C697a044D543C33D23d0173530e313
   > block number:        7981937
   > block timestamp:     1611957277
   > account:             0x99E985fA02a5F6EaE2a7F3e93C1c6d65574Cbc61
   > balance:             1.8559542984
   > gas used:            237287 (0x39ee7)
   > gas price:           80 gwei
   > value sent:          0 ETH
   > total cost:          0.01898296 ETH


   > Saving migration to chain.
   > Saving artifacts
   -------------------------------------
   > Total cost:          0.01898296 ETH


4_deploy_compliance_manager.js
==============================

   Replacing 'Proxy'
   -----------------
   > transaction hash:    0x6a0be4c38c8438b275e8db11dfb21fdae0420dfdc79a8076395f2addc8e8f056
   > Blocks: 0            Seconds: 0
   > contract address:    0xf9E846fDc153d93338316c75667ec3825FeEeadD
   > block number:        7981941
   > block timestamp:     1611957337
   > account:             0x99E985fA02a5F6EaE2a7F3e93C1c6d65574Cbc61
   > balance:             1.8185266184
   > gas used:            237287 (0x39ee7)
   > gas price:           80 gwei
   > value sent:          0 ETH
   > total cost:          0.01898296 ETH


   > Saving migration to chain.
   > Saving artifacts
   -------------------------------------
   > Total cost:          0.01898296 ETH


5_deploy_complianceConfiguration.js
===================================

   Replacing 'Proxy'
   -----------------
   > transaction hash:    0x377f8d6367ec3bd451fe94378d828af348cf43ffcebf3501c04335b56c7106be
   > Blocks: 1            Seconds: 9
   > contract address:    0xFD6729c6c3AE77d1848Ae7BaBD57A9ABAeA14244
   > block number:        7981945
   > block timestamp:     1611957397
   > account:             0x99E985fA02a5F6EaE2a7F3e93C1c6d65574Cbc61
   > balance:             1.7772871784
   > gas used:            237287 (0x39ee7)
   > gas price:           80 gwei
   > value sent:          0 ETH
   > total cost:          0.01898296 ETH


   > Saving migration to chain.
   > Saving artifacts
   -------------------------------------
   > Total cost:          0.01898296 ETH


6_deploy_wallet_manager.js
==========================

   Replacing 'Proxy'
   -----------------
   > transaction hash:    0x878af27215d9e808d4c276b872f4e3407744399fabeb70ffffc31c99fcba4c57
   > Blocks: 1            Seconds: 10
   > contract address:    0x7c16100673ACef01cE566DAC3910F9719F198601
   > block number:        7981949
   > block timestamp:     1611957457
   > account:             0x99E985fA02a5F6EaE2a7F3e93C1c6d65574Cbc61
   > balance:             1.7398560584
   > gas used:            237287 (0x39ee7)
   > gas price:           80 gwei
   > value sent:          0 ETH
   > total cost:          0.01898296 ETH


   > Saving migration to chain.
   > Saving artifacts
   -------------------------------------
   > Total cost:          0.01898296 ETH


7_deploy_lock_manager.js
========================

   Replacing 'Proxy'
   -----------------
   > transaction hash:    0x61cc3c87d1113f44f08664ab675f48b858e51399580d916416b0e10e395bed75
   > Blocks: 1            Seconds: 17
   > contract address:    0xE3C6578d33678604e428aFe1fa6346e096120D39
   > block number:        7981954
   > block timestamp:     1611957532
   > account:             0x99E985fA02a5F6EaE2a7F3e93C1c6d65574Cbc61
   > balance:             1.7024143784
   > gas used:            237287 (0x39ee7)
   > gas price:           80 gwei
   > value sent:          0 ETH
   > total cost:          0.01898296 ETH


   > Saving migration to chain.
   > Saving artifacts
   -------------------------------------
   > Total cost:          0.01898296 ETH


8_deploy_partitions_manager.js
==============================

   > Saving migration to chain.
   -------------------------------------
   > Total cost:                   0 ETH


9_deploy_token.js
=================
ConfigurationManager {
  proxiesAddresses: {
    TrustService: '0x4a3a83e9E25516260DCF2766403dA167fC30F7f6',
    RegistryService: '0x0633F2eC79C697a044D543C33D23d0173530e313',
    ComplianceServiceRegulated: '0xf9E846fDc153d93338316c75667ec3825FeEeadD',
    ComplianceConfigurationService: '0xFD6729c6c3AE77d1848Ae7BaBD57A9ABAeA14244',
    WalletManager: '0x7c16100673ACef01cE566DAC3910F9719F198601',
    InvestorLockManager: '0xE3C6578d33678604e428aFe1fa6346e096120D39'
  },
  decimals: 0,
  name: 'NinjitsuTeam',
  symbol: 'NINJA',
  owners: [
    '0x99E985fA02a5F6EaE2a7F3e93C1c6d65574Cbc61',
    '0x298890444c7AB229384B98CbD41D7442fc355875'
  ],
  requiredConfirmations: 2,
  chainId: 1,
  complianceManagerType: 'NORMAL',
  lockManagerType: 'INVESTOR',
  noRegistry: undefined,
  partitioned: undefined,
  noOmnibusWallet: true,
  omnibusWallet: undefined
}

   Replacing 'Proxy'
   -----------------
   > transaction hash:    0xb977f8baa95ab566c6a347bf6e4a12575c2131aa0303e33fb333b08df6d81b90
   > Blocks: 1            Seconds: 9
   > contract address:    0x06D60cC59E85465000D1D51028eC4DC827a81724
   > block number:        7981959
   > block timestamp:     1611957607
   > account:             0x99E985fA02a5F6EaE2a7F3e93C1c6d65574Cbc61
   > balance:             1.6627938984
   > gas used:            237287 (0x39ee7)
   > gas price:           80 gwei
   > value sent:          0 ETH
   > total cost:          0.01898296 ETH


   > Saving migration to chain.
   > Saving artifacts
   -------------------------------------
   > Total cost:          0.01898296 ETH


10_deploy_token_issuer.js
=========================

   Replacing 'Proxy'
   -----------------
   > transaction hash:    0x062d3cf35c1dd5d332386099ec7914a9ca29eb90fbdac2055b2de3e3cf51df21
   > Blocks: 2            Seconds: 19
   > contract address:    0x93C0398404E22bA5c2b41A3113354547bA5201f0
   > block number:        7981964
   > block timestamp:     1611957682
   > account:             0x99E985fA02a5F6EaE2a7F3e93C1c6d65574Cbc61
   > balance:             1.6199510984
   > gas used:            237287 (0x39ee7)
   > gas price:           80 gwei
   > value sent:          0 ETH
   > total cost:          0.01898296 ETH


   > Saving migration to chain.
   > Saving artifacts
   -------------------------------------
   > Total cost:          0.01898296 ETH


11_deploy_omnibus_wallet_controller.js
======================================
Skipping omnibus wallet controller

   > Saving migration to chain.
   -------------------------------------
   > Total cost:                   0 ETH


12_deploy_wallet_registrar.js
=============================

   Replacing 'Proxy'
   -----------------
   > transaction hash:    0x6df50e6406196e1a417103f35bf34207649e873ed8006902f9a4d0606c724f3c
   > Blocks: 1            Seconds: 17
   > contract address:    0xf66B523356EA604fADa7cF376272162ac8F5bb48
   > block number:        7981970
   > block timestamp:     1611957772
   > account:             0x99E985fA02a5F6EaE2a7F3e93C1c6d65574Cbc61
   > balance:             1.5803270984
   > gas used:            237287 (0x39ee7)
   > gas price:           80 gwei
   > value sent:          0 ETH
   > total cost:          0.01898296 ETH


   > Saving migration to chain.
   > Saving artifacts
   -------------------------------------
   > Total cost:          0.01898296 ETH


13_set_roles.js
===============
Give issuer permissions to token issuer
Give issuer permissions to wallet registrar

   > Saving migration to chain.
   -------------------------------------
   > Total cost:                   0 ETH


14_set_services.js
==================
Connecting compliance configuration to trust service
Connecting compliance manager to trust service
Connecting compliance manager to compliance configuration service
Connecting compliance manager to wallet manager
Connecting compliance manager to lock manager
Connecting compliance service to token
Connecting registry to trust service
Connecting registry to wallet manager
Connecting registry to token
Connecting registry to compliance service
Connecting token to registry
Connecting token to token issuer
Connecting token to wallet registrar
Connecting token issuer to registry
Connecting wallet registrar to registry
Connecting wallet manager to registry
Connecting lock manager to registry
Connecting compliance manager to registry
Connecting token to trust service
Connecting token to compliance service
Connecting token to compliance configuration service
Connecting token to wallet manager
Connecting token to lock manager
Connecting wallet manager to trust service
Connecting lock manager to trust service
Connecting lock manager to compliance service
Connecting lock manager to token
Connecting token issuer to trust service
Connecting token issuer to lock manager
Connecting token issuer to token
Connecting wallet registrar to trust service


Token "NinjitsuTeam" (NINJA) [decimals: 0] deployment complete
-------------------------
Token is at address: 0x06D60cC59E85465000D1D51028eC4DC827a81724 | Version: 3,4,5,4,4
Trust service is at address: 0x4a3a83e9E25516260DCF2766403dA167fC30F7f6 | Version: 3,3
Investor registry is at address: 0x0633F2eC79C697a044D543C33D23d0173530e313 | Version: 4,4,5,4
Compliance service is at address: 0xf9E846fDc153d93338316c75667ec3825FeEeadD, and is of type NORMAL | Version: 5,4,5,5,3,8
Compliance configuration service is at address: 0xFD6729c6c3AE77d1848Ae7BaBD57A9ABAeA14244 | Version: 5,4,5,5
Wallet manager is at address: 0x7c16100673ACef01cE566DAC3910F9719F198601 | Version: 3,4,5,3
Lock manager is at address: 0xE3C6578d33678604e428aFe1fa6346e096120D39, and is of type INVESTOR. | Version: 3,4,5,3
Token issuer is at address: 0x93C0398404E22bA5c2b41A3113354547bA5201f0 | Version: 3,4,5,3
Wallet registrar is at address: 0xf66B523356EA604fADa7cF376272162ac8F5bb48 | Version: 3,4,5,3

No omnibus wallet controller was deployed.

   > Saving migration to chain.
   -------------------------------------
   > Total cost:                   0 ETH


Summary
=======
> Total deployments:   10
> Final cost:          0.19278952 ETH

```

```bash
truffle migrate --name ChicagoTeam --symbol CANCH --decimals 0 --network rinkeby3 --owners "0x3D3EF8C37c53c34b99C71C1A0518A563F5e8512e 0x6e89F6fa95D517eE7a0a293D8A1d3C502bfB0701" --required_confirmations 2 --no_omnibus_wallet
```

```bash
Starting migrations...
======================
> Network name:    'rinkeby3'
> Network id:      4
> Block gas limit: 10000000 (0x989680)


1_initial_migration.js
======================
{
  _: [ 'migrate' ],
  name: 'ChicagoTeam',
  symbol: 'CANCH',
  decimals: 0,
  network: 'rinkeby3',
  owners: '0x3D3EF8C37c53c34b99C71C1A0518A563F5e8512e 0x6e89F6fa95D517eE7a0a293D8A1d3C502bfB0701',
  required_confirmations: 2,
  no_omnibus_wallet: true
}

   Deploying 'Migrations'
   ----------------------
   > transaction hash:    0x12af8c736282a660bc480eb09923fc7ee81ffe63a240b1ec4f56613db60db949
   > Blocks: 2            Seconds: 23
   > contract address:    0x5E577bD8A31F51876De9b6478f917022D9639381
   > block number:        7982112
   > block timestamp:     1611959902
   > account:             0x3D3EF8C37c53c34b99C71C1A0518A563F5e8512e
   > balance:             3.97784712
   > gas used:            274286 (0x42f6e)
   > gas price:           80 gwei
   > value sent:          0 ETH
   > total cost:          0.02194288 ETH


   > Saving migration to chain.
   > Saving artifacts
   -------------------------------------
   > Total cost:          0.02194288 ETH


2_deploy_trust_manager.js
=========================

   Deploying 'Proxy'
   -----------------
   > transaction hash:    0xf3e259617bf8f8302d4f7db3aa40c07ae8814e753ed3df35aad1c76eba7e4979
   > Blocks: 1            Seconds: 11
   > contract address:    0xd0ecDFdc1e45C9EbAd983bba7B687195320833de
   > block number:        7982114
   > block timestamp:     1611959932
   > account:             0x3D3EF8C37c53c34b99C71C1A0518A563F5e8512e
   > balance:             3.95548008
   > gas used:            237287 (0x39ee7)
   > gas price:           80 gwei
   > value sent:          0 ETH
   > total cost:          0.01898296 ETH


   > Saving migration to chain.
   > Saving artifacts
   -------------------------------------
   > Total cost:          0.01898296 ETH


3_deploy_registry.js
====================

   Replacing 'Proxy'
   -----------------
   > transaction hash:    0xd3bcf726bc35a8aa600f30c008e6d1de087676240f59ad58704b25136793cb8e
   > Blocks: 1            Seconds: 13
   > contract address:    0x205e1e6832F4287BceaBde160a1335b45524e14f
   > block number:        7982120
   > block timestamp:     1611960022
   > account:             0x3D3EF8C37c53c34b99C71C1A0518A563F5e8512e
   > balance:             3.91997728
   > gas used:            237287 (0x39ee7)
   > gas price:           80 gwei
   > value sent:          0 ETH
   > total cost:          0.01898296 ETH


   > Saving migration to chain.
   > Saving artifacts
   -------------------------------------
   > Total cost:          0.01898296 ETH


4_deploy_compliance_manager.js
==============================

   Replacing 'Proxy'
   -----------------
   > transaction hash:    0x8f1ea9bba2d31d9b00de24b0f4dead89cb720530410787385b3f71b54d30246b
   > Blocks: 1            Seconds: 17
   > contract address:    0xce23C8F1108fd486F9A9831D04eB68364Dfc8249
   > block number:        7982125
   > block timestamp:     1611960097
   > account:             0x3D3EF8C37c53c34b99C71C1A0518A563F5e8512e
   > balance:             3.8825496
   > gas used:            237287 (0x39ee7)
   > gas price:           80 gwei
   > value sent:          0 ETH
   > total cost:          0.01898296 ETH


   > Saving migration to chain.
   > Saving artifacts
   -------------------------------------
   > Total cost:          0.01898296 ETH


5_deploy_complianceConfiguration.js
===================================

   Replacing 'Proxy'
   -----------------
   > transaction hash:    0x8b3d84f03aa543e9bc7efdf561da270bbd694fa07eb93f2360525b6427c0d94c
   > Blocks: 2            Seconds: 17
   > contract address:    0x03001b39bdBC890F3703BdECBBc55FC4E19588cB
   > block number:        7982130
   > block timestamp:     1611960172
   > account:             0x3D3EF8C37c53c34b99C71C1A0518A563F5e8512e
   > balance:             3.84131016
   > gas used:            237287 (0x39ee7)
   > gas price:           80 gwei
   > value sent:          0 ETH
   > total cost:          0.01898296 ETH


   > Saving migration to chain.
   > Saving artifacts
   -------------------------------------
   > Total cost:          0.01898296 ETH


6_deploy_wallet_manager.js
==========================

   Replacing 'Proxy'
   -----------------
   > transaction hash:    0xfc833077bc36271196512ac1e699ebb1658dc6563809888b6679e76b0e31124e
   > Blocks: 1            Seconds: 9
   > contract address:    0x5A9a733A0D08cCdB05007359dD263DF310231604
   > block number:        7982134
   > block timestamp:     1611960232
   > account:             0x3D3EF8C37c53c34b99C71C1A0518A563F5e8512e
   > balance:             3.80387904
   > gas used:            237287 (0x39ee7)
   > gas price:           80 gwei
   > value sent:          0 ETH
   > total cost:          0.01898296 ETH


   > Saving migration to chain.
   > Saving artifacts
   -------------------------------------
   > Total cost:          0.01898296 ETH


7_deploy_lock_manager.js
========================

   Replacing 'Proxy'
   -----------------
   > transaction hash:    0x218695c1cf64ef4bfc50dd1e380339d9d155ed56f7043e2a4b18b0e6114ee224
   > Blocks: 0            Seconds: 5
   > contract address:    0xd866ceec865a8F1fc53786Ef1124e9C83F634e45
   > block number:        7982138
   > block timestamp:     1611960292
   > account:             0x3D3EF8C37c53c34b99C71C1A0518A563F5e8512e
   > balance:             3.76643736
   > gas used:            237287 (0x39ee7)
   > gas price:           80 gwei
   > value sent:          0 ETH
   > total cost:          0.01898296 ETH


   > Saving migration to chain.
   > Saving artifacts
   -------------------------------------
   > Total cost:          0.01898296 ETH


8_deploy_partitions_manager.js
==============================

   > Saving migration to chain.
   -------------------------------------
   > Total cost:                   0 ETH


9_deploy_token.js
=================
ConfigurationManager {
  proxiesAddresses: {
    TrustService: '0xd0ecDFdc1e45C9EbAd983bba7B687195320833de',
    RegistryService: '0x205e1e6832F4287BceaBde160a1335b45524e14f',
    ComplianceServiceRegulated: '0xce23C8F1108fd486F9A9831D04eB68364Dfc8249',
    ComplianceConfigurationService: '0x03001b39bdBC890F3703BdECBBc55FC4E19588cB',
    WalletManager: '0x5A9a733A0D08cCdB05007359dD263DF310231604',
    InvestorLockManager: '0xd866ceec865a8F1fc53786Ef1124e9C83F634e45'
  },
  decimals: 0,
  name: 'ChicagoTeam',
  symbol: 'CANCH',
  owners: [
    '0x3D3EF8C37c53c34b99C71C1A0518A563F5e8512e',
    '0x6e89F6fa95D517eE7a0a293D8A1d3C502bfB0701'
  ],
  requiredConfirmations: 2,
  chainId: 1,
  complianceManagerType: 'NORMAL',
  lockManagerType: 'INVESTOR',
  noRegistry: undefined,
  partitioned: undefined,
  noOmnibusWallet: true,
  omnibusWallet: undefined
}

   Replacing 'Proxy'
   -----------------
   > transaction hash:    0x6ae70fc0c33bb4956c85587994e0cdd60b22dc9aab84a1736cd9c3c1c9eca871
   > Blocks: 1            Seconds: 9
   > contract address:    0x6016e5e9CF8BFf4CD03F535FbF5f430E3c3e0676
   > block number:        7982143
   > block timestamp:     1611960367
   > account:             0x3D3EF8C37c53c34b99C71C1A0518A563F5e8512e
   > balance:             3.72681688
   > gas used:            237287 (0x39ee7)
   > gas price:           80 gwei
   > value sent:          0 ETH
   > total cost:          0.01898296 ETH


   > Saving migration to chain.
   > Saving artifacts
   -------------------------------------
   > Total cost:          0.01898296 ETH


10_deploy_token_issuer.js
=========================

   Replacing 'Proxy'
   -----------------
   > transaction hash:    0xce6c864e0cd4ec58c421fe090f13f0047e3e0b2c2ceffc64f94fd0a54b3ab389
   > Blocks: 0            Seconds: 9
   > contract address:    0x4F75132BA0581AA7aFee1a4207EbdA8b0e543959
   > block number:        7982147
   > block timestamp:     1611960427
   > account:             0x3D3EF8C37c53c34b99C71C1A0518A563F5e8512e
   > balance:             3.68397504
   > gas used:            237287 (0x39ee7)
   > gas price:           80 gwei
   > value sent:          0 ETH
   > total cost:          0.01898296 ETH


   > Saving migration to chain.
   > Saving artifacts
   -------------------------------------
   > Total cost:          0.01898296 ETH


11_deploy_omnibus_wallet_controller.js
======================================
Skipping omnibus wallet controller

   > Saving migration to chain.
   -------------------------------------
   > Total cost:                   0 ETH


12_deploy_wallet_registrar.js
=============================

   Replacing 'Proxy'
   -----------------
   > transaction hash:    0x4231bb6d0793a6128baa4ee80fafd12c11c39fbe100542217ca4e579c7c639f5
   > Blocks: 1            Seconds: 9
   > contract address:    0x186E315B47118F9174d5BAcF71cdC24574C36a9f
   > block number:        7982152
   > block timestamp:     1611960502
   > account:             0x3D3EF8C37c53c34b99C71C1A0518A563F5e8512e
   > balance:             3.64435104
   > gas used:            237287 (0x39ee7)
   > gas price:           80 gwei
   > value sent:          0 ETH
   > total cost:          0.01898296 ETH


   > Saving migration to chain.
   > Saving artifacts
   -------------------------------------
   > Total cost:          0.01898296 ETH


13_set_roles.js
===============
Give issuer permissions to token issuer
Give issuer permissions to wallet registrar

   > Saving migration to chain.
   -------------------------------------
   > Total cost:                   0 ETH


14_set_services.js
==================
Connecting compliance configuration to trust service
Connecting compliance manager to trust service
Connecting compliance manager to compliance configuration service
Connecting compliance manager to wallet manager
Connecting compliance manager to lock manager
Connecting compliance service to token
Connecting registry to trust service
Connecting registry to wallet manager
Connecting registry to token
Connecting registry to compliance service
Connecting token to registry
Connecting token to token issuer
Connecting token to wallet registrar
Connecting token issuer to registry
Connecting wallet registrar to registry
Connecting wallet manager to registry
Connecting lock manager to registry
Connecting compliance manager to registry
Connecting token to trust service
Connecting token to compliance service
Connecting token to compliance configuration service
Connecting token to wallet manager
Connecting token to lock manager
Connecting wallet manager to trust service
Connecting lock manager to trust service
Connecting lock manager to compliance service
Connecting lock manager to token
Connecting token issuer to trust service
Connecting token issuer to lock manager
Connecting token issuer to token
Connecting wallet registrar to trust service


Token "ChicagoTeam" (CANCH) [decimals: 0] deployment complete
-------------------------
Token is at address: 0x6016e5e9CF8BFf4CD03F535FbF5f430E3c3e0676 | Version: 3,4,5,4,4
Trust service is at address: 0xd0ecDFdc1e45C9EbAd983bba7B687195320833de | Version: 3,3
Investor registry is at address: 0x205e1e6832F4287BceaBde160a1335b45524e14f | Version: 4,4,5,4
Compliance service is at address: 0xce23C8F1108fd486F9A9831D04eB68364Dfc8249, and is of type NORMAL | Version: 5,4,5,5,3,8
Compliance configuration service is at address: 0x03001b39bdBC890F3703BdECBBc55FC4E19588cB | Version: 5,4,5,5
Wallet manager is at address: 0x5A9a733A0D08cCdB05007359dD263DF310231604 | Version: 3,4,5,3
Lock manager is at address: 0xd866ceec865a8F1fc53786Ef1124e9C83F634e45, and is of type INVESTOR. | Version: 3,4,5,3
Token issuer is at address: 0x4F75132BA0581AA7aFee1a4207EbdA8b0e543959 | Version: 3,4,5,3
Wallet registrar is at address: 0x186E315B47118F9174d5BAcF71cdC24574C36a9f | Version: 3,4,5,3

No omnibus wallet controller was deployed.

   > Saving migration to chain.
   -------------------------------------
   > Total cost:                   0 ETH


Summary
=======
> Total deployments:   10
> Final cost:          0.19278952 ETH


```

```bash
truffle migrate --name YautjaToken --symbol PREDATOR --decimals 0 --network rinkeby --owners "0xd51128F302755666c42e3920d72FF2FE632856a9 0x3D3EF8C37c53c34b99C71C1A0518A563F5e8512e" --required_confirmations 2 --no_omnibus_wallet
```

```bash
Starting migrations...
======================
> Network name:    'rinkeby'
> Network id:      4
> Block gas limit: 10000000 (0x989680)


1_initial_migration.js
======================
{
  _: [ 'migrate' ],
  name: 'YautjaToken',
  symbol: 'PREDATOR',
  decimals: 0,
  network: 'rinkeby',
  owners: '0xd51128F302755666c42e3920d72FF2FE632856a9 0x3D3EF8C37c53c34b99C71C1A0518A563F5e8512e',
  required_confirmations: 2,
  no_omnibus_wallet: true
}

   Deploying 'Migrations'
   ----------------------
   > transaction hash:    0x3d3d28ee865f22007ba78a54bbf46be81b577eba35834feeed09df6eb8b52148
   > Blocks: 0            Seconds: 5
   > contract address:    0x58C2ab32D311bb34438d3ABbE6740Ee07676B4a7
   > block number:        7982245
   > block timestamp:     1611961897
   > account:             0xd51128F302755666c42e3920d72FF2FE632856a9
   > balance:             22.2367412188
   > gas used:            274286 (0x42f6e)
   > gas price:           80 gwei
   > value sent:          0 ETH
   > total cost:          0.02194288 ETH


   > Saving migration to chain.
   > Saving artifacts
   -------------------------------------
   > Total cost:          0.02194288 ETH


2_deploy_trust_manager.js
=========================

   Deploying 'Proxy'
   -----------------
   > transaction hash:    0x59cdc108fe14c4ddad9251c619fee1506fbdeee8d56d6b1e702ce41d34fb634b
   > Blocks: 0            Seconds: 5
   > contract address:    0x7CCce3deFA936077699D528eB5e4E1f8731EF6c8
   > block number:        7982247
   > block timestamp:     1611961927
   > account:             0xd51128F302755666c42e3920d72FF2FE632856a9
   > balance:             22.2143741788
   > gas used:            237287 (0x39ee7)
   > gas price:           80 gwei
   > value sent:          0 ETH
   > total cost:          0.01898296 ETH


   > Saving migration to chain.
   > Saving artifacts
   -------------------------------------
   > Total cost:          0.01898296 ETH


3_deploy_registry.js
====================

   Replacing 'Proxy'
   -----------------
   > transaction hash:    0x06740929805cd7afa8647d3d9ea45041f334cb503526bc0e4fec758f4d9c21d0
   > Blocks: 1            Seconds: 17
   > contract address:    0x786b21fDedbEB68D25cb4F8AD8eB56eA02504686
   > block number:        7982253
   > block timestamp:     1611962017
   > account:             0xd51128F302755666c42e3920d72FF2FE632856a9
   > balance:             22.1788713788
   > gas used:            237287 (0x39ee7)
   > gas price:           80 gwei
   > value sent:          0 ETH
   > total cost:          0.01898296 ETH


   > Saving migration to chain.
   > Saving artifacts
   -------------------------------------
   > Total cost:          0.01898296 ETH


4_deploy_compliance_manager.js
==============================

   Replacing 'Proxy'
   -----------------
   > transaction hash:    0x584603a26aa67d78bbba78895d8b5e5d9706059ca9bf35dfe8869f5ccda1cbbd
   > Blocks: 1            Seconds: 13
   > contract address:    0xd52D64D6FB142Bde41FD7936E6e09C4BF48dC309
   > block number:        7982258
   > block timestamp:     1611962092
   > account:             0xd51128F302755666c42e3920d72FF2FE632856a9
   > balance:             22.1414436988
   > gas used:            237287 (0x39ee7)
   > gas price:           80 gwei
   > value sent:          0 ETH
   > total cost:          0.01898296 ETH


   > Saving migration to chain.
   > Saving artifacts
   -------------------------------------
   > Total cost:          0.01898296 ETH


5_deploy_complianceConfiguration.js
===================================

   Replacing 'Proxy'
   -----------------
   > transaction hash:    0x5c7bcd1a28480a913b052aa69da4bdfb928a693da02ce9804c49198c0718ae57
   > Blocks: 1            Seconds: 18
   > contract address:    0xb76570D76fdBE7CD7e2679cc281C78C31b516457
   > block number:        7982263
   > block timestamp:     1611962167
   > account:             0xd51128F302755666c42e3920d72FF2FE632856a9
   > balance:             22.1002042588
   > gas used:            237287 (0x39ee7)
   > gas price:           80 gwei
   > value sent:          0 ETH
   > total cost:          0.01898296 ETH


   > Saving migration to chain.
   > Saving artifacts
   -------------------------------------
   > Total cost:          0.01898296 ETH


6_deploy_wallet_manager.js
==========================

   Replacing 'Proxy'
   -----------------
   > transaction hash:    0x1284ffacf01991ad3e36e06f6909c426be3d8b2bcf40bbb8e931240c68aee659
   > Blocks: 0            Seconds: 5
   > contract address:    0xEd313feB1a9F6b11D26324941dBe81ae5e414206
   > block number:        7982267
   > block timestamp:     1611962227
   > account:             0xd51128F302755666c42e3920d72FF2FE632856a9
   > balance:             22.0627731388
   > gas used:            237287 (0x39ee7)
   > gas price:           80 gwei
   > value sent:          0 ETH
   > total cost:          0.01898296 ETH


   > Saving migration to chain.
   > Saving artifacts
   -------------------------------------
   > Total cost:          0.01898296 ETH


7_deploy_lock_manager.js
========================

   Replacing 'Proxy'
   -----------------
   > transaction hash:    0x7fa91a0245e3a79068239a1747e21aeda7d94c6ebfb49191b3d5d4a359fb98d3
   > Blocks: 2            Seconds: 17
   > contract address:    0xc701f225d0129F13365E4Ad3E997AdEde6E3f11B
   > block number:        7982272
   > block timestamp:     1611962302
   > account:             0xd51128F302755666c42e3920d72FF2FE632856a9
   > balance:             22.0253314588
   > gas used:            237287 (0x39ee7)
   > gas price:           80 gwei
   > value sent:          0 ETH
   > total cost:          0.01898296 ETH


   > Saving migration to chain.
   > Saving artifacts
   -------------------------------------
   > Total cost:          0.01898296 ETH


8_deploy_partitions_manager.js
==============================

   > Saving migration to chain.
   -------------------------------------
   > Total cost:                   0 ETH


9_deploy_token.js
=================
ConfigurationManager {
  proxiesAddresses: {
    TrustService: '0x7CCce3deFA936077699D528eB5e4E1f8731EF6c8',
    RegistryService: '0x786b21fDedbEB68D25cb4F8AD8eB56eA02504686',
    ComplianceServiceRegulated: '0xd52D64D6FB142Bde41FD7936E6e09C4BF48dC309',
    ComplianceConfigurationService: '0xb76570D76fdBE7CD7e2679cc281C78C31b516457',
    WalletManager: '0xEd313feB1a9F6b11D26324941dBe81ae5e414206',
    InvestorLockManager: '0xc701f225d0129F13365E4Ad3E997AdEde6E3f11B'
  },
  decimals: 0,
  name: 'YautjaToken',
  symbol: 'PREDATOR',
  owners: [
    '0xd51128F302755666c42e3920d72FF2FE632856a9',
    '0x3D3EF8C37c53c34b99C71C1A0518A563F5e8512e'
  ],
  requiredConfirmations: 2,
  chainId: 1,
  complianceManagerType: 'NORMAL',
  lockManagerType: 'INVESTOR',
  noRegistry: undefined,
  partitioned: undefined,
  noOmnibusWallet: true,
  omnibusWallet: undefined
}

   Replacing 'Proxy'
   -----------------
   > transaction hash:    0x5403f8d98dc452b687ed441bc16d0bfdf1d75f52d060854b9039e6d0bcaeeb88
   > Blocks: 0            Seconds: 5
   > contract address:    0x6F42729f103a6C612B8B50Ca64db5f451707170B
   > block number:        7982277
   > block timestamp:     1611962377
   > account:             0xd51128F302755666c42e3920d72FF2FE632856a9
   > balance:             21.9857109788
   > gas used:            237287 (0x39ee7)
   > gas price:           80 gwei
   > value sent:          0 ETH
   > total cost:          0.01898296 ETH


   > Saving migration to chain.
   > Saving artifacts
   -------------------------------------
   > Total cost:          0.01898296 ETH


10_deploy_token_issuer.js
=========================

   Replacing 'Proxy'
   -----------------
   > transaction hash:    0xd21b91566735b11bd2777b7b3a2bf1353caf727994dc4cd0fb707f1a7ff5b70d
   > Blocks: 1            Seconds: 17
   > contract address:    0xdBe6CFc193b8644e90230B2E2213e68e76B1E3Ba
   > block number:        7982282
   > block timestamp:     1611962452
   > account:             0xd51128F302755666c42e3920d72FF2FE632856a9
   > balance:             21.9428662588
   > gas used:            237287 (0x39ee7)
   > gas price:           80 gwei
   > value sent:          0 ETH
   > total cost:          0.01898296 ETH


   > Saving migration to chain.
   > Saving artifacts
   -------------------------------------
   > Total cost:          0.01898296 ETH


11_deploy_omnibus_wallet_controller.js
======================================
Skipping omnibus wallet controller

   > Saving migration to chain.
   -------------------------------------
   > Total cost:                   0 ETH


12_deploy_wallet_registrar.js
=============================

   Replacing 'Proxy'
   -----------------
   > transaction hash:    0xa6fe85c93956fe69a9e26dc905c2d00a712cda89fa89e85122fe638d4d1849a9
   > Blocks: 1            Seconds: 9
   > contract address:    0xEf3915d23F091DF36Fa95aD744Fe0a24d6AA83FF
   > block number:        7982287
   > block timestamp:     1611962527
   > account:             0xd51128F302755666c42e3920d72FF2FE632856a9
   > balance:             21.9032422588
   > gas used:            237287 (0x39ee7)
   > gas price:           80 gwei
   > value sent:          0 ETH
   > total cost:          0.01898296 ETH


   > Saving migration to chain.
   > Saving artifacts
   -------------------------------------
   > Total cost:          0.01898296 ETH


13_set_roles.js
===============
Give issuer permissions to token issuer
Give issuer permissions to wallet registrar

   > Saving migration to chain.
   -------------------------------------
   > Total cost:                   0 ETH


14_set_services.js
==================
Connecting compliance configuration to trust service
Connecting compliance manager to trust service
Connecting compliance manager to compliance configuration service
Connecting compliance manager to wallet manager
Connecting compliance manager to lock manager
Connecting compliance service to token
Connecting registry to trust service
Connecting registry to wallet manager
Connecting registry to token
Connecting registry to compliance service
Connecting token to registry
Connecting token to token issuer
Connecting token to wallet registrar
Connecting token issuer to registry
Connecting wallet registrar to registry
Connecting wallet manager to registry
Connecting lock manager to registry
Connecting compliance manager to registry
Connecting token to trust service
Connecting token to compliance service
Connecting token to compliance configuration service
Connecting token to wallet manager
Connecting token to lock manager
Connecting wallet manager to trust service
Connecting lock manager to trust service
Connecting lock manager to compliance service
Connecting lock manager to token
Connecting token issuer to trust service
Connecting token issuer to lock manager
Connecting token issuer to token
Connecting wallet registrar to trust service


Token "YautjaToken" (PREDATOR) [decimals: 0] deployment complete
-------------------------
Token is at address: 0x6F42729f103a6C612B8B50Ca64db5f451707170B | Version: 3,4,5,4,4
Trust service is at address: 0x7CCce3deFA936077699D528eB5e4E1f8731EF6c8 | Version: 3,3
Investor registry is at address: 0x786b21fDedbEB68D25cb4F8AD8eB56eA02504686 | Version: 4,4,5,4
Compliance service is at address: 0xd52D64D6FB142Bde41FD7936E6e09C4BF48dC309, and is of type NORMAL | Version: 5,4,5,5,3,8
Compliance configuration service is at address: 0xb76570D76fdBE7CD7e2679cc281C78C31b516457 | Version: 5,4,5,5
Wallet manager is at address: 0xEd313feB1a9F6b11D26324941dBe81ae5e414206 | Version: 3,4,5,3
Lock manager is at address: 0xc701f225d0129F13365E4Ad3E997AdEde6E3f11B, and is of type INVESTOR. | Version: 3,4,5,3
Token issuer is at address: 0xdBe6CFc193b8644e90230B2E2213e68e76B1E3Ba | Version: 3,4,5,3
Wallet registrar is at address: 0xEf3915d23F091DF36Fa95aD744Fe0a24d6AA83FF | Version: 3,4,5,3

No omnibus wallet controller was deployed.

   > Saving migration to chain.
   -------------------------------------
   > Total cost:                   0 ETH


Summary
=======
> Total deployments:   10
> Final cost:          0.19278952 ETH
```

## Deployed Proxy Tokens

- [0x1B2c0Fd2457B823E4bdADD396bB2CdCFe53b4f0a](https://rinkeby.etherscan.io/token/0x1b2c0fd2457b823e4bdadd396bb2cdcfe53b4f0a)

- [0x06D60cC59E85465000D1D51028eC4DC827a81724](https://rinkeby.etherscan.io/token/0x06D60cC59E85465000D1D51028eC4DC827a81724)

- [0x6016e5e9CF8BFf4CD03F535FbF5f430E3c3e0676](https://rinkeby.etherscan.io/token/0x6016e5e9CF8BFf4CD03F535FbF5f430E3c3e0676)

- [0x6F42729f103a6C612B8B50Ca64db5f451707170B](https://rinkeby.etherscan.io/token/0x6F42729f103a6C612B8B50Ca64db5f451707170B)