# We checked the tokens and counters of initial token

Whe checked the compliance and token info about PoCTokA01 (the first proxy).
It was ok.

```bash
diego-securitize@heavy-machine:~/dsclient/master$ dsclient token:info
{"tokenParams":{"name":"PoC-Token-A01","symbol":"PoCTokA01","decimals":0,"totalIssued":"3186","totalWallets":13,"isPaused":false}}
diego-securitize@heavy-machine:~/dsclient/master$ dsclient token:totalSupply
1456
diego-securitize@heavy-machine:~/dsclient/master$ dsclient compliance:counters
{
  euRetailInvestorsCounts: [
    { countryCode: 'AT', count: 0 },
    { countryCode: 'BE', count: 0 },
    { countryCode: 'BG', count: 0 },
    { countryCode: 'HR', count: 0 },
    { countryCode: 'CY', count: 0 },
    { countryCode: 'CZ', count: 0 },
    { countryCode: 'DK', count: 0 },
    { countryCode: 'EE', count: 0 },
    { countryCode: 'FI', count: 0 },
    { countryCode: 'FR', count: 0 },
    { countryCode: 'DE', count: 0 },
    { countryCode: 'GR', count: 0 },
    { countryCode: 'HU', count: 0 },
    { countryCode: 'IE', count: 0 },
    { countryCode: 'IT', count: 0 },
    { countryCode: 'LV', count: 0 },
    { countryCode: 'LT', count: 0 },
    { countryCode: 'LU', count: 0 },
    { countryCode: 'MT', count: 0 },
    { countryCode: 'NL', count: 0 },
    { countryCode: 'PL', count: 0 },
    { countryCode: 'PT', count: 0 },
    { countryCode: 'RO', count: 0 },
    { countryCode: 'SK', count: 0 },
    { countryCode: 'SI', count: 0 },
    { countryCode: 'ES', count: 0 },
    { countryCode: 'SE', count: 0 },
    { countryCode: 'GB', count: 0 },
    { countryCode: 'IS', count: 0 },
    { countryCode: 'LI', count: 0 },
    { countryCode: 'NO', count: 0 },
    { countryCode: 'CH', count: 0 }
  ],
  usInvestorsCount: 2,
  totalInvestorsCount: 12,
  accreditedInvestorsCount: 8,
  usAccreditedInvestorsCount: 0,
  jpInvestorsCount: 0
}
diego-securitize@heavy-machine:~/dsclient/master$ dsclient compliance:getCountry US
us
diego-securitize@heavy-machine:~/dsclient/master$ dsclient compliance:info
{
  totalInvestorsLimit: 5000,
  minUSTokens: '0',
  minEUTokens: '0',
  usInvestorsLimit: 5000,
  euRetailInvestorsLimit: 150,
  jpInvestorsLimit: 0,
  usAccreditedInvestorsLimit: 5000,
  nonAccreditedInvestorsLimit: 5000,
  maxUSInvestorsPercentage: 100,
  blockFlowbackEndTime: 4000000000,
  nonUSLockPeriod: 0,
  minimumTotalInvestors: 0,
  minimumHoldingsPerInvestor: '0',
  maximumHoldingsPerInvestor: '5000',
  usLockPeriod: 31536000,
  forceFullTransfer: false,
  forceAccredited: false,
  forceAccreditedUS: true,
  worldWideForceFullTransfer: false
}
diego-securitize@heavy-machine:~/dsclient/master$ dsclient lock:list investor01
[
  { value: '10', reason: 'evasor', reasonCode: 0 },
  { value: '20', reason: 'evasor2', reasonCode: 0 }
]
diego-securitize@heavy-machine:~/dsclient/master$ dsclient lock:list investor02
[ { value: '50', reason: 'test-sucutrule', reasonCode: 0 } ]
diego-securitize@heavy-machine:~/dsclient/master$ dsclient lock:list investor03
[]
```