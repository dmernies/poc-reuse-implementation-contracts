# Verifing deploys

```bash
diego-securitize@heavy-machine:~/dsclient/prueba-rinkeby/YautjaToken$ dsclient holder:create holder01 --accredited=approved --kyc=approved --wallet=0x298890444c7AB229384B98CbD41D7442fc355875
0xb6aad4d2cc4c1ce364a4c787130969565ba30ba480849fdd69e23c27fb7f11db
```

## Test01-Rinkeby: Minting 1500 Tokens to YautjaToken

We minted 1500 YautjaTokens. Other tokens was OK (no changes)!!!

```bash
diego-securitize@heavy-machine:~/dsclient/prueba-rinkeby/YautjaToken$ dsclient token:mint 0x298890444c7AB229384B98CbD41D7442fc355875 1500
0x04eed0bc2bb2891c6f883af1266a5a47a728b8a25131a9be7929d2924e1f3e11

diego-securitize@heavy-machine:~/dsclient/prueba-rinkeby/YautjaToken$ dsclient token:info
{"tokenParams":{"name":"YautjaToken","symbol":"PREDATOR","decimals":0,"totalIssued":"1500","totalWallets":1,"isPaused":false}}
```

## Test02-Rinkeby: Running BlockchainTeam test:token battery

Only BlockchainTeam token was updated. Other tokens was OK!

```bash
diego-securitize@heavy-machine:~/dsclient/prueba-rinkeby/BlockchainTeam$ dsclient test:token


  TOKEN related operations tests
0x9a555e61008afd1a5bd45501e096588b95d581b4e02542dbaccfc053c90b70bd
    ✓ should retrieve the information for the test Token (1123ms)
0x89dd2fb94ada8f476a988ef20058083e8bcab764c119a505072942994849698c
0x52a27b9a9165fb0a68e5246919dd6af963b336ac493b4672ddcf02bbf48ede1f
0xee120db48d778398ef972ea6cf45b8d106b2b0affbc2a4780862b2b2bea516b2
    ✓ should be able to mint tokens to Holder 1 (44864ms)
0xea6ca2e38a651a867900bb68c78d21d3e22078073814985d8a479841c95708c1
    ✓ should be able to create a Holder and include a mint operation in the same transaction (14011ms)
0x794b81bd70f75eaa80f3360e1f7f8be07675459cfb39ef437c85d31d01336b41
0xd2453d43e324640068599565e9558976b61599e3d2a6dcd76e9110686ef9082c
0x73b92dfc3be302b64476f3e3ca9f7c680fc1e82ba7e838cc0f3b8616327290c3
0x11bb300611051367fd51b4a853c75ee93a9a76ead0c06df4eb204eed39401bb4
0xf0b1a5d5a81be2eee9d25194246808579d9a43f79a46b22bc4ec6cd9f430d54b
0xa22b6ad287376ae03d0c8aa5de2c57be5e88099b79b6b899ffcb18ca89d297ff
    ✓ should be able to transfer tokens between Holder 1 and Holder 2 (89712ms)
0x86d5c6d9ad9a8d9adff1f3d688c4fdcc1cf2916d14c666caa9f03f8c6de2f416
0x91076b631739a75ade3c2699eadec960170c46192161153709b4f2359a663551
    ✓ should not allow to transfer more tokens than the current balance (30373ms)
    ✓ should not transfer tokens to an address not associated with a holder (710ms)
Current supply of tokens: 
425
0xca7af2af0d0e169741abe1266416e05e701c3c68f954af2caea83fa6ac5874fc
0xea28329558bae2dfd88c192604e7a677aec3315d3f32f4bdc6922bbb0954e78d
0x57857614eb825fff10c1225c2b20c22a983aea11ed3fe902a50075acdbae69b3
Total supply after minting 150: 
575
    ✓ should get the correct value for total Supply (44479ms)
0xda9749d7e429760424e0a3de332e20de57af6a741d6a9651c780b950caa5e08a
0x1b9441a7f67a2695b9ccdcd9eb742918789b9a7df3dea46e305c739170a3caba
0x947e5b34d37111a8af8e8e1e4d9f2449ddf3ea7f46bf2587c8388e88c5c353d2
0x7fea8cec45445986e70071245b639f9ff73a2dd504a336fd3cecc698d57e9476
    ✓ should allow to burn tokens to the owner (60362ms)
0x19edd8e75d1db2abc50252014448065bd03c5c07e12fc93c95f54744769531f2
    ✓ should emit an event when a holder is created (21576ms)
0x7e72d7c9019200dd85e2197c13557d2a50ee6346f036b5d7d07e44d0aac5b8ec
0xd93f01886baef8ed11d87287d17447aa5557a4d8470345bbd03e261b3465ab39
    ✓ should emit an event when a wallet is added to a holder (23252ms)
0x8844950a30ee503eb1387e8d40b070420e5835b5da01689fa32052b7c21755ef
0x6661a439fa1a6bbe8bcfaf3b50ed0fd80c52ebc015430c6302c43be2a47e6a2c
    ✓ should emit an event when the country is changed for an investor (30898ms)
0xa8a1f63eb515d8e56965e4fc2bfc33ea614391e21201a79538d3e528502ca272
0x4f40fe166dbe7f5a5898a1d8c2251dde63157bd85cf150804a4d9850e7a5b76f
0xa144a580138ee95ad24c355fc46a81baec1d71d6f51c4b71fa96e0acfb69330d
0x86df0d22e9291d403fb46496530e97ce935adc77e9eba1e814fb09d9f09c0857
0x8ccd7a574dcd47b860186ace48945d3fe8dba123774c7f68f389de11f3eb9d95
    ✓ should return the correct amount for the balance of a holder including all their wallets (74184ms)
    ✓ should be able to filter events (3763ms)
0x24c47344ccf677c86e404cd9f8e19ca945aa6ff2887b37753b8bdb322df8872c
0x15527c437932caae1c7e11691b6a554a72aad92dd06bf660f8784596d7283d31
0x33bcf5ca3dd26bb739e95585a2ef5b3a0d7b2fbf70ffb28647f77ee735f967ee
0xa0549ef19ba1b0688363b9ec2f6989836b2a16b8025f646e800eb51499febc98
0x8f7790e6f2be359227e11f9d9afae65803a0d4abe8547040d1a51b0a7dda9838
    ✓ should be able to pause the token (70788ms)
0xe15bd35b6d94827afe6519e2ce8d46e72c245d88c3c62c172f0f5075311dcf2c
    ✓ should be able to approve a transfer from a spender (15931ms)
    - should be able to transferFrom a holder to a spender with the correct allowance
    - should be able to seize tokens from a holder


  15 passing (9m)
  2 pending
```

## Test03-Rinkeby: Chicago Team

It was OK. Only Chicago Team was upated

```bash
diego-securitize@heavy-machine:~/dsclient/prueba-rinkeby/ChicagoTeam$ dsclient test:token


  TOKEN related operations tests
0x449daecc397534026dec7cc686dd9ddae8cb75e4ef7fe9e18d7333d5f36e4525
    ✓ should retrieve the information for the test Token (1214ms)
0xf4bbe81b05a26710bf162e4cd9b9506d5ab080068db73ce5765858eb92bedc4e
0xfe6b867545212e5ea036ac74b38e1e9a31b63cf704e9693da67fb64e95d3c1bd
0x5d0fe0a02f6f4733d6547ab34eb4fa0084753eae6460bed5ee407bc01bdf2781
    ✓ should be able to mint tokens to Holder 1 (43106ms)
0xf9ab6d879b563a43983d7ccd05392fe563ff1e80d7d17ba86d5c8debc5f5b394
    ✓ should be able to create a Holder and include a mint operation in the same transaction (13745ms)
0xd1d02243a8b91cc2f230af271d0ce8350d10a094ed76255db84f314e96698bdb
0x94771817ef92198cb112e051e153e4bb3b1e94ef4011c21fdeee9189fba286e6
0x3c559f4deabb559d3a8b4077d778b6b860ba621800ff9a6611fa618b02e6aa38
0xcfd028b130d8013502bef46be213fe6a45557371b7cb660db63d46e61b5e9976
0x1fb401da790fe93f880063e379a01dd8d02c896f601633338ae6ca2fae9997ea
0x9c85f6fcc8345eec5b250b24358c68c2b0285d3b223d1e369406775248caadc4
    ✓ should be able to transfer tokens between Holder 1 and Holder 2 (91045ms)
0xf996ab7858a14d9539ca9635d23d51699f1a94e35fc7e462a2aeed1745d5ab50
0x979f33d3417b4ae4401f0f556969f5d337ee33d91e7f396a425e660c3a8e928f
    ✓ should not allow to transfer more tokens than the current balance (29875ms)
    ✓ should not transfer tokens to an address not associated with a holder (716ms)
Current supply of tokens: 
425
0x355e61f8ba67f3486d404b967d8ae5a1637f6cd0a445440022ac2dc466701191
0x6c8ed2de154c42d9d18bee50e665d9b11a6e792423397d053434c1c99806d1fb
0x4aad90716946b86b5fd793ea9e74a8dbacf5244af3a35449b3adfeee4b104e53
Total supply after minting 150: 
575
    ✓ should get the correct value for total Supply (44171ms)
0x6d4bacd16a1435bd783b39fb24b4ed826b5e7653b21d83f1b886172b4a8e5bf4
0x119ffdc0f2f5a73dc10fd2dcd7c82ba9b1b265b5e1ead014eabe02622ab4bacd
0x61bb59535aa9aea7ee5f76601abf5d5e415367f431959ec567765d9813d8026a
0x4960e012e93754109011e3563bd421815711ea04390caa8dfb39d0058aac7f53
    ✓ should allow to burn tokens to the owner (60114ms)
0x6dedd84f1a19eeb868eade885b77f4a92359772487689fb57db1b0f7f728085f
    ✓ should emit an event when a holder is created (21093ms)
0xc43037c2c61ad91ccd138cec3dc603713864505542ad9cca2c7d0d1f5826de98
0x6eb729bd848509957d6295388abcf8a92a98e20ae95656fdb6e6c503f004fa16
    ✓ should emit an event when a wallet is added to a holder (23164ms)
0x08eb615c30e28aa7b4036ab57499274e2d196aff8dbdc32342a011ff40378f02
0x92d19e7adb1d6a1a1f0832a6a06a6b9637db8b632172e7e2ac31cfca28532a33
    ✓ should emit an event when the country is changed for an investor (31786ms)
0xc7790aa0de7db89965030d813daf2fb6f20c347f3dc9f5ee58ec681e3a51fabd
0xc4f40316924be25ba0ea15b5dc0f16191112f00a0eae58915ead593cb976671d
0xc5a93a0ec00636d83df8085327d1ab90c4d92d2c617f36df4a9bfba8db6d845f
0xfcaa00f4525c621e9575013e0b2bc8ea24ac123df2e565fdd16724ea2bb1613b
0xe94f4046c3bd3f85106e54ef843f4a9015162a649f987ea85b0ad08112697c2c
    ✓ should return the correct amount for the balance of a holder including all their wallets (73190ms)
    ✓ should be able to filter events (3268ms)
0x93562b49fa89d542f5331593bb7539fe87119cb1e07f3b646177576ce6149baa
0xdfd2514fe550a0caaecdf1bd208072c2a6092a212c470f01e7e4f912aa0704a1
0xda360664cb13d1853b310443f3fccd082d031a9f52259f931998e50061077de2
0xdd34cf0c4b824eee0d664ad32e9be8ec74633d63e69fb5d1add48bba8cd6b565
0x35e9a95be6809db13ccbc363ab22c11c971ef5e46ee91787f6d3fc7ce764a4af
    ✓ should be able to pause the token (70847ms)
0x51e4fe369a2cfa96635e3ef36b623494f5ee7a5d9a71fe23fb18b8379f9f0dac
    ✓ should be able to approve a transfer from a spender (15911ms)
    - should be able to transferFrom a holder to a spender with the correct allowance
    - should be able to seize tokens from a holder


  15 passing (9m)
  2 pending

```

## Test04-Rinkeby: Upgrading chicago team token and running

We will use the implementation contract of another token
0x9Df41D3D5F31Afc62CD71007E073AdA1fE6b8074.

```bash
diego-securitize@heavy-machine:~/src/dstoken$ npx truffle console --network rinkeby
truffle(rinkeby)> const otherToken = await Proxy.at('0x9Df41D3D5F31Afc62CD71007E073AdA1fE6b8074')
undefined
truffle(rinkeby)> otherToken.target.call()
'0x9ba6B4588d2866B142abF40816b8722158eC30cA'
truffle(rinkeby)> const blockchainTeamToken = await Proxy.at('0x1b2c0fd2457b823e4bdadd396bb2cdcfe53b4f0a')
undefined
truffle(rinkeby)> await blockchainTeamToken.setTarget('0x9ba6B4588d2866B142abF40816b8722158eC30cA')
{
  tx: '0xe1b4a47f14114c04086eb24f8893f6a66e1a1f40ff8908dc773970f6a9beb91f',
  receipt: {
    blockHash: '0xd92c21e6c5cd2b5dbfa4193bcaac661e5907540aa100761fed8d8ad5ba4effca',
    blockNumber: 7985732,
    contractAddress: null,
    cumulativeGasUsed: 25201,
    from: '0xd51128f302755666c42e3920d72ff2fe632856a9',
    gasUsed: 25201,
    logs: [ [Object] ],
    logsBloom: '0x00000000000000000000000008000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001000000000000000000000000000000000000200000000000000000000000000000000000000000000000000000000000000000100000008000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000040000000000000000000000000000000000000',
    status: true,
    to: '0x1b2c0fd2457b823e4bdadd396bb2cdcfe53b4f0a',
    transactionHash: '0xe1b4a47f14114c04086eb24f8893f6a66e1a1f40ff8908dc773970f6a9beb91f',
    transactionIndex: 0,
    rawLogs: [ [Object] ]
  },
  logs: [
    {
      address: '0x1B2c0Fd2457B823E4bdADD396bB2CdCFe53b4f0a',
      blockHash: '0xd92c21e6c5cd2b5dbfa4193bcaac661e5907540aa100761fed8d8ad5ba4effca',
      blockNumber: 7985732,
      logIndex: 0,
      removed: false,
      transactionHash: '0xe1b4a47f14114c04086eb24f8893f6a66e1a1f40ff8908dc773970f6a9beb91f',
      transactionIndex: 0,
      id: 'log_40fefceb',
      event: 'ProxyTargetSet',
      args: [Result]
    }
  ]
}
```

Now we mint 10000 to Blokchain team
```
diego-securitize@heavy-machine:~/dsclient/prueba-rinkeby/BlockchainTeam$ dsclient holder:create investor01 --accredited=approved --kyc=approved
0x2d31bd43f4819ec872b9cb5a813b798f3e7a7307a1d020b5154d67002d3a4891
diego-securitize@heavy-machine:~/dsclient/prueba-rinkeby/BlockchainTeam$ dsclient holder:addWallet investor01 0x8F4bfD7D893f81e0CB482A339FBF33e1CFF7F960
0xded47ef4c4746def5d41ab2faa3dd27a4429afb44638867a72842537e3eec940
diego-securitize@heavy-machine:~/dsclient/prueba-rinkeby/BlockchainTeam$ dsclient token:mint 0x8F4bfD7D893f81e0CB482A339FBF33e1CFF7F960 10000
0x18f7814b1ae389804b11133024aa37cdd1fa6a4b78f7a6e129153823f7021df2
```

## Test05-Rinkeby: Mintint 5000 chicago tokens

```bash
diego-securitize@heavy-machine:~/dsclient/prueba-rinkeby/ChicagoTeam$ dsclient holder:create investor01 --accredited=approved --kyc=approved
0xcf7b4e861ddea1a318e874817948c36c5def1099bc480a33b12a4906e616f946
diego-securitize@heavy-machine:~/dsclient/prueba-rinkeby/ChicagoTeam$ dsclient holder:addWallet investor01 0x610BAC8FA1cf31b934992E29b492D05cd898DA79
0x90527dbc08ea1b775c51baf41d651ee574158f28d210c606493a802265dedb0b
diego-securitize@heavy-machine:~/dsclient/prueba-rinkeby/ChicagoTeam$ dsclient token:mint 0x610BAC8FA1cf31b934992E29b492D05cd898DA79 5000
0x3055ac2eb6d1bb356da8d40949c8b5eaf75e33d39a9ace24cade01debc946f43
```